# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release (code from __init__.py)
#   o Cafix can now open and save screen capture
#   o New CafixFile class
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Cafix file format manager"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import os
from . import catdic
from . import pictures
from . import data as datacls

class CafixFile:
    """Cafix file format manager."""
    
    name = 'cafix'
    managed_data = [datacls.Program, datacls.Backup, datacls.Picture, \
                    datacls.ScreenCapture]
    read = True
    write = True
    list_ext = ['cafix']
    def __init__(self, filename = None):
        """Make a cafix file object with filename"""
        self.filename = filename
        
    def open_file(self):
        """Open a file in the cafix Format"""
        # Build the FileData
        file_data = datacls.FileData()
        # Try to import metadata from the file name
        name = os.path.basename(self.filename)\
                        .replace('prog-', '').replace('.cafix', '')\
                        .replace('backup-', '')
        # Open raw data
        cafix_file = open(self.filename, 'r')
        raw_data = cafix_file.read()
        cafix_file.close()
        # Try to set the type:
        if os.path.basename(self.filename)[:5] == 'prog-':
            new_data = file_data.new_record(datacls.Program)
            new_data.use_base = catdic.is_base_program(raw_data)
            new_data.raw_data = raw_data
            new_data.name = name
        elif os.path.basename(self.filename)[:7] == 'backup-':
            new_data = file_data.new_record(datacls.Backup)
            new_data.raw_data = raw_data
            new_data.name = name
        elif os.path.basename(self.filename) == 'unknown-.cafix' and \
                raw_data.startswith('\x01'):
            new_data = file_data.new_record(datacls.ScreenCapture)
            # add the white/3 sheet:
            new_data.raw_data = raw_data[:0x400 * 2 + 2] +\
                                '\x03' + '\x00' * 0x400 +\
                                raw_data[0x400 * 2 + 2:]
            new_data.pallet = ['b', 'g', 'w', 'o']
            new_data.color_byte = 0
            new_data.name = 'Screen capture'
        else:
            new_data = file_data.new_record(datacls.Data)
            new_data.name = name
            new_data.raw_data = raw_data
        return file_data

    def save(self, data_list):
        """Save a file in the cafix Format"""
        folder = os.path.dirname(self.filename)
        for data in data_list:
            # make a filename from the metadata
            if len(data_list) == 1 and os.path.basename(self.filename) != '':
                # try to use directory as name
                filename = self.filename
            else:
                if data.__class__ == datacls.Program:
                    filename = os.path.join(folder, 'prog-' + data.name \
                                        + '.cafix')
                elif data.__class__ == datacls.Backup:
                    filename = os.path.join(folder, 'backup-' + data.name + \
                                        '.cafix')
                elif data.__class__ == datacls.Picture or \
                        data.__class__ == datacls.ScreenCapture:
                    filename = os.path.join(folder, 'unknown-.cafix')
                else:
                    filename = os.path.join(folder, 'unknown-' + data.name\
                           + '.cafix')
            # write raw data
            cafix_file = open(filename, 'w')
            if data.__class__ == datacls.Picture or \
                        data.__class__ == datacls.ScreenCapture:
                # Remove the white sheet
                sheets = pictures.raw_to_sheets(data.raw_data, \
                                            data.color_byte, \
                                            len(data.pallet))
                del sheets[data.pallet.index('w')]
                cafix_file.write(pictures.sheets_to_raw(sheets))
            else:
                cafix_file.write(data.raw_data)
            cafix_file.close()
