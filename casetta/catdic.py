# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o correct some newcat equiv wich was on cat dictionaries
#   o get from __init__.py the code to:
#       - save and open cat and newcat files
#       - make conversion between raw and text or cat
#   o correct the ctoken for \Min( (thanks to Roy MacLean)
#   o Add CatFile and NewcatFile classes
#   o Move CatFile and NewcatFile in the new file cat.py
#   o Add img_decode/img_decode functions
#   o Add pict_decode/pict_encode functions
#
# # 0.2.1 version:
#
#   o version number aligned on ctfdic.py
#
# # 0.2.0 version:
#
#   o New organisation of dictionaries
#   o pylint code correction
#   o add \ln equiv in newcat format
#   o add a space after sin/cos/tan/asin/acos/atan equiv
#   o remove the space after Ran#
#   o correction of \RigtTptch -> \RightTptch
#   o add a newcat equiv \Meanx for 0xC2
#   o correct b\R n+1 in b\R n+2
#   o correct b0 in \b0
#   o add command for base programs for cat and newcat formats
#   o add the list for base characters
#   o add ProbQ(, ProbR(, Probt( cat equivs
#   o new title_equiv dictionary for equivs in title and 
#     passwords (cat and newcat formats)
#   o fix a cat bug with Next, Goto, \r and \To equiv
#
# # 0.1.2 version:
#
#   o fix a problem with the cat equiv for 'P(' (0xFB)
#   o add a newcat equvi for 'x^-1' (0x9B)
#
# # 0.1.1 version:
#
#   o version aligned on casetta.py
#
# # 0.1.0 version:
#
#   o first release
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Equivalence dictionaries into Casio Cat format and raw ascii-like format"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import base64
import re
from . import pictures

CAT_PALLET = ['o', 'b', 'g', 'w']

# Decode/Encode functions
def cat_encode(raw_data, title=False):
    """Convert raw data to cat-like data"""
    if title:
        out = raw_data
        for raw_car, cat_equiv in list(title_equiv.items()):
            out = out.replace(raw_car, cat_equiv)
        return out
    out = raw_data
    # convert * /
    out = out.replace("*", "\\aster")
    out = out.replace("\xf7\\aster", "\xf7*")
    out = out.replace("/", "\\slash")
    out = out.replace("\xf7\\slash", "\xf7/")
    #Convert ctokens in tokens
    ctoken_list, token_for = get_r2c_data()
    for ctoken in ctoken_list:
        out = out.replace(ctoken, token_for[ctoken])
    # Remove the end caract (chr(255))
    out = out.replace(chr(255), '')
    # Change end line (chr(13) to chr(13)+chr(10)
    out = out.replace(chr(13), chr(13) + chr(10))
    return out

def cat_decode(cat_data, title=False):
    """Convert cat-like data to raw data"""
    if title:
        out = cat_data
        for raw_car, cat_equiv in list(title_equiv.items()):
            out = out.replace(cat_equiv, raw_car)
        return out
    out = cat_data
    # Change end line chr(13)+chr(10) in chr(13)
    out = out.replace(chr(13) + chr(10), chr(13))
    # Save "\\DispR-Tbl" and "\\DrawWeb"
    out = out.replace("\\DispR-Tbl", "\\save_drtbl")
    out = out.replace("\\DrawWeb", "\\save_DrawWeb")
    #Convert ctokens in tokens
    token_list, ctoken_for = get_c2r_data()
    for token in token_list:
        out = out.replace(token, ctoken_for[token])
    # convert * /and save "\\DispR-Tbl" and "\\DrawWeb"
    out = out.replace("\\aster", "*")
    out = out.replace("\\slash", "/")
    out = out.replace("\\save_drtbl", chr(247)+chr(47))
    out = out.replace("\\save_DrawWeb", chr(247)+chr(42))
    # Add the last caract (chr(255))
    out += chr(255)
    return out

def catb16_encode(raw_data):
    """Convert raw data to base 16 cat like data"""
    b16raw = base64.b16encode(raw_data)
    out = ''
    for index in range(len(b16raw)):
        out += b16raw[index]
        if (index + 1) % 32 == 0 :
            out += "\r\n"
    return out

def catb16_decode(cat_data):
    """Convert base 16 cat like data to raw data"""
    return base64.b16decode(cat_data.replace('\r', '').replace('\n', ''))

def text_encode(raw_data, title=False):
    """Convert raw data to Editable text (NewCat format)"""
    if title:
        out = raw_data
        for raw_car, cat_equiv in list(title_equiv.items()):
            out = out.replace(raw_car, cat_equiv)
        return out
    out = raw_data
    # convert * /
    out = out.replace("*", "\\aster")
    out = out.replace("\xf7\\aster", "\xf7*")
    out = out.replace("\x7f\\aster", "\x7f*")
    out = out.replace("/", "\\slash")
    out = out.replace("\xf7\\slash", "\xf7/")
    out = out.replace("\x7f\\slash", "\x7f/")
    #Convert ctokens in tokens
    ctoken_list, token_for = get_r2c_data(True)
    for ctoken in ctoken_list:
        out = out.replace(ctoken, token_for[ctoken])
    # Remove the end caract (chr(255))
    out = out.replace(chr(255), '')
    # Change end line chr(13) in chr(10)
    out = out.replace(chr(13), '\n')
    return out

def text_decode(text_data, title=False):
    """Convert Editable text (NewCat format) to raw data"""
    if title:
        out = text_data
        for raw_car, cat_equiv in list(title_equiv.items()):
            out = out.replace(cat_equiv, raw_car)
        return out
    out = text_data
    # Change end line chr(10) in chr(13)
    out = out.replace('\n', chr(13))
    # Save "\\DispR-Tbl" and "\\DrawWeb"
    out = out.replace("\\DispR-Tbl", "\\save_drtbl")
    out = out.replace("\\DrawWeb", "\\save_DrawWeb")
    #Convert ctokens in tokens
    token_list, ctoken_for = get_c2r_data(True)
    for token in token_list:
        out = out.replace(token, ctoken_for[token])
    # convert * / and save "\\DispR-Tbl" and "\\DrawWeb"
    out = out.replace("\\aster", "*")
    out = out.replace("\\slash", "/")
    out = out.replace("\\save_drtbl", chr(247)+chr(47))
    out = out.replace("\\save_DrawWeb", chr(247)+chr(42))
    # Add the last caract (chr(255))
    out += chr(255)
    return out

def img_decode(txt_img):
    """Convert text image (cat format) to raw data"""
    txt_sheets = txt_img.split('Sheet:1')
    sheets = [''] * 4
    for txt_sheet in txt_sheets:
        if txt_sheet != '':
            txt_sheet = txt_sheet.replace('\r', '').replace('\n', '')[6:]
            byte_list = []
            for car in txt_sheet[1:]:
                if byte_list == [] or len(byte_list[-1]) > 1:
                    byte_list.append(car)
                else:
                    byte_list[-1] += car
            # Add the color byte
            byte_list = [txt_sheet[0]] + byte_list
            sheet = ''
            for byte in byte_list:
                sheet += chr(int(byte, 16))
            sheets[int(txt_sheet[0], 16) - 1] = sheet
    return sheets

def img_encode(data):
    """Convert a picture object to a text (cat format) image"""
    # Convert the picture to use the right pallet
    data.change_pallet(CAT_PALLET)
    # Get sheets
    sheets = pictures.raw_to_sheets(data.raw_data, data.color_byte, \
                                    len(CAT_PALLET))
    # Convert sheets
    txt_img = ''
    for sheet in sheets:
        # Color byte
        txt_img += 'Sheet:1\r\nColor:' + str(ord(sheet[0])) + '\r\n'
        # other bytes
        for index in range(len(sheet[1:])):
            txt_img += hex(ord(sheet[index + 1]))[2:].upper().zfill(2)
            if (index + 1) % 16 == 0:
                txt_img += '\r\n'
    return txt_img[:-2] # Remove the last \r\n

def pict_decode(txt_img):
    """Convert text image (newcat PICT format) to raw data"""
    txt_sheets = txt_img.split("Color:")
    pict_data = [pictures.WHITE] * (128 * 64)
    for txt_sheet in txt_sheets:
        if txt_sheet != '':
            color_id = txt_sheet[0]
            # Get the color
            if color_id == '1':
                color = pictures.BLUE
            elif color_id == '2':
                color = pictures.GREEN
            elif color_id == '3':
                color = pictures.ORANGE
            else:
                color = ''
            if color != '':
                color_txt_data = txt_sheet[1:].replace('\r', '').\
                                               replace('\n', '')
                # Split the text in a list of bytes
                byte_list = []
                for car in color_txt_data:
                    if byte_list == [] or len(byte_list[-1]) > 1:
                        byte_list.append(car)
                    else:
                        byte_list[-1] += car
                # Expand the list in bits
                bit_list = []
                for byte in byte_list:
                    bit_list += [int(car, 2) for car in \
                                 pictures.bin(int(byte, 16))]
                # Convert the bit list in picture data
                for index in range(len(bit_list)):
                    if bit_list[index]:
                        pict_data[index] = color
    return pict_data

def pict_encode(data):
    """Convert a picture object to a text (newcat PICT format) image"""
    pixel_list = data.get_picture()
    color_to_save = [pictures.BLUE, pictures.GREEN, pictures.ORANGE]
    text_out = ''
    for color in color_to_save:
        # Convert the pixel list into bit list
        bit_list = []
        for pixel in pixel_list:
            if pixel == color:
                bit_list.append('1')
            else:
                bit_list.append('0')
        if bit_list != ['0'] * len(bit_list):
            # Convert the bit list into byte list
            byte_list = []
            for index in range(len(bit_list) / 8):
                byte = hex(int(''.join(bit_list[index * 8 : index * 8 + 8]), 2))
                byte_list.append(byte[2:].upper().zfill(2))
            # Convert the byte list into text
            text_out += 'Color:' + str(color_to_save.index(color) + 1) + '\n'
            for index in range(len(byte_list)):
                text_out += byte_list[index]
                if (index + 1) % 16 == 0:
                    text_out += '\n'
    return text_out

# Misc
def is_base_program(raw_data):
    """Return if the raw_data program use base functions
    This function is used to detect if the program may have the
    BASE flag. It must be used only if casetta is not able to detect
    the BASE flag in anoter way.
    
    """
    reg_exp = ''
    for spe_car in base_car:
        reg_exp += '[^\f7\7f]' + spe_car + '|'
    reg_exp = reg_exp[:-1]
    base_pattern = re.compile(reg_exp)
    if base_pattern.search(raw_data) == None:
        return False
    else:
        return True

# Tokens data and functions
def get_r2c_data(newcat=False):
    """Return necessary data to make the convertion
    
    Return the sorted by descendant len list of ctoken and the dictionary of
    tokens"""
    len1 = []
    len2 = []
    token_for = {}
    for ctoken in r2c:
        if len(ctoken) == 2:
            len2.append(ctoken)
        else:
            len1.append(ctoken)
        token_for[ctoken] = r2c[ctoken][0]
    if newcat:
        for ctoken in r2n:
            if len(ctoken) == 2:
                len2.append(ctoken)
            else:
                len1.append(ctoken)
            token_for[ctoken] = r2n[ctoken][0]
    return (len2 + len1, token_for)

def get_c2r_data(newcat=False):
    """Return necessary data to make the convertion
    
    Return the sorted by descendant len list of tokens and the dictionary of
    ctokens"""
    len_dic = {}
    ctoken_for = {}
    for ctoken in r2c:
        for token in r2c[ctoken]:
            if not len(token) in len_dic:
                len_dic[len(token)] = []
            len_dic[len(token)].append(token)
            ctoken_for[token] = ctoken
    if newcat:
        for ctoken in r2n:
            for token in r2n[ctoken]:
                if not len(token) in len_dic:
                    len_dic[len(token)] = []
                len_dic[len(token)].append(token)
                ctoken_for[token] = ctoken
    len_list = list(len_dic.keys())
    len_list.sort()
    len_list.reverse()
    token_list = []
    for alen in len_list:
        token_list += len_dic[alen]
    return (token_list, ctoken_for)

# This module provide 2 dictionaries :
# r2c: raw to cat
# r2n: raw to newcat

# Same characters as ASCII:
# A~Z 0~9 spaces , : ( ) . { } [ ] = ' " ~ * / #
# End of line (chr(13) or \r)
# 0xFF -> end of file

r2c = {}
# Fichier prog-CH-CL+ME
r2c[chr(247)+chr(204)]=["\\DrawOn"]
r2c[chr(247)+chr(220)]=["\\DrawOff"]
r2c[chr(247)+chr(74)]=["\\S-Gph1 "]
r2c[chr(247)+chr(75)]=["\\S-Gph2 "]
r2c[chr(247)+chr(76)]=["\\S-Gph3 "]
r2c[chr(247)+chr(80)]=["\\Scatter"]
r2c[chr(247)+chr(81)]=["\\xyLine"]
r2c[chr(247)+chr(82)]=["\\Hist"]
r2c[chr(247)+chr(83)]=["\\MedBox"]
r2c[chr(247)+chr(84)]=["\\MeanBox"]
r2c[chr(247)+chr(85)]=["\\N-Dist"]
r2c[chr(247)+chr(86)]=["\\Broken"]
r2c[chr(247)+chr(87)]=["\\Linear"]
r2c[chr(247)+chr(88)]=["\\Med-Med"]
r2c[chr(247)+chr(89)]=["\\Quad"]
r2c[chr(247)+chr(90)]=["\\Cubic"]
r2c[chr(247)+chr(91)]=["\\Quart"]
r2c[chr(247)+chr(92)]=["\\Log"]
r2c[chr(247)+chr(93)]=["\\Exp"]
r2c[chr(247)+chr(94)]=["\\Power"]
r2c[chr(127)+chr(106)]=["\\List1"]
r2c[chr(127)+chr(107)]=["\\List2"]
r2c[chr(127)+chr(108)]=["\\List3"]
r2c[chr(127)+chr(109)]=["\\List4"]
r2c[chr(127)+chr(110)]=["\\List5"]
r2c[chr(127)+chr(111)]=["\\List6"]
r2c[chr(247)+chr(77)]=["\\Square"]
r2c[chr(247)+chr(78)]=["\\Cross"]
r2c[chr(247)+chr(79)]=["\\Dot"]
r2c[chr(127)+chr(53)]=["\\Blue "]
r2c[chr(127)+chr(52)]=["\\Orange "]
r2c[chr(127)+chr(54)]=["\\Green "]
r2c[chr(247)+chr(64)]=["\\1-Variable "]
r2c[chr(247)+chr(65)]=["\\2-Variable "]
r2c[chr(247)+chr(66)]=["\\LinearReg "]
r2c[chr(247)+chr(67)]=["\\Med-MedLine "]
r2c[chr(247)+chr(68)]=["\\QuadReg "]
r2c[chr(247)+chr(69)]=["\\CubicReg "]
r2c[chr(247)+chr(70)]=["\\QuartReg "]
r2c[chr(247)+chr(71)]=["\\LogReg "]
r2c[chr(247)+chr(72)]=["\\ExpReg "]
r2c[chr(247)+chr(73)]=["\\PowerReg "]
r2c[chr(127)+chr(69)]=["\\Swap("]
r2c[chr(127)+chr(66)]=["\\*Row("]
r2c[chr(127)+chr(67)]=["\\*Row+("]
r2c[chr(127)+chr(68)]=["\\Row+("]
r2c[chr(247)+chr(176)]=["\\SortA("]
r2c[chr(247)+chr(177)]=["\\SortD("]
r2c[chr(247)+chr(200)]=["\\G SelOn "]
r2c[chr(247)+chr(216)]=["\\G SelOff "]
r2c[chr(247)+chr(99)]=["\\Y=Type"]
r2c[chr(247)+chr(100)]=["\\r=Type"]
r2c[chr(247)+chr(101)]=["\\ParamType"]
r2c[chr(247)+chr(103)]=["\\X=cType"]
r2c[chr(247)+chr(106)]=["\\Y>Type"]
r2c[chr(247)+chr(107)]=["\\Y<Type"]
r2c[chr(247)+chr(108)]=["\\Y>=Type"]
r2c[chr(247)+chr(109)]=["\\Y<=Type"]
r2c[chr(127)+chr(56)]=["\\BlueG "]
r2c[chr(127)+chr(55)]=["\\OrangeG "]
r2c[chr(127)+chr(57)]=["\\GreenG "]
r2c[chr(247)+chr(149)]=["\\StoGMEM "]
r2c[chr(247)+chr(150)]=["\\RclGMEM "]
r2c[chr(247)+chr(202)]=["\\D SelOn "]
r2c[chr(247)+chr(218)]=["\\D SelOff "]
r2c[chr(247)+chr(128)]=["\\D Var "]
r2c[chr(247)+chr(101)]=["\\ParamType"]
r2c[chr(247)+chr(201)]=["\\T SelOn "]
r2c[chr(247)+chr(217)]=["\\T SelOff "]
r2c[chr(247)+chr(203)]=["\\R SelOn "]
r2c[chr(247)+chr(219)]=["\\R SelOff "]
r2c[chr(247)+chr(144)]=["\\aRanType"]
r2c[chr(247)+chr(145)]=["a\\R n+Type"]
r2c[chr(127)+chr(163)]=["\\R n"]
r2c[chr(127)+chr(160)]=["a\\R n"]
r2c[chr(127)+chr(161)]=["a\\R n+"]
r2c[chr(127)+chr(167)]=["b\\R n"]
r2c[chr(127)+chr(168)]=["b\\R n+1"]

# fichier prog-ch-opt
r2c[chr(127)+chr(81)]=["\\List "]
r2c[chr(127)+chr(74)]=["\\List->Mat("]
r2c[chr(127)+chr(70)]=["\\Dim "]
r2c[chr(127)+chr(71)]=["\\Fill("]
r2c[chr(127)+chr(44)]=["\\Seq("]
#r2c[chr(127)+chr(45)]=["\\Min("] # ??
r2c[chr(127)+chr(153)]=["\\Min("]
r2c[chr(127)+chr(32)]=["\\Max("]
r2c[chr(127)+chr(46)]=["\\Mean("]
r2c[chr(127)+chr(47)]=["\\Median("]
r2c[chr(127)+chr(76)]=["\\Sum "]
r2c[chr(127)+chr(77)]=["\\Prod "]
r2c[chr(127)+chr(79)]=["\\Cuml "]
r2c[chr(127)+chr(78)]=["\\Percent "]
r2c[chr(127)+chr(64)]=["\\Mat "]
r2c[chr(127)+chr(75)]=["\\Mat->List("]
r2c[chr(127)+chr(33)]=["\\Det "]
r2c[chr(127)+chr(65)]=["\\Trn "]
r2c[chr(127)+chr(73)]=["\\Augment("]
r2c[chr(127)+chr(72)]=["\\Identity "]
r2c[chr(127)+chr(80)]=["\\{i}"]
r2c[chr(127)+chr(34)]=["\\Arg "]
r2c[chr(127)+chr(35)]=["\\Conj "]
r2c[chr(127)+chr(36)]=["\\ReP "]
r2c[chr(127)+chr(37)]=["\\ImP "]
r2c[chr(127)+chr(40)]=["\\Solve("]
r2c[chr(127)+chr(38)]=["\\d/dx("]
r2c[chr(127)+chr(39)]=["\\d2/dxc("]
r2c[chr(127)+chr(42)]=["\\FMin("]
r2c[chr(127)+chr(43)]=["\\FMax("]
r2c[chr(127)+chr(41)]=["\\Sum("]
r2c[chr(127)+chr(52)]=["\\Orange "]
r2c[chr(127)+chr(54)]=["\\Green "]
r2c[chr(247)+chr(147)]=["\\StoPict "]
r2c[chr(247)+chr(148)]=["\\RclPict "]
r2c[chr(127)+chr(176)]=["\\ And ", " \\And "]
r2c[chr(127)+chr(177)]=["\\ Or ", " \\Or "]
r2c[chr(127)+chr(179)]=["\\Not "]

# fichier prog-ch-prgm
r2c[chr(247)+chr(0)]=["\\If "]
r2c[chr(247)+chr(1)]=["\\Then "]
r2c[chr(247)+chr(2)]=["\\Else "]
r2c[chr(247)+chr(3)]=["\\IfEnd"]
r2c[chr(247)+chr(4)]=["\\For "]
r2c[chr(247)+chr(5)]=["\\To ", "\\ To "]
r2c[chr(247)+chr(6)]=["\\Step "]
r2c[chr(247)+chr(7)]=["\\Next "]
r2c[chr(247)+chr(8)]=["\\While "]
r2c[chr(247)+chr(9)]=["\\WhileEnd"]
r2c[chr(247)+chr(10)]=["\\Do"]
r2c[chr(247)+chr(11)]=["\\LpWhile "]
r2c[chr(247)+chr(12)]=["\\Return"]
r2c[chr(247)+chr(13)]=["\\Break"]
r2c[chr(247)+chr(14)]=["\\Stop"]
r2c[chr(247)+chr(24)]=["\\ClrText"]
r2c[chr(247)+chr(25)]=["\\ClrGraph"]
r2c[chr(247)+chr(26)]=["\\ClrList"]
r2c[chr(247)+chr(35)]=["\\DrawStat"]
r2c[chr(247)+chr(32)]=["\\DrawGraph"]
r2c[chr(247)+chr(34)]=["\\DrawDyna"]
r2c[chr(247)+chr(46)]=["\\DispF-Tbl"]
r2c[chr(247)+chr(36)]=["\\DrawFTG-Con"]
r2c[chr(247)+chr(37)]=["\\DrawFTG-Plt"]
r2c[chr(247)+chr(47)]=["\\DispR-Tbl"]
r2c[chr(247)+chr(42)]=["\\DrawWeb "]
r2c[chr(247)+chr(38)]=["\\DrawR-Con"]
r2c[chr(247)+chr(40)]=["\\DrawR-SumCon"]
r2c[chr(247)+chr(39)]=["\\DrawR-Plt"]
r2c[chr(247)+chr(41)]=["\\DrawR-SumPlt"]
r2c[chr(247)+chr(16)]=["\\Locate "]
r2c[chr(127)+chr(143)]=["\\Getkey"]
r2c[chr(247)+chr(17)]=["\\Send("]
r2c[chr(247)+chr(18)]=["\\Receive("]

# fichier prog-ch-sec
r2c[chr(247)+chr(151)]=["\\StoV-Win "]
r2c[chr(247)+chr(152)]=["\\RclV-Win "]
r2c[chr(247)+chr(160)]=["\\Tangent "]
r2c[chr(247)+chr(161)]=["\\Normal "]
r2c[chr(247)+chr(162)]=["\\Inverse "]
r2c[chr(247)+chr(98)]=["\\Graph X="]
r2c[chr(247)+chr(168)]=["\\PlotOn "]
r2c[chr(247)+chr(169)]=["\\PlotOff "]
r2c[chr(247)+chr(170)]=["\\PlotChg "]
r2c[chr(247)+chr(167)]=["\\F-Line "]
r2c[chr(247)+chr(166)]=["\\Circle "]
r2c[chr(247)+chr(163)]=["\\Vertical "]
r2c[chr(247)+chr(164)]=["\\Horizontal "]
r2c[chr(247)+chr(165)]=["\\Text "]
r2c[chr(247)+chr(171)]=["\\PxlOn "]
r2c[chr(247)+chr(172)]=["\\PxlOff "]
r2c[chr(247)+chr(173)]=["\\PxlChg "]
r2c[chr(247)+chr(175)]=["\\PxlTest "]

# fichier prog-ch-setup
r2c[chr(247)+chr(195)]=["\\CoordOn"]
r2c[chr(247)+chr(211)]=["\\CoordOff"]
r2c[chr(247)+chr(125)]=["\\GridOn"]
r2c[chr(247)+chr(122)]=["\\GridOff"]
r2c[chr(247)+chr(194)]=["\\AxesOn"]
r2c[chr(247)+chr(210)]=["\\AxesOff"]
r2c[chr(247)+chr(196)]=["\\LabelOn"]
r2c[chr(247)+chr(212)]=["\\LabelOff"]
r2c[chr(127)+chr(50)]=["\\P/L-Blue"]
r2c[chr(127)+chr(49)]=["\\P/L-Orange"]
r2c[chr(127)+chr(51)]=["\\P/L-Green"]
r2c[chr(247)+chr(112)]=["\\G-Connect"]
r2c[chr(247)+chr(113)]=["\\G-Plot"]
r2c[chr(247)+chr(197)]=["\\DerivOn"]
r2c[chr(247)+chr(213)]=["\\DerivOff"]
r2c[chr(247)+chr(120)]=["\\BG-None"]
r2c[chr(247)+chr(121)]=["\\BG-Pict "]
r2c[chr(247)+chr(192)]=["\\FuncOn"]
r2c[chr(247)+chr(208)]=["\\FuncOff"]
r2c[chr(247)+chr(193)]=["\\SimulOn"]
r2c[chr(247)+chr(209)]=["\\SimulOff"]
r2c[chr(247)+chr(96)]=["\\S-WindAuto"]
r2c[chr(247)+chr(97)]=["\\S-WindMan"]
r2c[chr(247)+chr(184)]=["\\File1"]
r2c[chr(247)+chr(185)]=["\\File2"]
r2c[chr(247)+chr(186)]=["\\File3"]
r2c[chr(247)+chr(187)]=["\\File4"]
r2c[chr(247)+chr(188)]=["\\File5"]
r2c[chr(247)+chr(189)]=["\\File6"]
r2c[chr(247)+chr(198)]=["\\LocusOn"]
r2c[chr(247)+chr(214)]=["\\LocusOff"]
r2c[chr(247)+chr(136)]=["\\VarRange"]
r2c[chr(247)+chr(178)]=["\\VarList1"]
r2c[chr(247)+chr(179)]=["\\VarList2"]
r2c[chr(247)+chr(180)]=["\\VarList3"]
r2c[chr(247)+chr(181)]=["\\VarList4"]
r2c[chr(247)+chr(182)]=["\\VarList5"]
r2c[chr(247)+chr(183)]=["\\VarList6"]
r2c[chr(247)+chr(199)]=["\\SumDispOn"]
r2c[chr(247)+chr(215)]=["\\SumDispOff"]

# fichier prog-ch-vars
r2c[chr(127)+chr(0)]=["\\Xmin"]
r2c[chr(127)+chr(1)]=["\\Xmax"]
r2c[chr(127)+chr(2)]=["\\Xscl"]
r2c[chr(127)+chr(4)]=["\\Ymin"]
r2c[chr(127)+chr(5)]=["\\Ymax"]
r2c[chr(127)+chr(6)]=["\\Yscl"]
r2c[chr(127)+chr(8)]=["\\Tmin"]
r2c[chr(127)+chr(9)]=["\\Tmax"]
r2c[chr(127)+chr(10)]=["\\Tptch"]
r2c[chr(127)+chr(16)]=["\\RightXmin"]
r2c[chr(127)+chr(17)]=["\\RightXmax"]
r2c[chr(127)+chr(18)]=["\\RightXscl"]
r2c[chr(127)+chr(20)]=["\\RightYmin"]
r2c[chr(127)+chr(21)]=["\\RightYmax"]
r2c[chr(127)+chr(22)]=["\\RightYscl"]
r2c[chr(127)+chr(24)]=["\\RightTmin"]
r2c[chr(127)+chr(25)]=["\\RightTmax"]
r2c[chr(127)+chr(26)]=["\\RightTptch"]
r2c[chr(127)+chr(11)]=["\\Xfct"]
r2c[chr(127)+chr(12)]=["\\Yfct"]
r2c[chr(127)+chr(118)]=["\\Q1"]
r2c[chr(127)+chr(119)]=["\\Q3"]
r2c[chr(127)+chr(120)]=["\\x1"]
r2c[chr(127)+chr(122)]=["\\x2"]
r2c[chr(127)+chr(124)]=["\\x3"]
r2c[chr(127)+chr(121)]=["\\y1"]
r2c[chr(127)+chr(123)]=["\\y2"]
r2c[chr(127)+chr(125)]=["\\y3"]
r2c[chr(127)+chr(240)]=["\\Y "]
r2c[chr(127)+chr(241)]=["\\{r} "]
r2c[chr(127)+chr(242)]=["\\Xt "]
r2c[chr(127)+chr(243)]=["\\Yt "]
r2c[chr(127)+chr(13)]=["\\D Start"]
r2c[chr(127)+chr(14)]=["\\D End"]
r2c[chr(127)+chr(15)]=["\\D pitch"]
r2c[chr(127)+chr(145)]=["\\F Start"]
r2c[chr(127)+chr(146)]=["\\F End"]
r2c[chr(127)+chr(147)]=["\\F pitch"]
r2c[chr(127)+chr(144)]=["\\F Result"]
r2c[chr(127)+chr(160)]=["a\\R n"]
r2c[chr(127)+chr(161)]=["a\\R n+"]
r2c[chr(127)+chr(167)]=["b\\R n"]
r2c[chr(127)+chr(168)]=["b\\R n+"]
r2c[chr(127)+chr(149)]=["\\R Start"]
r2c[chr(127)+chr(150)]=["\\R End"]
r2c[chr(127)+chr(173)]=["a\\R nStart"]
r2c[chr(127)+chr(174)]=["b\\R nStart"]
r2c[chr(127)+chr(148)]=["\\R Result"]
r2c[chr(127)+chr(98)]=["\\Sim Result"]
r2c[chr(127)+chr(96)]=["\\Sim Coef"]
r2c[chr(127)+chr(99)]=["\\Ply Result"]
r2c[chr(127)+chr(97)]=["\\Ply Coef"]

## Octet-Raw2Cat
r2c[chr(134)]=["\\sqrt"]
r2c[chr(139)]=["\\x^2"]
r2c[chr(184)]=["\\xrt"]
r2c[chr(206)]=["\\theta"]
r2c[chr(181)]=["\\10^x"]
r2c[chr(149)]=["\\log "]
r2c[chr(165)]=["\\e^x"]
r2c[chr(145)]=["\\asin "]
r2c[chr(129)]=["\\sin "]
r2c[chr(146)]=["\\acos "]
r2c[chr(130)]=["\\cos "]
r2c[chr(147)]=["\\atan "]
r2c[chr(131)]=["\\tan "]
r2c[chr(187)]=["\\ab/c"]
r2c[chr(150)]=["\\curt"]
r2c[chr(14)]=["\\->"]
r2c[chr(208)]=["\\Pi"]
r2c[chr(192)]=["\\Ans"]
r2c[chr(15)]=["\\EE"]
r2c[chr(135)]=["\\(-)"]
## MUST BE DO BEFORE (raw2cat) or AFTER (cat2raw) the convertion
#r2c["*"]=["\\aster"]
#r2c[chr(47)]=["\\slash"]
# fichier prog-chln
r2c[chr(133)]=["\\In "] ## !!CAT \\In for ln
r2c[chr(151)]=["\\Abs "]
r2c[chr(141)]=["\\Integral("]
r2c[chr(203)]=["\\Eox"]
r2c[chr(204)]=["\\Eoy"]
r2c[chr(161)]=["\\sinh "]
r2c[chr(162)]=["\\cosh "]
r2c[chr(163)]=["\\tanh "]
r2c[chr(177)]=["\\asinh "]
r2c[chr(178)]=["\\acosh "]
r2c[chr(179)]=["\\atanh "]
r2c[chr(171)]=["!"] # (!= "!")
r2c[chr(136)]=["\\nPr"]
r2c[chr(152)]=["\\nCr"]
r2c[chr(193)]=["\\Ran#"]
r2c[chr(251)]=["\\ProbP("] ## v0.1.2
r2c[chr(252)]=["\\ProbQ("] ## v0.2
r2c[chr(253)]=["\\ProbR("] ## v0.2
r2c[chr(254)]=["\\ProbT("] ## v0.2
r2c[chr(151)]=["\\Abs "]
r2c[chr(166)]=["\\Int "]
r2c[chr(182)]=["\\Frac "]
r2c[chr(211)]=["\\Rnd"]
r2c[chr(222)]=["\\Intg "]
r2c[chr(156)]=["\\Deg>"]
r2c[chr(172)]=["\\Rad>"]
r2c[chr(188)]=["\\Gra>"]
r2c[chr(140)]=["\\Dms"]
r2c[chr(128)]=["\\Pol("]
r2c[chr(160)]=["\\Rec("]
r2c[chr(5)]=["\\milli"]
r2c[chr(4)]=["\\micro"]
r2c[chr(3)]=["\\nano"]
r2c[chr(2)]=["\\pico"]
r2c[chr(1)]=["\\fempto"]
r2c[chr(6)]=["\\kilo"]
r2c[chr(7)]=["\\Mega"]
r2c[chr(8)]=["\\Giga"]
r2c[chr(9)]=["\\Tera"]
r2c[chr(10)]=["\\Peta"]
r2c[chr(11)]=["\\Exa"]
r2c[chr(20)]=["\\f1"]
r2c[chr(21)]=["\\f2"]
r2c[chr(22)]=["\\f3"]
r2c[chr(23)]=["\\f4"]
r2c[chr(24)]=["\\f5"]
r2c[chr(25)]=["\\f6"]
r2c[chr(237)]=["\\Prog "]
r2c[chr(226)]=["\\Lbl "]
r2c[chr(236)]=["\\Goto"]
r2c[chr(19)]=["\\=>"]
r2c[chr(233)]=["\\Isz "]
r2c[chr(232)]=["\\Dsz "]
r2c[chr(63)]=["?"]                      # (!= "?")
r2c[chr(12)]=["\\Disp"]       # (+return to the line)
r2c[chr(17)]=["\\<>"]
r2c[chr(18)]=["\\>="]
r2c[chr(16)]=["\\<="]
r2c[chr(58)]=[":"]
r2c[chr(234)]=["\\Factor "]
r2c[chr(235)]=["\\ViewWindow "]
r2c[chr(209)]=["\\Cls"]
r2c[chr(238)]=["\\Graph Y="]
r2c[chr(244)]=["\\Graph r="]
r2c[chr(245)]=["\\Graph (X,Y)=("]
r2c[chr(239)]=["\\Graph Integral "]
r2c[chr(240)]=["\\Graph Y>"]
r2c[chr(241)]=["\\Graph Y<"]
r2c[chr(242)]=["\\Graph Y>="]
r2c[chr(243)]=["\\Graph Y<="]
r2c[chr(224)]=["\\Plot "]
r2c[chr(225)]=["\\Line"]
r2c[chr(218)]=["\\Deg"]
r2c[chr(219)]=["\\Rad"]
r2c[chr(220)]=["\\Gra"]
r2c[chr(227)]=["\\Fix "]
r2c[chr(228)]=["\\Sci "]
r2c[chr(217)]=["\\Norm"]
r2c[chr(221)]=["\\Eng"]
r2c[chr(159)]=["\\Sumx"]
r2c[chr(143)]=["\\Sumx2"]
r2c[chr(196)]=["\\Sdx"]
r2c[chr(197)]=["\\Sdxn"]
r2c[chr(174)]=["\\minX"]
r2c[chr(190)]=["\\maxX"]
r2c[chr(195)]=["\\Meany"]
r2c[chr(207)]=["\\Sumy"]
r2c[chr(191)]=["\\Sumy2"]
r2c[chr(223)]=["\\Sumxy"]
r2c[chr(198)]=["\\Sdy"]
r2c[chr(199)]=["\\Sdyn"]
r2c[chr(173)]=["\\minY"]
r2c[chr(189)]=["\\maxY"]
r2c[chr(158)]=["\\Med"]
r2c[chr(142)]=["\\Mo"]
r2c[chr(175)]=["\\Cnt"]
r2c[chr(202)]=["\\Cor"]
r2c[chr(205)]=["\\r"]
# Base cat equiv
r2c['\x1a'] = ["\\hA"]
r2c['\x1b'] = ["\\hB"]
r2c['\x1c'] = ["\\hC"]
r2c['\x1d'] = ["\\hD"]
r2c['\x1e'] = ["\\hE"]
r2c['\x1f'] = ["\\hF"]
r2c['\x94'] = ["\\Dec>"]
r2c['\x84'] = ["\\Hex>"]
r2c['\xb4'] = ["\\Bin>"]
r2c['\xa4'] = ["\\Oct>"]
r2c['\xb7'] = ["\\Neg "]
r2c['\xa7'] = ["\\ Not "]
r2c['\xba'] = ["\\and"]
r2c['\xaa'] = ["\\or"]
r2c['\x9a'] = ["\\xor"]
r2c['\x8a'] = ["\\xnor"]
r2c['\xd4'] = ["\\Dec"]
r2c['\xd5'] = ["\\Hex"]
r2c['\xd6'] = ["\\Bin"]
r2c['\xd7'] = ["\\Oct"]

## Raw2Cat Last
r2c[chr(168)]=["^"]
r2c[chr(169)]=["*"] # (!= "*")
r2c[chr(185)]=["/"]
r2c[chr(137)]=["+"]
r2c[chr(153)]=["-"]
r2c[chr(61)]=["="]
r2c[chr(62)]=[">"]
r2c[chr(60)]=["<"]


## New cat (r2n)
r2n = {}
r2n[chr(247)+chr(5)]=["\\ To ", " \\To "]
r2n[chr(12)]=["\\Disp" + chr(13), "\\Disp"]       # (+return to the line)
r2n[chr(15)]=["\\E"]
r2n[chr(236)]=["\\Goto "]
r2n[chr(247)+chr(7)]=["\\Next"]
r2n[chr(247)+chr(51)]=["\\Sinusoidal"]
r2n[chr(247)+chr(50)]=["\\NPPlot"]
r2n[chr(247)+chr(53)]=["\\Logistic"]
r2n[chr(247)+chr(52)]=["\\SinReg "]
r2n[chr(247)+chr(54)]=["\\LogisticReg "]
r2n[chr(247)+chr(146)]=["a\\R n+2Type"]
r2n[chr(127)+chr(82)]=["\\DeltaList "]
r2n[chr(247)+chr(118)]=["\\Resid-None"]
r2n[chr(247)+chr(119)]=["\\Resid-List "]
r2n[chr(127)+chr(151)]=["\\H Start"]
r2n[chr(127)+chr(152)]=["\\H pitch"]
r2n[chr(127)+chr(192)]=["\\n1"]
r2n[chr(127)+chr(193)]=["\\n2"]
r2n[chr(127)+chr(194)]=["\\Test x1"]
r2n[chr(127)+chr(195)]=["\\Test x2"]
r2n[chr(127)+chr(196)]=["\\x1on-1"]
r2n[chr(127)+chr(197)]=["\\x2on-1"]
r2n[chr(127)+chr(198)]=["\\xpon-1"]
r2n[chr(127)+chr(208)]=["\\{F}"]
r2n[chr(127)+chr(215)]=["\\Fdf"]
r2n[chr(127)+chr(218)]=["\\SS"]
r2n[chr(127)+chr(219)]=["\\MS"]
r2n[chr(127)+chr(216)]=["\\Edf"]
r2n[chr(127)+chr(220)]=["\\SSe"]
r2n[chr(127)+chr(221)]=["\\MSe"]
r2n[chr(127)+chr(210)]=["\\{p}"]
r2n[chr(127)+chr(209)]=["\\{z}"]
r2n[chr(127)+chr(211)]=["\\{t}"]
r2n[chr(127)+chr(213)]=["\\chi2"]
r2n[chr(127)+chr(202)]=["\\Left"]
r2n[chr(127)+chr(203)]=["\\Right"]
r2n[chr(127)+chr(199)]=["\\p^"]
r2n[chr(127)+chr(200)]=["\\p^1"]
r2n[chr(127)+chr(201)]=["\\p^2"]
r2n[chr(127)+chr(217)]=["\\df"]
r2n[chr(127)+chr(212)]=["\\{s}"]
r2n[chr(127)+chr(214)]=["\\r2"]
r2n[chr(127)+chr(244)]=["\\{X}"]
r2n[chr(127)+chr(164)]=["\\a0"]
r2n[chr(127)+chr(166)]=["\\a1"]
r2n[chr(127)+chr(165)]=["\\a2"]
r2n[chr(127)+chr(170)]=["\\b0"]
r2n[chr(127)+chr(171)]=["\\b1"]
r2n[chr(127)+chr(172)]=["\\b2"]
r2n[chr(127)+chr(100)]=["\\{n}"] # Conflict with \nCr and others
r2n[chr(127)+chr(101)]=["\\I%"]
r2n[chr(127)+chr(102)]=["\\PV"]
r2n[chr(127)+chr(103)]=["\\PMT"]
r2n[chr(127)+chr(104)]=["\\FV"]
r2n[chr(127)+chr(204)]=["\\P/Y"]
r2n[chr(127)+chr(205)]=["\\C/Y"]
r2n[chr(127)+chr(29)]=["\\{c}"]
r2n[chr(127)+chr(30)]=["\\{d}"]
r2n[chr(127)+chr(31)]=["\\{e}"]
r2n[chr(127)+chr(162)]=["a\\R n+2"]
r2n[chr(127)+chr(169)]=["b\\R n+2"]

## Octet-Raw2NewCat dic (r2c)
# r2n[chr(139)]=["\\r"]
r2n[chr(205)]=["\\r", "\\(r)"]
r2n[chr(200)]=["\\{a}"]
r2n[chr(201)]=["\\{b}"]
r2n[chr(202)]=["\\S r"]
r2n[chr(133)]=["\\Ln "] # en plus de cat "\In "
r2n[chr(133)]=["\\ln "] # en plus de cat "\In "
r2n[chr(0x9B)]=["\\x^-1"] # New cat equiv for x^-1 (155 - 0x9B) ## v0.1.2
r2n[chr(0xC2)]=["\\Meanx"]

# List for base characters (to detect base programs):
base_car = ['\x1a', '\x1b', '\x1c', '\x1d', '\x1e', '\x1f', '\x94', '\x84', \
            '\xb4', '\xa4', '\xb7', '\xa7', '\xba', '\xaa', '\x9a', '\x8a', \
            '\xd4', '\xd5', '\xd6', '\xd7']

# Dictionary for equivalences into title and passwords
title_equiv = {}
title_equiv['\xCD'] = '\\(r)'
title_equiv['\xCE'] = '\\theta'
title_equiv['\x89'] = '+'
title_equiv['\x99'] = '-'
title_equiv['\xA9'] = '*'
title_equiv['\xB9'] = '/'
#vim:ai:et:sw=4:ts=4:sts=4:tw=78:fenc=utf-8
