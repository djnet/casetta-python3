# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release (code from __init__.py)
#   o Add G1rFile class
#   o Add the DOTALL flag in regular expressions
#   o Extract pictures from g1r files
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""New Fa-124 files formats (such as g1r) manager"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import re
from . import data as datacls
from . import pictures

class G1rFile:
    """g1r (Fa124) file format manager."""
    
    name = 'g1r'
    managed_data = [datacls.Program, datacls.Picture]
    read = True
    write = False
    list_ext = ['g1r']
    def __init__(self, filename = None):
        """Make a g1r file object with filename"""
        self.filename = filename
    
    def open_file(self):
        """Open a file in the g1r Format"""
        g1r_file = open(self.filename, 'r')
        # Build the FileData
        file_data = datacls.FileData()
        ctn = g1r_file.read()
        g1r_file.close()
        # Extract programs:
        # Make the regular expression
        data_pattern = re.compile('''
        (PROGRAM)      # type of data
        [\x00]{12}     # Separator
        [\x01]         # one 0x01 character
        system         # The 'system' string
        [\x00]{2}      # Separator
        ([^\x00]{1,8}) # data name
        [\x00]{0,7}    # to complete the data name
        [\x01]         # end of data name
        [\x00]{2}      # Separator
        .{2}           # two characters
        [\x00]{11}     # Separator
        (\x00|\x01)    # Base (01) or not (00)
        \x00           # Separator
        (.*[^\x00])    # any characters ended by a non 0x00 : raw data 
                       #(w/o the 0xFF end character)
        [\x00]{2}      # end of the record
        ''', re.VERBOSE | re.DOTALL)
        # extract data from g1r_file
        data_list = data_pattern.findall(ctn)
        for record in data_list:
            # record = ('DATA TYPE', 'FILE NAME', 'BASE', 'RAW DATA')
            if record[0] == 'PROGRAM' :
                new_data = file_data.new_record(datacls.Program)
                new_data.name = record[1]
                if record[2] == '\x01':
                    new_data.use_base = True
                else:
                    new_data.use_base = False
                new_data.raw_data = record[3] + '\x0d\xff'
        # Extract pictures:
        # Make the regular expression
        picture_pattern = re.compile('''
        (PICTURE[ ][1-6])   # data name
        [\x00]{10}          # Separator
        [\x01]              # one 0x01 character
        main                # The 'main' string
        [\x00]{4}           # Separator
        (PICT[1-6])         # name (#2)
        [\x00]{3}           # Separator
        [\x07][\x00]{2}
        [\x08][\x00]{4}
        (.{1024})           # Sheet1
        (.{1024})           # Sheet2
        ''', re.VERBOSE | re.DOTALL)
        picture_list = picture_pattern.findall(ctn)
        for record in picture_list:
            # record = ('DATA NAME #1', 'DATA NAME #2', 'SHEET1', 'SHEET2')
            new_data = file_data.new_record(datacls.Picture)
            new_data.name = 'Picture' + record[0][-1]
            new_data.pallet = pictures.DEFAULT_PALLET
            new_data.color_byte = 0
            new_data.set_picture(g1r_picture_to_raw(record[2], record[3]))
        return file_data

def g1r_picture_to_raw(sheet1, sheet2):
    """Return a list of pixel from two g1r sheets"""
    pixel_list = []
    for byte_index in range(len(sheet1)):
        byte1 = pictures.bin(ord(sheet1[byte_index]))
        byte2 = pictures.bin(ord(sheet2[byte_index]))
        for bit_index in range(8):
            bit1 = byte1[bit_index]
            bit2 = byte2[bit_index]
            if bit1 == '1':
                pixel_list.append(pictures.BLUE)
            elif bit2 == '1':
                pixel_list.append(pictures.GREEN)
            else:
                pixel_list.append(pictures.WHITE)
    return pixel_list
