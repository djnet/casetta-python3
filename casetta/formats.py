# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release (code from __init__.py)
#   o Add Image file type, pictures and screencaptures data type
#   o Allow cafix to manage pictures and screencaptures
#   o Allow fxi to write data and manage pictures
#   o Add the new transfer tool
#   o Organization changes:
#       o Data related informations have been moved inside data.py
#       o Format related informations are now inside each format class
#       o check_no_managed_data is now in data.FileData
#       o the list of formats is format_list and contain format classes
#       o the choose_format function raise FormatNotFound instead
#         returning False if the format is cannot be found
#       o open a file can be done by making a new data.FileData
#   o Register cas (not casemul) file format
#   o Add magic detection to choose between cas and casemul formats
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Data management classes for casetta"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import os

from . import cat
from . import cafix
from . import fa124
from . import ctfdic
from . import fxi
from . import casemul
from . import pil
from . import cas
from . import errors

def choose_format(filename):
    """Return the format class corresponding of filename"""
    # If .cas file, choose between cas and casemul
    if filename.lower().endswith('.cas'):
        # Check if the file exist
        if os.path.exists(filename):
            dot_cas_file = open(filename, 'r')
            magic = dot_cas_file.read(4)
            dot_cas_file.close()
            if magic == 'ACFS':
                # Casmul format
                return format_dic['casemul']
            elif magic.startswith(':'):
                # Cas format
                return format_dic['cas']
            else:
                # Unknown .cas format
                raise errors.FormatNotFound()
    # Other formats
    for format in format_list:
        for ext in format.list_ext:
            if filename[-len(ext)-1:].lower() == '.' + ext :
                return format
    raise errors.FormatNotFound()

# list of formats
format_list = [cafix.CafixFile, \
               casemul.CasemulFile, \
               cat.CatFile, \
               cat.NewcatFile, \
               ctfdic.CtfFile, \
               fa124.G1rFile, \
               fxi.FxiFile, \
               fxi.FxbFile, \
               fxi.FxmFile, \
               fxi.FxdFile, \
               pil.ImageFile, \
               cas.CasFile]
# Make format_dic:
format_dic = {}
for aformat in format_list:
    format_dic[aformat.name] = aformat
