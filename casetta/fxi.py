# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release (code from __init__.py)
#   o Use the right behaviour with the logical or
#   o Write access for fxi files
#   o Allow to load too-long-name programs, but cut the name after 8 characters
#   o Add the management of pictures in read/write for fxi
#   o Add file format classes
#   o Add the management of pictures / screencaptures in read only for fxd
#   o Add the DOTALL flag in regular expressions
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Fx-Interface (pro or not) formats management module"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import re
import os
import base64
from . import data as datacls
from . import cas

FXI_PALLET = ['o', 'b', 'g', 'w']

# Build the raw2fxi dic
# Thanks to http://www.casioland.net/tutoriaux.php?idTuto=3&noHeader=1
r2f = {}
dep, chrNumber, fxi_index, fxi_index2 = 23, 255, 0, 0
chr_value = dep
while chrNumber >= 0:
    chr_value = chr_value + 1
    r2f[chrNumber] = chr_value
    chrNumber -= 1
    fxi_index += 1
    if fxi_index == 8:
        dep -= 8
        fxi_index2 += 1
        fxi_index = 0
        chr_value = dep
    if fxi_index2 == 4:
        fxi_index2 = 0
        dep += 8 * 8
        chr_value = dep

# Build the fxi2raw dic
f2r = {}
for ascii, equiv in list(r2f.items()):
    f2r[equiv] = ascii

# Decode / Encode functions
def fxi_encode(raw_data):
    """Convert raw data to fxi-like data"""
    out = ''
    for car in range(len(raw_data)):
        out += chr(r2f[ord(raw_data[car])])
    return out

def fxi_decode(fxi_data):
    """Convert fxi-like data to raw data"""
    out = ''
    for car in range(len(fxi_data)):
        out += chr(f2r[ord(fxi_data[car])])
    return out

# File formats management classes
class FxiFile:
    """Fxi (Fx-Interface) file format manager."""
    
    name = 'fxi'
    managed_data = [datacls.Program, datacls.Picture]
    read = True
    write = True
    list_ext = ['fxi']
    def __init__(self, filename = None):
        """Make a fxi file object with filename"""
        self.filename = filename
        
    def open_file(self):
        """Open a file in the fxi Format (new function using re)"""
        fxi_file = open(self.filename, 'r')
        # Make a new FileData
        file_data = datacls.FileData()
        # Get the raw data
        ctn = fxi_decode(fxi_file.read())
        fxi_file.close()
        # Make the regular expression for programs
        data_pattern = re.compile('''
        (TXT)           # data format (but fxi txt is not really text :-/)
        .               # separator
        (PG)            # type of data
        .{4}            # 4 characters
        ([^\xff]+)      # data name (old:([^\xff]{1,8}))
        \xff{1,23}      # between 8 and 23 (16+8-len(data name)) 0xFF (o:{16,23})
        (NL|BN)         # Option1 (Normal or Base)
        \xff{12}        # 12 0xFF
        ([^\xff]+\xff)  # any non 0xFF characters ended by one 0xFF:
                        # raw data
        ''', re.VERBOSE | re.DOTALL)
        pict_pattern = re.compile('''
        (IMG)           # data format
        .               # separator
        (PC)            # type of data
        .{4}            # 4 characters
        ([^\xff]+)      # data name (old:([^\xff]{1,8}))
        \xff{1,23}      # between 8 and 23 (16+8-len(data name)) 0xFF (o:{16,23})
        (DRUWF)         # Sens of reading
        .{4}            # 4 bytes
        \xff{13}        # Separator
        (.{4112})       # (( sheet of 16 * 64 byte ) + 4 header byte) 
                        #* 4 colors
        ''', re.VERBOSE | re.DOTALL)
        # Extract records from raw data using the regular expression
        data_list = data_pattern.findall(ctn) + pict_pattern.findall(ctn)
        for record in data_list:
            # record = (FORMAT, DATA_TYPE, FILE_NAME, OPTION, RAW_DATA)
            if record[1] == 'PG' :
                new_data = file_data.new_record(datacls.Program)
                new_data.name = record[2][:8]
                if record[3] == 'BN':
                    new_data.use_base = True
                else:
                    new_data.use_base = False
                new_data.raw_data = record[4]
            if record[1] == 'PC' :
                new_data = file_data.new_record(datacls.Picture)
                new_data.name = record[2][:8]
                new_data.pallet = FXI_PALLET
                new_data.color_byte = 3
                new_data.raw_data = record[4]
        return file_data

    def save(self, data_list):
        """Save a FileData in a fxi file"""
        fxi_file = open(self.filename, 'w')
        # For each data, convert in the casemul format
        # Make the file header
        ctn = '\xd5\xd7\x1fFX-INTERFACE - YELLOW COMPUTING'
        ctn += '\x00'*40 + '\x06' + '\x00'*81 + '\x07\x00'
        ctn += '\xff'*2 + '\x00'*2 + '\x0f\x00CYCFXListItemNP' + '\x00'*8
        ctn += '\x03\x00\x15\x18' + '\x00'*5 + '\x4a\xa0' + '\x00'*76
        ctn += '\x01\x80\x00\x00\x01' + '\x00'*3 + '\x01\x00\x03\x00\x55\x1a'
        ctn += '\x00'*2 + '\x04' + '\x00'*2 + '\x5b\x80' + '\x00'*76
        ctn += '\x01\x80' + '\x00'*2 + '\x02' + '\x00'*3 + '\x02' + '\x00'*3
        ctn += '\x08\x13' + '\x00'*2 + '\x22\x00\x00\x83\x80' + '\x00'*76
        ctn += '\x01\x80' + '\x00'*2 + '\x03' + '\x00'*3 + '\x01\x00\x03\x00'
        ctn += '\x55\x1a\x00\x00' + '\x20' + '\x00'*2 + '\x1f\x81' + '\x00'*76
        ctn += '\x01\x80' + '\x00'*2 + '\x04' + '\x00'*3 + '\x02' + '\x00'*3
        ctn += '\x08\x13' + '\x00'*2 + '\x21' + '\x00'*2 + '\x83\x80'
        ctn += '\x00'*76 + '\x01\x80' + '\x00'*2 + '\x05' + '\x00'*5
        ctn += '\x02\x00\x58\x10' + '\x00'*2 + '\x1e' + '\x00'*2 + '\xcc\x80'
        ctn += '\x00'*76 + '\x01\x80' + '\x00'*2 + '\x06' + '\x00'*3 + '\x01'
        ctn += '\x00'*3 + '\x08\x11' + '\x00'*2 + '\x1e' + '\x00'*2
        ctn += '\x83\x80' + '\x00'*76
        # Sort programs/pictures
        prgm_list, pict_list = [], []
        for data in data_list:
            if data.__class__ == datacls.Program:
                prgm_list.append(data)
            elif data.__class__ == datacls.Picture:
                pict_list.append(data)
        # Add program block
        if len(prgm_list) > 0:
            # Block header
            ctn += chr(len(prgm_list)) + '\x00'
            # Make records
            for data in prgm_list:
                # Lengths calculs
                len_prgm = len(data.raw_data)
                len1 = '0'*(4 - len(hex(len_prgm + 0x30)[2:]))
                len1 += hex(len_prgm + 0x30)[2:]
                len1str = chr(int(len1[2:], 16)) + chr(int(len1[:2], 16))
                len2 = '0'*(8 - len(hex(len_prgm + 2)[2:]))
                len2 += hex(len_prgm + 2)[2:]
                len2str =  chr(int(len2[0:2], 16)) + chr(int(len2[2:4], 16))
                len2str += chr(int(len2[4:6], 16)) + chr(int(len2[6:8], 16))
                # Make the record header
                ctn += '\x01\x80' + '\x00'*6 + '\x02\x00\x01\x00\xba\x1e'
                ctn += '\x00\x00\x22\x00'
                ctn += chr(len(data.name))          # Length of the name
                ctn += data.name                    # Name (length variable)
                ctn += '\x83\x80' + '\x00' * 0x2a
                ctn += data.name                    # Name, #2
                ctn += '\xff' * (8 - len(data.name))# To complete the name
                ctn += '\x00' * 0x18 + '\x01\x00'*2
                ctn += len1str                      # Length of the data
                ctn += 'TXT'                        # Data format (non sense,
                                                    # it's in raw format).
                ctn += '\x00'
                ctn += 'PG'                         # Data type
                ctn += len2str                      # Length of the data, #2
                ctn += data.name                    # Name, #3
                ctn += '\xff' * (9 - len(data.name))# To complete the name
                ctn += '\xff' * 15
                if data.use_base:
                    ctn += 'BN'
                else:
                    ctn += 'NL'
                ctn += '\xff' * 12
                # Add the record body
                ctn += data.raw_data
        # Add picture block
        if len(pict_list) > 0:
            # Block header
            if len(prgm_list) == 0:
                ctn += '\x00' * 2
            ctn += '\x00' * 4
            ctn += chr(len(pict_list)) + '\x00'
            # Make records
            for data in pict_list:
                # Make the record header
                ctn += '\x01\x80' + '\x00'*6 + '\x02\x00\x01\x00\x3a\x1c'
                ctn += '\x00\x00\x21\x00'
                ctn += '\x08'                       # Length of the name (cst)
                ctn += data.name                    # Name (length = 8)
                ctn += '\x83\x80' + '\x00' * 0x2a
                ctn += data.name                    # Name, #2
                ctn += '\xff' * (8 - len(data.name))# To complete the name
                ctn += '\x00' * 0x18 + '\x01\x00'*2
                ctn += '\x40\x10'                   # Length of the data
                ctn += 'IMG'                        # Data format
                ctn += '\x00'
                ctn += 'PC'                         # Data type
                ctn += '\x00\x40\x00\x80'           # Length of the data, #2
                ctn += data.name                    # Name, #3
                ctn += '\xff' * 8
                ctn += 'DRUWF'
                ctn += '\x00\x04\x00\x01' + '\xff' * 13
                # Add the record body
                data.change_pallet(FXI_PALLET, ['\x00\x01\x00'] * 4)
                ctn += data.raw_data
        # Add the file footer
        ctn += '\x00' * 7
        # Convert in fxi encodage
        ctn = fxi_encode(ctn)
        # Write file
        fxi_file.write(ctn)
        fxi_file.close()

class FxbFile:
    """Fxb (Fx-Interface Backup) file format manager."""
    
    name = 'fxb'
    managed_data = [datacls.Backup]
    read = True
    write = True
    list_ext = ['fxb']
    def __init__(self, filename = None):
        """Make a fxb file object with filename"""
        self.filename = filename

    def open_file(self):
        """Open a backup in FX-Interface backup (fxb) format"""
        fxb_file = open(self.filename, 'r')
        ctn = fxb_file.read()
        fxb_file.close()
        file_data = datacls.FileData()
        # Delete the 10 FF in the header
        ctn = ctn.replace(10*chr(0xFF), '', 1)
        # Split by 9 FF
        bak = (9*chr(0xFF)).join(ctn.split(9*chr(0xFF))[1:])
        new_data = file_data.new_record(datacls.Backup)
        new_data.raw_data = bak
        new_data.name = os.path.basename(self.filename)[:-4]
        return file_data

    def save(self, data_list):
        """Save a file in the fxb Format"""
        folder = os.path.dirname(self.filename)
        # For each data
        for data in [data for data in data_list if \
                     data.__class__ == datacls.Backup]:
            # make a filename from the metadata
            if len(data_list) == 1 and os.path.basename(self.filename) != '':
                # try to use directory as name
                filename = self.filename
            else:
                if data.date == None:
                    date_prefix = ''
                else:
                    date_prefix = data.date.strftime('%Y%m%d') + '-'
                filename = os.path.join(folder, date_prefix + data.name + \
                                        '.fxb')
            # fxb header:
            ctn = base64.decodestring("""
iAABAAAARlggQkFDSyBVUCBGSUxFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABA+AAACwAXAIEaMgEAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZAAAAP7///9cAAAAAAAAEUNBU0lPIEZYIEJBQ0sgVVAg
REFUQQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuAQAAAAEABgAl
AAEAAAAAABAARGF0YQBA+AAAPPgAAAEAAAA0+AAAMPgAAE1FTQBCVQAA+AJCYWNrdXD/////////
////R1kzNTj//4AACCAAAP///////////w==""")
            ctn += data.raw_data
            fxb_file = open(filename, 'w')
            fxb_file.write(ctn)
            fxb_file.close()

class FxmFile:
    """Fxm (Fx-Interface Pro Backup) format manager."""
    
    name = 'fxm'
    managed_data = [datacls.Backup]
    read = True
    write = False
    list_ext = ['fxm']
    def __init__(self, filename = None):
        """Make a fxb file object with filename"""
        self.filename = filename

    def open_file(self):
        """Open a backup in Fx-Interface Pro backup (fxm) format"""
        fxm_file = open(self.filename, 'r')
        ctn = fxm_file.read(67706)
        fxm_file.close()
        file_data = datacls.FileData()
        bak = ctn[4218:]
        new_data = file_data.new_record(datacls.Backup)
        new_data.raw_data = bak
        new_data.name = os.path.basename(self.filename)[:-4]
        return file_data

class FxdFile:
    """Fxd (Fx-Interface Pro) file format manager."""
    
    name = 'fxd'
    managed_data = [datacls.Program, datacls.Picture, datacls.ScreenCapture]
    read = True
    write = False
    list_ext = ['fxd']
    def __init__(self, filename = None):
        """Make a fxd file object with filename"""
        self.filename = filename

    def open_file(self):
        """Open a file in the fxd Format"""
        fxd_file = open(self.filename, 'r')
        ctn = fxd_file.read()
        fxd_file.close()
        # Build the FileData
        file_data = datacls.FileData()
        # Make the regular expression for programs
        program_pattern = re.compile('''
        (PG)            # type of data
        .{4}            # 4 characters
        ([^\xff]{1,8})  # data name
        \xff{16,23}     # between 16 and 23 (16+8-len(data name)) 0xFF
        (NL|BN)         # Option1 (Normal or Base)
        \xff{13}        # 13 0xFF
        .{8}            # 8 characters
        ([^\xff]+\xff)  # any non 0xFF characters ended by one 0xFF : raw data
        ''', re.VERBOSE | re.DOTALL)
        # extract programs from fxd_file
        program_list = program_pattern.findall(ctn)
        for record in program_list:
            # record = ('DATA TYPE', 'FILE NAME', 'OPTION', 'RAW DATA')
            if record[0] == 'PG' :
                new_data = file_data.new_record(datacls.Program)
                new_data.name = record[1]
                if record[2] == 'BN':
                    new_data.use_base = True
                else:
                    new_data.use_base = False
                new_data.raw_data = record[3]
        # Make the regular expression for pictures
        pictures_pattern = re.compile('''
        (Picture[1-6])      # data name
        \xee\x03\x00\x00    # Some data (?)
        \(\x00\x00\x00\x01\x00\x00\x00
        \x80\x00\x00\x00\x40\x00\x00\x00
        \x01\x00\x00\x00\x04\x00\x00\x00
        \x04\x00\x00\x00\x55\x00\x00\x00
        F\x00\x00\x00W\x00\x00\x00\x00
        \x10\x00\x00
        (.{1024})           # First sheet
        (.{1024})           # Second sheet
        (.{1024})           # Third sheet
        (.{1024})           # Last sheet
        \xee\x03\xef\xfe
        \xae\x0f\xef\xfe\x03\x00\xef\xfe
        ''', re.VERBOSE | re.DOTALL)
        # extract pictures from fxd_file
        pictures_list = pictures_pattern.findall(ctn)
        for record in pictures_list:
            # record = ('FILE NAME', 'SHEET1', 'SHEET2', 'SHEET3', 'SHEET4')
            new_data = file_data.new_record(datacls.Picture)
            new_data.name = record[0]
            new_data.raw_data = "\x01" + record[1] + \
                                "\x02" + record[2] + \
                                "\x03" + record[3] + \
                                "\x04" + record[4]
            new_data.pallet = FXI_PALLET
            new_data.color_byte = 0
        # Make the regular expression for mono screen captures
        mono_sc_pattern = re.compile('''
        \x00([^\x00]+)      # data name
        \xee\x03\x00\x00    # some data (?)
        \(\x00\x00\x00\x02\x00\x00\x00
        \x80\x00\x00\x00\x40\x00\x00\x00
        \x01\x00\x00\x00\x01\x00\x00\x00
        \x02\x00\x00\x00\x44\x00\x00\x00
        F\x00\x00\x00W\x00\x00\x00\x00
        \x04\x00\x00
        (.{1024})           # First sheet
        \xee\x03\xef\xfe
        \xb1\x0f\xef\xfe\x03\x00\xef\xfe
        ''', re.VERBOSE | re.DOTALL)
        # extract mono screen capture from fxd_file
        mono_sc_list = mono_sc_pattern.findall(ctn)
        for record in mono_sc_list:
            # record = ('FILE NAME', 'SHEET1')
            new_data = file_data.new_record(datacls.ScreenCapture)
            new_data.name = record[0]
            new_data.raw_data = cas.mono_screencapture_to_raw(record[1])
            new_data.pallet = cas.SCREEN_PALLET
            new_data.color_byte = 0
        # Make the regular expression for color screen captures
        color_sc_pattern = re.compile('''
        \x00([^\x00]+)      # data name
        \xee\x03\x00\x00    # some data (?)
        \(\x00\x00\x00\x02\x00\x00\x00
        \x80\x00\x00\x00\x40\x00\x00\x00
        \x01\x00\x00\x00\x03\x00\x00\x00
        \x04\x00\x00\x00\x55\x00\x00\x00
        F\x00\x00\x00W\x00\x00\x00\x00
        [\x0c]\x00\x00
        (.{1024})           # First sheet
        (.{1024})           # Second sheet
        (.{1024})           # Third sheet
        \xee\x03\xef\xfe
        \xb1\x0f\xef\xfe\x03\x00\xef\xfe
        ''', re.VERBOSE | re.DOTALL)
        # extract color screen capture from fxd_file
        color_sc_list = color_sc_pattern.findall(ctn)
        for record in color_sc_list:
            # record = ('FILE NAME', 'SHEET1', 'SHEET2', 'SHEET3')
            new_data = file_data.new_record(datacls.ScreenCapture)
            new_data.name = record[0]
            new_data.raw_data = "\x01" + record[1] + \
                                "\x02" + record[2] + \
                                "\x03" + ('\x00' * 1024) + \
                                "\x04" + record[3]
            new_data.pallet = cas.SCREEN_PALLET
            new_data.color_byte = 0
        return file_data
