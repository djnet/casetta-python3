# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First version, code from catdic.py
#   o Add the support of pictures in read/write for cat
#   o Add the newcat password/use base properties only if needed
#   o Let to use any end of line in newcat
#   o Add the support of pictures/screen captures in rw for newcat
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""File management classes form cat and newcat formats"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import base64
import datetime
from . import catdic
from . import pictures
from . import data as datacls

# File format management classes
class CatFile:
    """Cat (Fa122/Fa123/Fa124) file format manager."""
    
    name = 'cat'
    managed_data = [datacls.Program, datacls.Backup, datacls.Picture]
    read = True
    write = True
    list_ext = ['cat']
    def __init__(self, filename = None):
        """Make a cat file object with filename"""
        self.filename = filename
        
    def open_file(self):
        """Open a file in the cat Format"""
        # Build the FileData
        file_data = datacls.FileData()
        cat_file = open(self.filename, 'r')
        ctn = cat_file.read()
        cat_file.close()
        # Split data
        for record in ctn.split('%End' + chr(13)+chr(10)):
            if '%Data Record' in record:
                header = record.split('%Data Record' + chr(13)+chr(10))[0]
                data = record.split('%Data Record' + chr(13)+chr(10))[1]
                # for each data, set metadata
                data_type = 'unknown'
                format = 'raw'
                name = 'unknown'
                use_base = False
                password = ''
                for headline in header.split(chr(13)+chr(10)):
                    label = headline.split(':')[0]
                    try:
                        value = headline.split(':')[1]
                    except IndexError:
                        value = ''
                    if label == 'Data Type' :
                        if value == 'PG':
                            data_type = 'program'
                        elif value == 'BU':
                            data_type = 'backup'
                        elif value == 'PC':
                            data_type = 'picture'
                    elif label == 'Format':
                        if value == 'TXT':
                            format = 'txt'
                        elif value == 'MEM':
                            format = 'raw16'
                        elif value == 'IMG':
                            format = 'img'
                    elif label == 'File Name' or label == 'Data Name':
                        name = value
                    elif label == 'Password':
                        password = value
                    elif label == 'Option1' and value == 'BN':
                        use_base = True
                # Creat the data
                if data_type == 'program':
                    new_data = file_data.new_record(datacls.Program)
                    new_data.password = catdic.cat_decode(password, True)
                    new_data.use_base = use_base
                elif data_type == 'backup':
                    new_data = file_data.new_record(datacls.Program)
                elif data_type == 'picture':
                    new_data = file_data.new_record(datacls.Picture)
                else:
                    new_data = file_data.new_record(datacls.Data)
                new_data.name = catdic.cat_decode(name, True)
                if format == 'raw':
                    new_data.raw_data = data
                elif format == 'txt':
                    new_data.raw_data = catdic.cat_decode(data)
                elif format == 'raw16':
                    new_data.raw_data = catdic.catb16_decode(data)
                elif format == 'img':
                    sheets = catdic.img_decode(data)
                    new_data.color_byte = 0
                    new_data.pallet = catdic.CAT_PALLET
                    new_data.raw_data = pictures.sheets_to_raw(sheets)
        return file_data

    def save(self, data_list):
        """Save a file in the cat Format"""
        cat_file = open(self.filename, 'w')
        ctn = ''
        # For each data, convert in the cat format
        for data in data_list:
            data_type = ''
            format = ''
            if data.__class__ == datacls.Program:
                data_type = "PG"
                format = "TXT"
            elif data.__class__ == datacls.Backup:
                data_type = "BU"
                format = "MEM"
            elif data.__class__ == datacls.Picture:
                data_type = "PC"
                format = "IMG"
            if data_type != '':
                # Set the header and metadata
                ctn += "%Header Record"+chr(13)+chr(10)
                ctn += "Format:" + format + chr(13)+chr(10)
                ctn += "Communication SW:0"+chr(13)+chr(10)
                ctn += "Data Type:" + data_type + chr(13)+chr(10)
                if data_type == "PC":
                    ctn += "Height:64\r\n"
                    ctn += "Width:128\r\n"
                else:
                    ctn += "Capacity:" + str(len(data.raw_data) + 2) + \
                       chr(13) + chr(10)
                if data_type == "PG":
                    ctn += "File Name:" + catdic.cat_encode(data.name, \
                           True) + chr(13) + chr(10)
                elif data_type == "BU" or data_type == "PC":
                    ctn += "Data Name:" + catdic.cat_encode(data.name, \
                            True) + chr(13) + chr(10)
                ctn += "Group Name:" + chr(13) + chr(10)
                if data_type == "PG":
                    ctn += "Password:" + catdic.cat_encode(data.password, \
                           True) + chr(13) + chr(10)
                    if data.use_base:
                        ctn += "Option1:BN" + chr(13)+chr(10)
                    else:
                        ctn += "Option1:NL" + chr(13)+chr(10)
                    ctn += "Option2:" + chr(13)+chr(10)
                    ctn += "Option3:" + chr(13)+chr(10)
                    ctn += "Option4:" + chr(13)+chr(10)
                elif data_type == "BU":
                    ctn += "Model:GY358" + chr(13)+chr(10)
                    ctn += "Option:" + chr(13)+chr(10)
                elif data_type == "PC":
                    ctn += "Start Position:DR\r\n"
                    ctn += "Direction:U\r\n"
                    ctn += "Byte Direction:W\r\n"
                    ctn += "Bit Weight:F\r\n"
                    ctn += "Colors:4\r\n"
                    ctn += "Sheets:1\r\n"
                    ctn += "Option1:\r\n"
                    ctn += "Option2:\r\n"
                    ctn += "Option3:\r\n"
                    ctn += "Option4:\r\n"
                ctn += "%Data Record" + chr(13)+chr(10)
                # Write cat data
                if format == "TXT":
                    ctn += catdic.cat_encode(data.raw_data)
                elif format == "MEM":
                    ctn += catdic.catb16_encode(data.raw_data)
                elif format == "IMG":
                    ctn += catdic.img_encode(data)
                ctn += chr(13)+chr(10) + "%End" + chr(13)+chr(10)
        # join in a cat file
        cat_file.write(ctn)
        cat_file.close()

class NewcatFile:
    """Newcat (Cassetta official format) manager."""
    
    name = 'newcat'
    managed_data = [datacls.Program, datacls.Backup, datacls.Picture, \
                    datacls.ScreenCapture]
    read = True
    write = True
    list_ext = ['newcat']
    end_of_line = '\n'
    def __init__(self, filename = None):
        """Make a newcat file object with filename"""
        self.filename = filename
    
    def open_file(self):
        """Open a file in the newcat Format"""
        # Build the file_data
        file_data = datacls.FileData()
        nc_file = open(self.filename, 'r')
        ctn = nc_file.read()
        nc_file.close()
        # Convert windows and mac end of line into unix end of line
        ctn = ctn.replace('\r\n', '\n').replace('\r', '\n')
        # Split data
        for record in ctn.split('%End\n'):
            if '%Data Record' in record:
                header, data = record.split('%Data Record\n')
                # for each data, set metadata
                data_type = 'unknown'
                format = 'RAW'
                name = 'unknown'
                date = None
                use_base = False
                password = ''
                for headline in header.split('\n'):
                    label = headline.split(':')[0]
                    try:
                        value = headline.split(':')[1]
                    except IndexError:
                        value = ''
                    if label == 'Format' :
                        format = value
                    elif label == 'Data Type':
                        if value == 'PG':
                            data_type = 'program'
                        elif value == 'BK':
                            data_type = 'backup'
                        elif value == 'PC':
                            data_type = 'picture'
                        elif value == 'SC':
                            data_type = 'screencapture'
                    elif label == 'File Name' or label == 'Data Name' or \
                         label == 'Variable Name':
                        name = value
                    elif label == 'Date':
                        date = datetime.date(int(value[:4]), \
                                             int(value[4:6]), \
                                             int(value[6:]))
                    elif label == 'Password':
                        password = value
                    elif label == 'Option1' and value == 'BN':
                        use_base = True
                # Creat the data
                if data_type == 'program':
                    new_data = file_data.new_record(datacls.Program)
                    new_data.set_password(password)
                    new_data.use_base = use_base
                elif data_type == 'backup':
                    new_data = file_data.new_record(datacls.Backup)
                elif data_type == 'picture':
                    new_data = file_data.new_record(datacls.Picture)
                elif data_type == 'screencapture':
                    new_data = file_data.new_record(datacls.ScreenCapture)
                new_data.set_name(name)
                new_data.date = date
                if format == 'RAW':
                    new_data.raw_data = data
                elif format == 'RAW64':
                    new_data.raw_data = base64.decodestring(data)
                elif format == 'TXT':
                    new_data.raw_data = catdic.text_decode(data)
                elif format == 'IMG':
                    sheets = catdic.img_decode(data)
                    new_data.color_byte = 0
                    new_data.pallet = catdic.CAT_PALLET
                    new_data.raw_data = pictures.sheets_to_raw(sheets)
                elif format == 'PICT':
                    pixel_list = catdic.pict_decode(data)
                    new_data.color_byte = 0
                    new_data.pallet = catdic.CAT_PALLET
                    new_data.set_picture(pixel_list)
        return file_data

    def save(self, data_list):
        """Save a file in the newcat Format"""
        nc_file = open(self.filename, 'w')
        ctn = ''
        # For each data, convert in the newcat format
        for data in data_list:
            data_type = ''
            if data.__class__ == datacls.Program:
                format = "TXT"
                data_type = "PG"
            elif data.__class__ == datacls.Backup:
                format = "RAW64"
                data_type = "BK"
            elif data.__class__ == datacls.Picture :
                format = "PICT"
                data_type = "PC"
            elif data.__class__ == datacls.ScreenCapture :
                format = "PICT"
                data_type = "SC"
            if data_type != '':
                # Set the header and metadata
                ctn += "%Header Record\n"
                ctn += "Format:" + format + "\n"
                # ctn += "Communication SW:0\n"
                ctn += "Data Type:" + data_type + '\n'
                # ctn += "Capacity:" + str(len(data.raw_data)) + '\n'
                ctn += "File Name:" + data.get_name() + '\n'
                # ctn += "Group Name:\n"
                if data_type == "PG" :
                    if data.password != '':
                        ctn += "Password:" + data.get_password() + '\n'
                    if data.use_base:
                        ctn += "Option1:BN\n"
                # ctn += "Option2:\n"
                # ctn += "Option3:\n"
                # ctn += "Option4:\n"
                if data.date != None :
                    ctn += "Date:" + data.date.strftime("%Y%m%d") + "\n"
                ctn += "%Data Record\n"
                # Write *New* cat data
                if format == "TXT" :
                    ctn += catdic.text_encode(data.raw_data)
                elif format == "RAW64" :
                    ctn += base64.encodestring(data.raw_data)
                elif format == "IMG":
                    ctn += catdic.img_encode(data).replace('\r\n', '\n')
                elif format == "PICT" :
                    ctn += catdic.pict_encode(data)
                ctn += "%End\n"
        # change the end of line, if needed
        if self.end_of_line != '\n':
            ctn = ctn.replace('\n', self.end_of_line)
        # join in a newcat file
        nc_file.write(ctn)
        nc_file.close()
