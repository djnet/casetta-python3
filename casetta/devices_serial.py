# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2007 Florian Birée aka Thesa <florian@biree.name>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release
#     This code is hardly inspired by cafix softwares (cafix.sourceforge.com)
#     A lot of thanks for cafix coders.
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Internal serial tansfer tool"""

__author__ = "Florian Birée"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2007, Florian Birée"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import serial
import time
import sys
from . import cas
from . import errors
from . import data as datacls

# Default functions
def default_status(data_name, current_size, total_size, is_header = False):
    """Print the status of a transfert on the standard output."""
    percent = ((current_size * 100) / total_size)
    if is_header:
        progress_line = 'Header   '
    else:
        progress_line = data_name + ' ' * (9 - len(data_name))
    progress_line += '[' + '=' * (percent / 2)
    progress_line += ' ' * (50 - (percent / 2)) + '] '
    progress_line += '(%i%%) %i/%i' % (percent, current_size, total_size)
    if current_size != 0:
        progress_line = '\r' + progress_line
    sys.stdout.write(progress_line)
    sys.stdout.flush()
    if current_size >= total_size:
        sys.stdout.write('\n')

def default_overwrite(data_name):
    """Ask the user if he want to overwrite data_name on the calculator."""
    user_rep = eval(input('Overwrite %s (Y/N): ' % data_name))
    return user_rep.lower().startswith('y')


# Serial management classes
class Connexion:
    """Casetta serial transfer tool."""
    
    name = 'serial'
    managed_data = [datacls.Program, datacls.Backup, datacls.Picture, \
                    datacls.ScreenCapture]
    def __init__(self, serial_port = 0, status = default_status, \
                 overwrite = default_overwrite):
        """Initial configuration of the serial port
        
        status is a function which is runned as status(current_data_name, 
        current_size, total_size, is_header) each time the status change.
        overwrite is a function which is runned as overwrite(data_name) and 
        which must return True or False.
        """
        if type(serial_port) == str and serial_port.isdigit():
            serial_port = int(serial_port)
        self.serial = serial.Serial(
            port=serial_port,
            baudrate=9600,
            bytesize=serial.EIGHTBITS,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            timeout=5, # or None
            xonxoff=0, #0
            rtscts=0) #(o:1)0
        self.status = status
        self.overwrite = overwrite
        self.cancel = False
    
    def check_cancel(self):
        """If the cancel flag is set, unset it and raise TransferAborted"""
        if self.cancel:
            self.cancel = False
            raise errors.TransferAborted()
    
    # low level serial functions
    def get_byte(self):
        """Read a single byte"""
        byte = self.serial.read()
        return byte

    def send_byte(self, byte):
        """Send a single byte"""
        self.serial.write(byte)
        
    def send_string(self, string, status = None, data_name = '', \
                    is_header = False):
        """Send a string of bytes"""
        for index in range(len(string)):
            self.check_cancel()
            self.send_byte(string[index])
            time.sleep(0.005)
            if status != None:
                status(data_name, index, len(string) - 1, is_header)
    
    # send system
    def send(self, data_list):
        """Send all data in data_list
        
        Warning: a backup must be the only data to send
        """
        while not self.calc_is_ready_to_receive():
            self.check_cancel()
        for data in data_list:
            self.send_data(data)
        if data_list[-1].__class__ != datacls.Backup:
            # Send the END header
            self.send_header(cas.End())
    
    def calc_is_ready_to_receive(self):
        """Return if the calculator is ready to receive some data"""
        interactive = False
        if interactive:
            code = '\x06'
        else:
            code = '\x16'
        self.send_byte(code)
        byte_rep = self.get_byte()
        if interactive and byte_rep == '\x06':
            return True
        elif not interactive and byte_rep == '\x13':
            return True
        else:
            return False
    
    def send_data(self, data):
        """Send a data to the calc"""
        self.send_header(data)
        response = self.get_byte()
        if response == '\x06':
            # Calculator accept the header
            pass
        elif response == '\x24':
            raise errors.DataNoManaged([data.__class__])
        elif response == '\x2b':
            raise errors.ChecksumError()
        elif response == '\x21':
            #File already exist in the calculator.
            if self.overwrite(data.name) :
                self.send_byte('\x06')
                sec_resp = self.get_byte()
                if sec_resp != '\x06':
                    raise errors.CannotOverwrite(data.name)
                # Calculator accept to overwrite.
            else:
                # send abort code
                self.send_byte('\x15')
                sec_resp = self.get_byte()
                if sec_resp != '\x06':
                    raise errors.BadAnswerFromCalc(sec_resp)
                raise errors.TransferAborted()
        elif response == '\x51':
            raise errors.HeaderError()
        else:
            raise errors.BadAnswerFromCalc(response)
        # Make the data list to be transfered
        if data.__class__ == datacls.Picture:
            data.change_pallet(cas.PICTURE_PALLET, ['\x00\x01\x00'] * 4)
            raw_data_list = [data.raw_data[0:1028],
                             data.raw_data[1028:2056],
                             data.raw_data[2056:3084],
                             data.raw_data[3084:4112]]
        else:
            raw_data_list = [data.raw_data]
        for raw_data in raw_data_list:
            # CRC calcul
            crc = 0
            for byte in raw_data:
                crc += ord(byte)
            crc = (abs(255 - (crc % 256)) + 1) % 256
            # Other way to get the crc - maybe better
            #crc = 0
            #for byte in raw_data:
            #    crc += ord(byte)
            #    if crc > 255:
            #        crc -= 256
            #crc = 256 - crc
            # Now sending the data
            self.send_string('\x3a' + raw_data, self.status, data.name, False)
            # All data sent, let the calc check the CRC
            self.send_byte(chr(crc))
            response = self.get_byte()
            if response != '\x06':
                raise errors.ChecksumError()
        return
    
    def send_header(self, data):
        """Send the header corresponding to data"""
        header = cas.build_header(data)
        if data.__class__ == cas.End:
            self.send_string('\x3a' + header, self.status, 'End', False)
        else:
            self.send_string('\x3a' + header, self.status, data.name, True)
        return

    #Receive system
    def receive(self):
        """Return a file_data with all data receive from the calc."""
        file_data = datacls.FileData()
        while not self.calc_is_ready_to_send():
            self.check_cancel()
        # Debut de la réception
        while True:
            data = self.receive_data()
            if data.__class__ == cas.End:
                break
            file_data.data.append(data)
            if data.__class__ == datacls.ScreenCapture or \
                    data.__class__ == datacls.Backup:
                break
        return file_data
    
    def calc_is_ready_to_send(self):
        """Waiting for receiving data"""
        byte = self.get_byte()
        if byte == '\x15':
            # Received request for interactive handshake (0x15)
            # -- responding with 0x13
            self.send_byte("\x13")
            return True
        elif byte == '\x16':
            # Received request for noninteractive handshake (0x16)
            # -- responding with 0x13
            self.send_byte("\x13")
            return True
        else:
            return False

    def receive_data(self):
        """Receive a data"""
        # Get header
        self.status('unknown', 0, 49, True)
        header, data_type, sub_type = self.get_header()
        self.status('unknown', 49, 49, True)
        if data_type == 'end':
            # End of the transfer : no data
            return cas.End()
        # Get data informations
        data_len = cas.get_data_len(header, data_type, sub_type)
        # Make the data
        if data_type == 'program':
            data = datacls.Program()
        elif data_type == 'backup':
            data = datacls.Backup()
        elif data_type == 'picture':
            data = datacls.Picture()
        elif data_type == 'screencapture':
            data = datacls.ScreenCapture()
        else:
            # unknown data, refuse header
            self.send_byte("\x00")
            raise errors.HeaderError()
        cas.fill_metadata(data, header)
        #if data_len == 0:
        #    #datalen is 0 -- sending 0x00
        #    self.send_byte('\x00')       

        # we accepted the header -- so we send 0x06 to the calc
        self.send_byte("\x06")
        if not self.get_byte() == '\x3a':
            raise errors.BadAnswerFromCalc()
        crc = 0
        raw_data = ''
        for index in range(data_len):
            self.check_cancel()
            byte = self.get_byte()
            crc = crc + ord(byte)
            if data_type == 'screencapture' and sub_type == 'color' \
                    and (index == 1024 or index == 2048):
                resp2 = self.get_byte()
                self.send_byte('\x06')
                resp2 = self.get_byte()
                crc = 0
            if data_type == 'picture' and (index + 1) % 1028 == 0 \
                    and index != 0 and index + 1 < data_len:
                # IMAGE / color screencapture
                newcrc = ord(self.get_byte())
                crc = abs(255 - (crc % 256)) + 1
                if not newcrc == crc :
                    raise errors.ChecksumError()
                crc = 0
                self.send_byte('\x06')
                resp2 = self.get_byte()
                if not resp2 == '\x3a':
                    raise errors.BadAnswerFromCalc(resp2)
            #if data_type == 1 and bytesproc + 1 == 62684:
            #    print 'byte 62684 of a backup : send 0x06'
            #    send_byte('\x06')
            raw_data += byte
            self.status(data.name, index, data_len - 1, False)
        newcrc = ord(self.get_byte())
        crc = abs(255 - (crc % 256)) + 1
        if newcrc != crc and data_type != 'screencapture' and \
                sub_type != 'color':
            # Warning: the crc check is not done for color screencapture
            #          because the crc of color screencapture is never
            #          valid.
            raise errors.ChecksumError()
        self.send_byte('\x06')
        #if data_type == 8 and data_sub_type == 13:
        #    print "coincoin (cf.perl)"
        if data_type == 'screencapture' and sub_type == 'color':
            data.raw_data = cas.color_screencapture_to_raw(raw_data)
        elif data_type == 'screencapture' and sub_type == 'mono':
            data.raw_data = cas.mono_screencapture_to_raw(raw_data)
        else:
            data.raw_data = raw_data
        return data

    def get_header(self):
        """Return [header, header_type]"""
        byte = self.get_byte()
        if not byte == '\x3a':
            raise errors.HeaderError()
        header = ''
        total_bytes = 100
        cur_byte = 0
        h_crc = 0
        while cur_byte < total_bytes:
            byte = self.get_byte()
            header += byte
            if cur_byte == 7:
                header_len, header_type, sub_type = \
                                cas.get_header_format(header)
                total_bytes = header_len
            cur_byte += 1
            if cur_byte < total_bytes:
                h_crc = (h_crc + ord(byte)) % 256
        h_crc = (0 - h_crc) % 256
        if h_crc != ord(byte) :
            raise errors.ChecksumError()
        return [header, header_type, sub_type]
