# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2007 Florian Birée aka Thesa <florian@biree.name>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release
#     This code is hardly inspired by cafix softwares (cafix.sourceforge.com)
#     A lot of thanks for cafix coders.
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""cas (CASIOLINK format) management module"""

__author__ = "Florian Birée"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2007, Florian Birée"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import datetime
from . import errors
from . import data as datacls

# Constant
PICTURE_PALLET = ['o', 'b', 'g', 'w']
SCREEN_PALLET = ['b', 'g', 'w', 'o']

# Special Data class
class End(datacls.Data):
    """Special data to end a transfer."""
    def __init__(self):
        """New end data"""
        datacls.Data.__init__(self)
        self.dType = 'end'

# Cas format functions
def build_header(data):
    """Return a cas header built from data"""
    if data.__class__ == End:
        header = 'END' + '\xff' * 45
    else:
        #header_len = 49
        data_len = len(data.raw_data) + 2
        #header = '\xff' * header_len
        if data.__class__ == datacls.Program:
            header = 'TXT\x00PG'
            header += chr(data_len / (256 ** 3))
            header += chr((data_len % 256 ** 3) / (256 ** 2))
            header += chr((data_len % 256 ** 2) / 256)
            header += chr(data_len % 256) # 0 -> 9
            header += data.name[:8] + '\xff' * (8 - len(data.name[:8])) #10->17
            header += '\xff' * 8
            header += data.password[:8] + '\xff' * (8 - len(data.password[:8]))
            if data.use_base:
                header += 'BN'
            else:
                header += 'NL'
            header += '\xff' * 12
        elif data.__class__ == datacls.Backup:
            header = data.raw_data[2048: 2048 + 48]
            header = header[:33] + '\x00\x10\x00\x00\x00\x00' + header[39:]
        elif data.dType == 'variable' and False:
            # TO BE CHECKED
            header += 'VAL\x00'
            #for (my $a = 0; $a < length($self->{dataname}); $a++) {
            #	last if ($a > 8);
            #	my $bar = sprintf "0x%lx",
            #	ord(substr($self->{dataname}, $a, 1));
            #	$foo[10+$a] = $bar;
            #}
            #$foo[18] = "0x56"; # V
            #$foo[19] = "0x61"; # a
            #$foo[20] = "0x72"; # r
            #$foo[21] = "0x69"; # i
            #$foo[22] = "0x61"; # a
            #$foo[23] = "0x62"; # b
            #$foo[24] = "0x6c"; # l
            #$foo[25] = "0x65"; # e
            #$foo[26] = "0x52"; # R
            #$foo[27] = "0x0a"; # LF
            #if ($self->{subtype} == 13) {
            #	$foo[4] = "0x56"; # V
            #	$foo[5] = "0x4d"; # M
            #	$foo[6] = "0x00"; # NUL
            #	$foo[7] = "0x01"; # SOH
            #	$foo[8] = "0x00"; # NUL
            #	$foo[9] = "0x01"; # SOH
            #}
        elif data.__class__ == datacls.Picture:
            header = 'IMG\x00PC'
            header += '\x00\x40\x00\x80' # len ?
            header += data.name
            header += '\xff' * 8
            header += 'DRUWF'
            header += '\x00\x04\x00\x01' # len ?
            header += '\xff' * 13
        else:
            raise errors.DataNoManaged([data.__class__])
    # h_crc (header checksum) calcul
    h_crc = 0
    for byte in header:
        h_crc = (h_crc + ord(byte)) % 256
    h_crc = (0 - h_crc) % 256
    header += chr(h_crc)
    return header

def get_header_format(header):
    """Return [header_len, header_type, sub_type]."""
    sub_type = None
    type_id = header[0:3]
    sub_id = header[4:6]
    if type_id == "MEM" and sub_id == "BU":
        header_type = 'backup'
        headerlen = 49
    elif type_id == "DD@":
        header_type = 'screencapture'
        headerlen = 39
        sub_type = 'mono'
    elif type_id == "DC@":
        header_type = 'screencapture'
        headerlen = 39
        sub_type = 'color'
#     elif type_id == "FNC":
#         datatype = 4
#         headerlen = 49
    elif type_id == "IMG" and sub_id == "PC":
        header_type = 'picture'
        headerlen = 49
    elif type_id == "TXT" and sub_id == "PG":
        header_type = 'program'
        headerlen = 49
#     elif type_id == "VAL":
#         datatype = 8
#         headerlen = 49
#     elif type_id == "REQ":
#         datatype = 7
#         headerlen = 49
    elif type_id == "END":
        header_type = 'end'
        headerlen = 49
    else:
        header_type = 'unknown'
        headerlen = 49
    return [headerlen, header_type, sub_type]

def get_data_len(header, data_type, sub_type):
    """Return data len from the header"""
    std_len = ord(header[6]) * (256 ** 3) + ord(header[7]) * (256 ** 2) + \
              ord(header[8]) * 256 + ord(header[9]) - 2
    if data_type == 'program':
        data_len = std_len
    elif data_type == 'backup': # bck
        data_len = std_len
    elif data_type == 'end':
        data_len = 0
#     elif data_type == 'val' :
#         data_len = stdlen
#         if data_sub_type == 13:
#             data_len = 14
#         elif data_sub_type == 6:
#             data_len = 2 * (ord(header[8]) + ord(header[9])) - 2
    elif data_type == 'screencapture' and sub_type == 'color': 
        #screencapture color
        data_len = 3075
    elif data_type == 'screencapture' and sub_type == 'mono':
        #screencapture mono
        data_len = 1024
    elif data_type == 'picture': #image
        data_len = 4112
    else:
        data_len = std_len
    return data_len

def fill_metadata(data, header, add_date = True):
    """Fill a data object with metadata from header"""
    ### All data ###
    # Name:
    # lazy code : what append if \xff are followed by non ff characters ?
    data.name = header[10:18].replace('\xff', '')
    # Date:
    if add_date:
        data.date = datetime.date.today()
    ### Programs ###
    if data.__class__ == datacls.Program:
        # Password:
        data.password = header[26:34].replace('\xff', '')
        # Base:
        if header[34:36] == 'BN':
            data.use_base = True
        else:
            data.use_base = False
    ### Pictures ###
    elif data.__class__ == datacls.Picture:
        data.pallet = PICTURE_PALLET
        data.color_byte = 3
        ### Screen capture ###
    elif data.__class__ == datacls.ScreenCapture:
        data.name = "Capture"
        data.pallet = SCREEN_PALLET
        data.color_byte = 0

def color_screencapture_to_raw(screen_capture):
    """Return picture raw data from raw color screen capture."""
    return screen_capture[:0x400 * 2 + 2] +\
           '\x03' + '\x00' * 0x400 +\
           screen_capture[0x400 * 2 + 2:]
           
def mono_screencapture_to_raw(screen_capture):
    """Return picture raw data from raw mono screen capture."""
    # Convert screencapture (invert up and down)
    columns = [''] * 16
    for index in range(len(screen_capture)):
        col = index / 64
        columns[col] += screen_capture[index]
    for col in range(len(columns)):
        byte_list = list(columns[col])
        byte_list.reverse()
        columns[col] = ''.join(byte_list)
    black_sheet = ''.join(columns)
    return '\x01' + black_sheet +\
           '\x02' + '\x00' * 0x400 +\
           '\x03' + '\x00' * 0x400 +\
           '\x04' + '\x00' * 0x400

# File format management classes
class CasFile:
    """Cas (Casiolink) file format manager."""
    
    name = 'cas'
    managed_data = [datacls.Program, datacls.Backup, datacls.Picture, \
                    datacls.ScreenCapture]
    read = True
    write = False
    list_ext = ['cas']
    def __init__(self, filename = None):
        """Make a cas file object with filename"""
        self.filename = filename
        
    def open_file(self):
        """Open a file in the cas Format"""
        # Build the FileData
        file_data = datacls.FileData()
        cas_file = open(self.filename, 'r')
        ctn = cas_file.read()
        cas_file.close()
        # Init var
        index = 0
        is_header = True
        header = ""
        header_len = 100
        data = None
        data_len = 100
        raw_data = ""
        # Loop on bytes in ctn
        while index < len(ctn):
            if is_header and ctn[index] != ':' and len(header) < header_len:
                header += ctn[index]
                if len(header) == 7:
                    # Get metadata
                    header_len, data_type, sub_type = \
                                get_header_format(header)
            elif is_header and len(header) == header_len:
                # Crc byte, init the data record
                is_header = False
                if data_type == 'end':
                    break
                data_len = get_data_len(header, data_type, sub_type)
                # Make the data
                if data_type == 'program':
                    data = file_data.new_record(datacls.Program)
                elif data_type == 'backup':
                    data = file_data.new_record(datacls.Backup)
                elif data_type == 'picture':
                    data = file_data.new_record(datacls.Picture)
                elif data_type == 'ScreenCapture':
                    data = file_data.new_record(datacls.ScreenCapture)
                else:
                    data = file_data.new_record(datacls.Data)
                fill_metadata(data, header, False)
            elif not is_header and len(raw_data) == 0 and ctn[index] == ':':
                # Not header, first :
                pass
            elif not is_header and len(raw_data) < data_len:
                # Raw_data
                if data_type == 'screencapture' and sub_type == 'color' \
                        and (len(raw_data) == 1024 or len(raw_data) == 2048):
                    # Jump two bytes (checksum and :)
                    index += 2
                if data_type == 'picture' and (len(raw_data) + 1) % 1028 == 0 \
                        and len(raw_data) != 0 and len(raw_data) + 1 < data_len:
                    # Jump two bytes (checksum and :)
                    index += 2
                raw_data += ctn[index]
            elif not is_header and len(raw_data) == data_len:
                # Data Crc, save raw_data (and make some convertions)
                if data_type == 'screencapture' and sub_type == 'color':
                    data.raw_data = color_screencapture_to_raw(raw_data)
                elif data_type == 'screencapture' and sub_type == 'mono':
                    data.raw_data = mono_screencapture_to_raw(raw_data)
                else:
                    data.raw_data = raw_data
                # Init vars
                is_header = True
                header = ""
                header_len = 100
                data = None
                raw_data = ""
            index += 1
        return file_data
