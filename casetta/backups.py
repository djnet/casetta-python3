# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2007 Xion345 aka Fabien ANDRE <fabien.andre.g@wanadoo.fr>
# (c) 2006-2007 Thesa aka Florian Birée <florian@biree@.name>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release (extract password function is from casetta_gtk code)
#   o Add the DOTALL flag in regular expressions
#   o Remove the find_password function (replaced by the improved 
#     find_prgm_metadata function)
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Pictures manager for casetta"""

__author__ = "Fabien ANDRE, Florian Birée"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Fabien ANDRE, Florian Birée"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import re

def find_prgm_list(rawback):
    """Return a list like [(name1, use_base1, offset1, password1),... ]
    
    Where name1 is the raw name of the first program ;
    use_base1 is True if the program use base calculation, False else ;
    offset1 is the offset of the first program (for find_prgm_data function) ;
    password1 is the raw password of the first program.
    """
    names_pattern = re.compile( """
    [\x00\xee]          # first character (0x00 except if the last prgm have a
                        # password. In this case, 0xee)
    ([a-zA-Z0-9\x99\xa9\xb9
    \xcd\xce\x89\x20'"~.{}\[\]] # A program name begins by a charater
    [a-zA-Z0-9
    \x99\xa9\xb9\xcd\xce\x89\x20'"~.{}\[\]\xff]
    {7})                # End of program name: 7 of the above charaters or 0xff
    
    .{2}                # 2 charaters
    [\x00]{2}           # 2 0x00 characters
    ([\x00\x01])        # 0x01 if using base calculation, 0x00 else
    (.{2})              # offset of the beginning of the prgm data 
                        # If a password is present:
    (?:[\x10]           # A 0x10 character (ie a password)
    ([a-zA-Z0-9\x99\xa9\xb9\xcd\xce\x89\x20'"~.{}\[\]] # Password (same as name)
    [a-zA-Z0-9\x99\xa9\xb9\xcd\xce\x89\x20'"~.{}\[\]\xff]{7})
    [\x00]{7}           # 7 0x00 characters
    )?
    """, re.VERBOSE | re.DOTALL)
    names_list = names_pattern.findall(rawback)
    return [(i[0].replace('\xff',''), # Clean the name
             bool(ord(i[1])),         # Boolean base attribut
             i[2],                    # offset
             i[3].replace('\xff', '') # Clean the password
            ) for i in names_list]

def find_prgm_data(offset, rawback):
    """Return raw data of a program in a backup with its offset"""
    ad2 = ord(offset[1]) * 0x100 + ord(offset[0])
    i = ad2
    prgm_data = ""
    
    while i > 0:
        prgm_data += (rawback[i])
        if rawback[i] == '\xff':
            break
        i -= 1
    return prgm_data
