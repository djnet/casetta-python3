# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release (code from __init__.py)
#   o Manage the new internal transfer tool
#   o New organization: each transfer tool is a class
#   o Fix the seach for commands in the path
#   o Now remove temp files
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Casio devices manager"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import os
import tempfile
import datetime

from . import errors
from . import devices_serial
from . import data as datacls

# Functions and Class for cafix
def default_cafix_send_interface(tmp_dir):
    """Send data from tmpDir to the casio using cafix"""
    # Get the current dir
    cur_dir = os.path.abspath(os.path.curdir)
    print("Run your calculator, and set it in the state 'Receiving'.")
    print("Next press Enter.")
    eval(input())
    os.system("cd " + tmp_dir + " && cafix -s *")
    os.system("cd " + cur_dir)

def default_cafix_receive_interface(tmp_dir):
    """Receive data from a casio with cafix in tmpDir"""
    cur_dir = os.path.abspath(os.path.curdir)
    print("You can send data from the calculator")
    os.system("cd " + tmp_dir + " && cafix -r")
    os.system("cd " + cur_dir)

class CafixTool:
    """Serial transfer tool using Cafix"""
    
    name = 'cafix'
    managed_data = [datacls.Program, datacls.Backup]
    def __init__(self, receive_interface = default_cafix_receive_interface, \
            send_interface = default_cafix_send_interface):
        """Initialize a new Cafix Tool, and set interfaces."""
        self.receive_interface = receive_interface
        self.send_interface = send_interface
    
    def receive(self):
        """Import data in a file_data from a casio with cafix.

        Raise NoCommand if cafix isn't in the path
        Raise ToolNotConfigured if /dev/casio doesn't exist
        
        """
        # Check cafix
        if not command_exist('cafix'):
            raise errors.NoCommand('cafix')
        # Check /dev/casio
        if not os.path.exists('/dev/casio'):
            raise errors.ToolNotConfigured
        # Choice a temp directory and make it
        directory = tempfile.mkdtemp()
        tmp_list = [directory]
        self.receive_interface(directory)
        # import all cafix files
        file_data = datacls.FileData()
        for file_name in [file_name for file_name in os.listdir(directory)\
                     if os.path.isfile(os.path.join(directory, file_name)) \
                        and file_name[-6:] == '.cafix']:
            file_path = os.path.join(directory, file_name)
            (new_data,) = file_data.import_file(file_path)
            new_data.date = datetime.date.today()
            tmp_list = [file_path] + tmp_list
        # Remove temp files
        clean_temp(tmp_list)
        return file_data

    def send(self, data_list):
        """Transfert data from data_list to the casio using cafix.

        Raise NoCommand if cafix isn't found in the path
        Raise ToolNotConfigured if /dev/casio doesn't exist
        
        """
        # Check cafix
        if not command_exist('cafix'):
            raise errors.NoCommand('cafix')
        # Check /dev/casio
        if not os.path.exists('/dev/casio'):
            raise errors.ToolNotConfigured
        # Choice a temp directory and make it
        directory = tempfile.mkdtemp()
        tmp_list = [directory]
        # Export data in this folder
        for data in data_list:
            cafix_file_path = os.path.join(directory, data.name)
            cafix_file = open(cafix_file_path, 'w')
            cafix_file.write(data.raw_data)
            cafix_file.close()
            tmp_list = [cafix_file_path] + tmp_list
        self.send_interface(directory)
        # Remove temp files
        clean_temp(tmp_list)

# Functions and class for another tool
def default_other_receive_interface(cmdline):
    """Receive data from a casio with another tool"""
    print("You can send data from the calculator")
    if os.system(cmdline):
        raise errors.ToolNotConfigured
    #[term [-c]] program [options] %port %file

def default_other_send_interface(cmdline):
    """Send data to the casio using another tool"""
    print("Run your calculator, and set it in the state 'Receiving'.")
    print("Next press Enter.")
    eval(input())
    if os.system(cmdline):
        raise errors.ToolNotConfigured

class OtherTool:
    """Serial transfer tool using a custom program"""
    
    name = 'other'
    managed_data = []
    def __init__(self, format, receive_command, send_command, port, \
            receive_interface = default_other_receive_interface, \
            send_interface = default_other_send_interface):
        """Initialize a new Cafix Tool, and set interfaces."""
        self.format = format
        self.managed_data = format.managed_data
        self.receive_command = receive_command
        self.send_command = send_command
        self.port = port
        self.receive_interface = receive_interface
        self.send_interface = send_interface

    def receive(self):
        """Import data in a file_data from a casio with another tool."""
        # Choice a temp filename
        tmp_file = tempfile.mkstemp("." + self.format.list_ext[0])[1]
        cmdline = self.receive_command.replace(r'%port', self.port).\
                                       replace(r'%file', tmp_file)
        self.receive_interface(cmdline)
        # import the temp file
        file_data = datacls.FileData(tmp_file, self.format)
        # Remove the temp file
        clean_temp(tmp_file)
        return file_data

    def send(self, data_list):
        """Transfert data from data_list to the casio using another tool."""
        # Choice a temp filename and make it
        tmp_file = tempfile.mkstemp("." + self.format.list_ext[0])[1]
        cmdline = self.send_command.replace(r'%port', self.port).\
                                    replace(r'%file', tmp_file)
        file_data = datacls.FileData()
        file_data.data = data_list
        file_data.save(tmp_file, self.format, ignore_warnings = True)
        self.send_interface(cmdline)
        # Remove the temp file
        clean_temp(tmp_file)

# Misc functions:
def command_exist(command) :
    """Check if command is in the path"""
    if 'PATH' in os.environ:
        path_var = os.environ['PATH']
    else:
        path_var = os.defpath
    if len([path for path in path_var.split(os.pathsep) \
                 if os.path.exists(os.path.join(path,command))]) == 0:
        return False
    else:
        return True

def clean_temp(path_list) :
    """Delete all paths (files or dirs) in the path_list.
    
    paths will be deleted in the order of the list ; dirs should be empty."""
    for path in path_list:
        if os.path.exists(path):
            if os.path.isdir(path):
                try:
                    os.rmdir(path)
                except OSError:
                    pass
            else:
                try:
                    os.remove(path)
                except OSError:
                    pass

# Transfer tool list:
tool_list = [devices_serial.Connexion, CafixTool, OtherTool]
tool_dic = {}
for tool in tool_list:
    tool_dic[tool.name] = tool
