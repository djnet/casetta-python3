#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o many change to be compatible with casetta 0.3.*
# o Change the rename behaviour
# o Make the code to be modular (a lot of code is now into other modules)
# o Fix the i18n bug under windows on a non-localized system
# o Add a windows icon
# o casetta_gtk has a new name: Gasetta
#
# # version 0.2.0:
#
# o partial rewrite of the 0.1.* version to work with casetta 0.2.*
# o New utility to find passwords in a backup
# o Export function
# o New way to show the command list: as casio menus
# o New file filter : All managed data
# o newcat is now the default save/export format
# o New preferences system (config file/preferences window)
# o The font of the program editor is now configurable
# o Syntax coloration (experimental)
# o Configuration of the syntax coloration
# o Management of base programs
# o Passwords management
# o Fix a regression in the command line file opening
# o Specials characters in data names and passwords are allowed
# o Preference window about transfers tool
# o Management of cas / user transfert tool
# o New way to choice the cafix port (allowing other port as /dev/ttyUSB0)
# o Allow the user to choice the console / sudo app.
# o Replace os.tmpnam() by os.tempnam()
# o Disable the CaS transfer tool (CaS can be used with the other transfer
#   tool)
# o new main port configuration (the same port is used by all tool). Cafix ln
#   is now made if needed when the user want to send/receive data.
# o use tempfile.mk[sd]temp instead of os.tempnam() (security reason)
# o fix bugs in the About Dialog (close button, re-open wich fails)
# o add a windows specific local directory
# o new locale code for windows
#
# # version 0.1.1:
#
#  o Fix a bug when the 'cancel' button is clicked in a file selection dialog
#    function on_ouvrir1_activate, on_importer1_activate and 
#    on_enregistrer_sous1_activate
#
# # version 0.1.0:
#
#   o First release
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Graphic data manager for casio calculators"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

# Python modules imports
import os
import sys
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import gi
# gi.require_version("Gtk", "3.0")
# from gi.repository import Gtk.glade
from gi.repository import GObject
# GObject.threads_init()

# i18n
application = 'gasetta'
__builtins__.application = application
import gettext
if os.name == 'nt':
    # Get the windows local path
    win_local_path = os.path.abspath(os.path.join(os.path.pardir, 'locale'))
    Gtk.glade.bindtextdomain(application, win_local_path)
    Gtk.glade.textdomain(application)
    import locale
    lang = locale.getdefaultlocale()[0][:2]
    try:
        cur_lang = gettext.translation(application, localedir=win_local_path, \
                                   languages=[lang])
        cur_lang.install()
    except IOError:
        __builtins__._ = lambda text:text
else :
    gettext.install(application)

# Casetta import
import casetta

# Gasetta imports
import prefs as prefs_mod
import data_man as data_man_mod
import file_man as file_man_mod
import properties as properties_mod
import prgm_editor as prgm_editor_mod
import backup_manager as backup_manager_mod
import pict_editor as pict_editor_mod
import dev_man as dev_man_mod
import wprefs as wprefs_mod
import misc

WIN_ICON = 'gasetta.ico'
help_url = _("http://casetta.tuxfamily.org/doc")

# Gui classes
class MainWindow:
    """Main window class"""
    
    def __init__(self, filename=''):
        """Init the GUI, optionaly load filename"""
        # GUI
        # self.gui = Gtk.glade.XML(fname="gasetta.glade", domain=application)
        # ref: https://python-gtk-3-tutorial.readthedocs.io/en/latest/builder.html
        builder = Gtk.Builder()
        builder.add_from_file("gasetta.ui")


        # https://python-gtk-3-tutorial.readthedocs.io/en/latest/menus.html?highlight=ui#ui-manager
        self.gui = Gtk.UIManager()
        self.builder = builder


        # Set the windows icon (because bill hates svg)
        if os.name == 'nt':
            self.builder.get_object("wmain").set_icon_from_file(WIN_ICON)
            self.builder.get_object("aboutdialog").set_icon_from_file(WIN_ICON)
            self.builder.get_object("wprefs").set_icon_from_file(WIN_ICON)
            self.builder.get_object("wtransfer").set_icon_from_file(WIN_ICON)
        
        # Load prefs
        self.prefs = prefs_mod.prefs
        self.prefs.load()
        
        # init variables
        self.file_changed = False
        self.current_filename = ''
        self.current_data = None
        
        # Creat an empty file
        self.file_data = casetta.data.FileData()
        
        # Load several gui parts (and build signals)
        self.accel_group = Gtk.AccelGroup()
        self.builder.get_object("wmain").add_accel_group(self.accel_group)
        
        signals = {"close" : self.close,
                   "about" : self.about,
                   "web_help" : self.web_help}
        self.misc = misc
        self.data_man = data_man_mod.DataManager(self)
        signals.update(self.data_man.signals)
        self.file_man = file_man_mod.FileManager(self)
        signals.update(self.file_man.signals)
        self.properties = properties_mod.PropertiesManager(self)
        signals.update(self.properties.signals)
        self.prgm_editor = prgm_editor_mod.ProgramEditor(self)
        signals.update(self.prgm_editor.signals)
        self.backup_manager = backup_manager_mod.BackupManager(self)
        signals.update(self.backup_manager.signals)
        self.pict_editor = pict_editor_mod.PictureEditor(self)
        signals.update(self.pict_editor.signals)
        self.wprefs = wprefs_mod.PrefsWindow(self)
        signals.update(self.wprefs.signals)
        self.dev_man = dev_man_mod.DeviceManager(self)
        signals.update(self.dev_man.signals)
        
        # GUI Main connexions
        self.builder.connect_signals(signals)
        
        # open filename from command line
        if filename != '':
            self.file_man.open_file(filename)
        self.select(None)
    
    # Very basical event manager
    def close(self, widget=None, data=None):
        """Quit the program"""
        if not self.file_man.close_file():
            return
        Gtk.main_quit()
    
    def about(self, gui):
        """Show the about window"""
        about_window = self.builder.get_object("aboutdialog")
        about_window.run()
        about_window.hide()
    
    def web_help(self, gui):
        """Open into a web browser the help page"""
        if self.misc.command_exist("gnome-open"):
            os.system('gnome-open %s' % help_url)
        else:
            import webbrowser
            webbrowser.open(help_url)
    
    def refresh_title(self):
        """Refresh window title"""
        window = self.builder.get_object("wmain")
        if self.current_filename == '':
            cur_file = '--'
        else:
            cur_file = os.path.basename(self.current_filename)
        if self.file_changed:
            mod = '* '
        else:
            mod = ''
        window.set_title('%s %s- Gasetta' % (cur_file, mod))
    
    def select(self, data):
        """Change the window follow the selected data"""
        self.properties.select(data)
        self.prgm_editor.select(data)
        self.backup_manager.select(data)
        self.pict_editor.select(data)

# Main code
if __name__ == "__main__":
    if len(sys.argv) == 1 or sys.argv[1] == '':
        cli_filename = ''
    elif sys.argv[1].lower() == '-h' or sys.argv[1].lower() == '--help':
        # CLI help text
        print('Gasetta -- Graphic interface for casetta')
        print('Usage: gasetta [FILENAME]')
        print('Open FILENAME in Gasetta')
        print("You can also use 'gasetta --help' to display this message")
        sys.exit()
    else:
        # check if sys.argv[1] is really a file
        if os.path.isfile(sys.argv[1]):
            cli_filename = sys.argv[1]
        else:
            sys.stderr.write('Bad filename. Run gasetta normally.\n')
            cli_filename = ''
    ui = MainWindow(cli_filename)
    Gtk.main()

# vim:ai:et:sw=4:ts=4:sts=4:tw=78:fenc=utf-8
