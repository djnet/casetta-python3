# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o First release, code from casetta_gtk
# o Add the get_home_dir function
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Some functions useful in Gasetta"""
__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import os
import sys
import casetta

def command_exist(command) :
    """Check if command is in the path"""
    if 'PATH' in os.environ:
        path_var = os.environ['PATH']
    else:
        path_var = os.defpath
    if len([path for path in path_var.split(os.pathsep) \
                 if os.path.exists(os.path.join(path,command))]) == 0:
        return False
    else:
        return True

def gdk_color_to_hexa(gdk_color):
    """Convert a Gdk.Color into a hexa string"""
    red = hex(gdk_color.red)[2:]
    if len(red) == 1:
        red = '0' + red
    green = hex(gdk_color.green)[2:]
    if len(green) == 1:
        green = '0' + green
    blue = hex(gdk_color.blue)[2:]
    if len(blue) == 1:
        blue = '0' + blue
    hexa = '#' + red + green + blue
    return hexa

def get_home_dir() :
    """Return the path of the user home dir"""
    # Function by K.S.Sreeram <sreeram@tachyontech.net> from:
    # <URL:http://mail.python.org/pipermail/python-list/2006-July/393819.html>
    if sys.platform != 'win32' :
        return os.path.expanduser( '~' )
        
    def valid(path) :
        """Return if a path is valid"""
        return (path and os.path.isdir(path))
    
    def env(name) :
        """Return the environment variable env"""
        return os.environ.get( name, '' )
    
    home_dir = env( 'USERPROFILE' )
    if not valid(home_dir) :
        home_dir = env( 'HOME' )
        if not valid(home_dir) :
            home_dir = '%s%s' % (env('HOMEDRIVE'), env('HOMEPATH'))
            if not valid(home_dir) :
                home_dir = env( 'SYSTEMDRIVE' )
                if home_dir and (not home_dir.endswith('\\')) :
                    home_dir += '\\'
                if not valid(home_dir) :
                    home_dir = 'C:\\'
    return home_dir

# Get i18n format names
formats_i18n_names = {'newcat': _('Newcat format'), \
                     'cafix': _('Cafix files'), \
                     'cat': _('Fa-122/Fa-123 files'), \
                     'fxi': _('Fx-Interface files'), \
                     'fxb': _('Fx-Interface backups'), \
                     'fxm': _('Fx-Interface Pro backups'), \
                     'fxd': _('Fx-Interface Pro files'), \
                     'g1r': _('Fa-124 files'), \
                     'ctf': _('Calculator Text Format'), \
                     'casemul': _('Casemul format'), \
                     'cas': _('Casiolink/Fa-121 format'), \
                     'pil': _('Picture format')}
formats_names = {}
for format_cls in casetta.formats.format_list:
    if format_cls.name in list(formats_i18n_names.keys()):
        formats_names[format_cls] = formats_i18n_names[format_cls.name]
    else:
        formats_names[format_cls] = _('%s files') % format_cls.name
del formats_i18n_names, format_cls

# Get i18n data type names
data_type_i18n = {'unknown': (_('Unknowns'), _('Unknown')), \
                  'program': (_('Programs'), _('Program')), \
                  'backup': (_('Backups'), _('Backup')), \
                  'picture': (_('Pictures'), _('Picture')), \
                  'screencapture': (_('Screen captures'), _('Screen capture'))}
data_names = {}
data_names_sing = {}
for data_type_id in list(casetta.data.data_type_dic.keys()):
    if data_type_id in data_type_i18n :
        data_names[data_type_id] = data_type_i18n[data_type_id][0]
        data_names_sing[data_type_id] = data_type_i18n[data_type_id][1]
    else:
        data_names[data_type_id] = data_type_id
        data_names_sing[data_type_id] = data_type_id
del data_type_i18n, data_type_id
