# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o First release, code from casetta_gtk
# o Add a preference to choice the end of line of ctf/newcat files
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""GUI functions to manage the preferences window"""
__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gdk
from gi.repository import Pango as pango

import casetta

class PrefsWindow:
    """This class contain all function needed to manage the prefs window"""
    
    def __init__(self, main_window):
        """Initialize the pref window"""
        self.wmain = main_window
        self.prefs = self.wmain.prefs
        self.signals = {'wprefs_open': self.wopen,
                        'wprefs_close': self.close,
                        'wprefs_font_set': self.font_set,
                        'wprefs_enable_color': self.set_enable_color,
                        'wprefs_command_color_set': self.command_color_set,
                        'wprefs_variable_color_set': self.variable_color_set,
                        'wprefs_constant_color_set': self.constant_color_set,
                        'wprefs_comment_color_set': self.comment_color_set,
                        'wprefs_tool_changed': self.tool_changed,
                        'wprefs_tool_settings_changed': \
                                                    self.tool_settings_changed,
                        'end_of_line_changed': self.end_of_line_changed}
        
        # End of line equivalents
        self.eol = ['posix', 'mac', 'win']
        
        self.wprefs = self.wmain.builder.get_object("wprefs")
        # General
        self.end_of_line = self.wmain.builder.get_object("end_of_line")
        self.end_of_line.set_active(self.eol.index(self.prefs.end_of_line))
        
        # Program editor
        self.editor_font = self.wmain.builder.get_object("editor_font")
        self.editor_font.set_font_name(self.prefs.editor_font)
        self.enable_color = self.wmain.builder.get_object("enable_color")
        if self.prefs.enable_color:
            self.enable_color.set_active(True)
        else:
            self.enable_color.set_active(False)
        self.command_color = self.wmain.builder.get_object("command_color")
        self.command_color.set_color(Gdk.color_parse(\
                                                    self.prefs.command_color))
        self.variable_color = self.wmain.builder.get_object("variable_color")
        self.variable_color.set_color(Gdk.color_parse(\
                                                    self.prefs.variable_color))
        self.constant_color = self.wmain.builder.get_object("constant_color")
        self.constant_color.set_color(Gdk.color_parse(\
                                                    self.prefs.constant_color))
        self.comment_color = self.wmain.builder.get_object("comment_color")
        self.comment_color.set_color(Gdk.color_parse(\
                                                    self.prefs.comment_color))
        # Default values for transfer tool
        self.tt_port = self.wmain.builder.get_object("tt_port")
        self.tt_none = self.wmain.builder.get_object("tt_none")
        self.tt_serial = self.wmain.builder.get_object("tt_serial")
        self.tt_cafix = self.wmain.builder.get_object("tt_cafix")
        self.tt_cafix_prefs = self.wmain.builder.get_object("tt_cafix_prefs")
        self.tt_cafix_console = self.wmain.builder.get_object("tt_cafix_console")
        self.tt_cafix_sudo = self.wmain.builder.get_object("tt_cafix_sudo")
        self.tt_other = self.wmain.builder.get_object("tt_other")
        self.tt_other_format = self.wmain.builder.get_object("tt_other_format")
        self.tt_other_input = self.wmain.builder.get_object("tt_other_input")
        self.tt_other_output = self.wmain.builder.get_object("tt_other_output")
        if not self.wmain.misc.command_exist('cafix'):
            self.tt_cafix.set_label(_("cafix is not in \
your path"))
            self.tt_cafix.set_sensitive(False)
            self.tt_cafix_prefs.set_sensitive(False)
            if self.prefs.transfer_tool == 'cafix':
                self.prefs.define('transfer_tool', 'none')
        if self.prefs.transfer_tool == 'serial':
            self.tt_serial.set_active(True)
        elif self.prefs.transfer_tool == 'cafix':
            self.tt_cafix.set_active(True)
        elif self.prefs.transfer_tool == 'other':
            self.tt_other.set_active(True)
        else:
            self.tt_none.set_active(True)
        self.tt_cafix_console.get_child().set_text(self.prefs.cafix_console)
        self.tt_cafix_sudo.get_child().set_text(self.prefs.cafix_sudo)
        self.tt_port.get_child().set_text(self.prefs.port)
        self.tt_other_format.get_child().set_text(self.prefs.other_format)
        self.tt_other_input.set_text(self.prefs.other_input)
        self.tt_other_output.set_text(self.prefs.other_output)
        
    # Preference window function
    def wopen(self, gui):
        """Open the preference window"""
        self.wprefs.show()
        
    def close(self, gui):
        """Close the preference window"""
        self.wprefs.hide()

    def font_set(self, gui):
        """Change the font of the program editor"""
        font = gui.get_font_name()
        self.prefs.define('editor_font', font)
        self.wmain.prgm_editor.editor.modify_font(Pango.FontDescription(font))
        
    def set_enable_color(self, gui):
        """Enable/disable the syntax coloration"""
        if gui.get_active():
            self.prefs.define('enable_color', '1')
            self.wmain.prgm_editor.color()
        else:
            self.prefs.define('enable_color', '0')
    
    def command_color_set(self, gui):
        """Change the color of commands"""
        color = self.wmain.misc.gdk_color_to_hexa(gui.get_color())
        self.prefs.define('command_color', color)
        self.wmain.prgm_editor.command_tag.set_property("foreground", color)
        self.wmain.prgm_editor.color()
        
    def variable_color_set(self, gui):
        """Change the color of variable"""
        color = self.wmain.misc.gdk_color_to_hexa(gui.get_color())
        self.prefs.define('variable_color', color)
        self.wmain.prgm_editor.variable_tag.set_property("foreground", color)
        self.wmain.prgm_editor.color()
        
    def constant_color_set(self, gui):
        """Change the color of constant"""
        color = self.wmain.misc.gdk_color_to_hexa(gui.get_color())
        self.prefs.define('constant_color', color)
        self.wmain.prgm_editor.constant_tag.set_property("foreground", color)
        self.wmain.prgm_editor.color()
        
    def comment_color_set(self, gui):
        """Change the color of comment"""
        color = self.wmain.misc.gdk_color_to_hexa(gui.get_color())
        self.prefs.define('comment_color', color)
        self.wmain.prgm_editor.comment_tag.set_property("foreground", color)
        self.wmain.prgm_editor.color()
        
    def tool_changed(self, gui):
        """Change the current transfer tool"""
        if gui.state == 2:
            self.prefs.define('transfer_tool', gui.get_name()[3:])
    
    def tool_settings_changed(self, gui):
        """Change a setting about transfers tool"""
        if 'child' in dir(gui):
            opt_name = gui.get_name()[3:]
            value = gui.get_child().get_text()
        else :
            opt_name = gui.get_name()[3:]
            value = gui.get_text()
        self.prefs.define(opt_name, value)
    
    def end_of_line_changed(self, gui):
        """Set the end_of_line pref"""
        self.prefs.define('end_of_line', self.eol[gui.get_active()])
        self.set_end_of_line()
    
    def set_end_of_line(self):
        """Set the end of line of newcat/ctf files"""
        if self.prefs.end_of_line.lower().startswith('win'):
            eol = '\r\n'
        elif self.prefs.end_of_line.lower().startswith('mac'):
            eol = '\r'
        else:
            eol = '\n'
        casetta.formats.format_dic['newcat'].end_of_line = eol
        casetta.formats.format_dic['ctf'].end_of_line = eol
