# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o First release, code from casetta_gtk
# o Open/Save as/Import/Export have now a good default path
# o Correct behaviour when the file_data is empty
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""GUI functions to manage files"""
__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import os

import casetta

import dialogs

class FileManager:
    """This class contain all function needed to manage files"""
    
    def __init__(self, main_window):
        """Initialize the file manager"""
        self.wmain = main_window
        self.signals = {'open_file' : self.open_button,
                        'new_file' : self.new_file,
                        'save_file' : self.save_file,
                        'save_file_as' : self.save_file_as,
                        'import_file' : self.import_file,
                        'export_file' : self.export}
    
    def open_button(self, gui=None):
        """Show a open file window"""
        if not self.close_file():
            return
        pattern_list = []
        for format in casetta.formats.format_list:
            if format.read :
                pattern_list.append((self.wmain.misc.formats_names[format], \
                                    format.list_ext))
        pattern_list.append((_('All files'), ['*']))
        new_filename = dialogs.open_file(pattern_list, \
                                         self.get_current_path())
        if new_filename != None:
            self.open_file(new_filename)
    
    def new_file(self, gui):
        """Make a new file"""
        if not self.close_file():
            return
        self.wmain.file_data = casetta.data.FileData()
        self.wmain.file_changed = False
        self.wmain.current_filename = ''
        self.wmain.refresh_title()
        self.wmain.data_man.refresh_data_list()
        
    def open_file(self, filename):
        """Open a file"""
        try:
            file_data = casetta.data.FileData(filename)
        except (casetta.errors.FormatCantOpen, casetta.errors.FormatNotFound):
            dialogs.error(_("The format of this file wasn't detected. \
\nIt wont be opened."))
            return None
        if file_data == None:
            dialogs.error(_("The file cannot be opened."))
            return None
        else :
            self.wmain.file_data = file_data
            self.wmain.current_filename = filename
            self.wmain.refresh_title()
            self.wmain.data_man.refresh_data_list()
            
    def close_file(self):
        """Close the current file"""
        self.wmain.select(None)
        if self.wmain.file_changed and \
                len(self.wmain.file_data.data) != 0:
            if dialogs.question(_('Do you want to save this file?'), \
                                _("Unsaved file")):
                #yes
                self.save_file()
            else:
                #no
                return 1
        else:
            return 1
        #return 0 : don't close
        
    def save_file(self, gui=None):
        """Save the current file"""
        if self.wmain.current_filename == '' or \
                len(self.wmain.file_data.data) == 0:
            #Save as
            self.save_file_as()
        else:
            self.wmain.file_data.save(self.wmain.current_filename, \
                                      ignore_warnings=True)
            self.wmain.file_changed = 0
            self.wmain.refresh_title()
                    
    def save_file_as(self, gui=None):
        """Save as the current file"""
        # Check if the file is empty
        if len(self.wmain.file_data.data) == 0:
            dialogs.error(_("An empty file cannot be saved."))
            return
        pattern_list = [(self.wmain.misc.formats_names[\
                            casetta.formats.format_dic['newcat']], \
                         casetta.formats.format_dic['newcat'].list_ext)]
        for format in casetta.formats.format_list:
            if format.write and format.name != 'newcat':
                pattern_list.append((self.wmain.misc.formats_names[format], \
                                    format.list_ext))
        new_filename = dialogs.save_file(pattern_list, \
                                         self.get_current_path(), \
                                         os.path.basename(\
                                            self.wmain.current_filename))
        if new_filename != None:
            try:
                self.wmain.file_data.save(new_filename)
            except casetta.errors.FormatNotFound:
                dialogs.error(_("Gasetta can't find the file format. \
Please specify it."))
                return
            except casetta.errors.DataNoManaged as not_managed:
                if not_managed.data_type_list == ['all'] :
                    dialogs.error(_("The output format you choice can not \
 manage all input data.\nPlease choose another output format."))
                    return
                else:
                    not_managed_data = [self.wmain.misc.data_names[data.dType]
                                        for data in not_managed.data_type_list]
                    dialogs.question(_("Warning: the output format can not \
manage the followed data types: %s. Do you really want to save the \
current file in this format?") % ", ".join(not_managed_data))
                self.wmain.file_data.save(\
                                    filename=new_filename, \
                                    ignore_warnings=True)
            self.wmain.current_filename = new_filename
            self.wmain.file_changed = 0
            self.wmain.refresh_title()
    
    def import_file(self, gui):
        """Import a file"""
        pattern_list = []
        for format in casetta.formats.format_list:
            if format.read:
                pattern_list.append((self.wmain.misc.formats_names[format], \
                                    format.list_ext))
        pattern_list.append((_('All files'), ['*']))
        new_filename = dialogs.open_file(pattern_list, \
                                         self.get_current_path(), \
                                         _("Import a file..."))
        if new_filename != None:
            try:
                new_data_list = self.wmain.file_data.import_file(new_filename)
            except (casetta.errors.FormatCantOpen, \
                    casetta.errors.FormatNotFound):
                dialogs.error(_("The format of this file wasn't detected. \
\nIt wont be opened."))
                return
            for data in new_data_list:
                self.wmain.data_man.check_name(data)
            self.wmain.data_man.refresh_data_list()
            self.wmain.file_changed = True
            self.wmain.refresh_title()
            
    def export(self, gui=None):
        """Export selected data to a file"""
        # Check if some data are selected
        if self.wmain.file_data.export == []:
            dialogs.error(_("You haven't choice data to export. \
Select first data by using the checkboxes in the list to \
the left. Next retry to export."))
            return
        # Select a file to export
        pattern_list = [(self.wmain.misc.formats_names[\
                            casetta.formats.format_dic['newcat']], \
                         casetta.formats.format_dic['newcat'].list_ext)]
        for format in casetta.formats.format_list:
            if format.write and format.name != 'newcat':
                pattern_list.append((self.wmain.misc.formats_names[format], \
                                    format.list_ext))
        new_filename = dialogs.save_file(pattern_list, \
                                         self.get_current_path(), \
                                         None, \
                                         _("Export data"))
        # Export
        if new_filename != None:
            self.wmain.current_filename = new_filename
            try:
                self.wmain.file_data.save(self.wmain.current_filename, \
                                          export=True)
            except casetta.errors.FormatNotFound:
                dialogs.error(_("Gasetta can't find the file format. \
Please specify it."))
                return
            except casetta.errors.DataNoManaged as not_managed:
                if not_managed.data_type_list == ['all'] :
                    dialogs.error(_("The output format you choice can not \
 manage all input data.\nPlease choose another output format."))
                    return
                else:
                    dialogs.question(_("Warning: the output format can not \
manage the followed data types: %s. Do you really want to save the \
current file in this format?") % ", ".join(not_managed.data_type_list))
                self.wmain.file_data.save(\
                                    filename=self.wmain.current_filename, \
                                    ignore_warnings=True, export=True)
    
    def get_current_path(self):
        """Return the current path for open/save as"""
        # Check if the current file have a path
        if os.path.isdir(os.path.dirname(self.wmain.current_filename)):
            return os.path.dirname(self.wmain.current_filename)
        else:
            return self.wmain.misc.get_home_dir()
