# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o Change the EntryDialog class to add a Replace button
# o Add title in each dialog, default buttons, use Gtk stock buttons
# o Allow to choice the default path in open/save
#
# # version 0.2.0:
#
# o first version (version number aligned on casetta_gtk)
#   code from casetta-gtk 0.1.*
# o New files filters (with a All managed formats filter)
# o Save window add automaticaly an extension if needed
# o The input box can have a list of choices
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Some gtk dialogs for Gasetta"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

# Imports
import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

SVG_ICON = 'gasetta.svg'
WIN_ICON = 'gasetta.ico'
if os.name == 'nt':
    icon = WIN_ICON
else:
    icon = SVG_ICON

class EntryDialog(Gtk.Dialog):
    """Input dialog class"""
    
    def __init__(self, message="", default_text='', modal= True, \
            replace_button = False, choices = None):
        """Init the input dialog"""
        GObject.GObject.__init__(self)
        self.choices = choices
        if modal:
            self.set_modal(True)
        box = Gtk.VBox(spacing=10)
        box.set_border_width(10)
        self.vbox.pack_start(box, True, True, 0)
        box.show()
        if message:
            label = Gtk.Label(label=message)
            box.pack_start(label, True, True, 0)
            label.show()
        if choices == None:
            # Text entry
            self.entry = Gtk.Entry()
            self.entry.set_text(default_text)
            box.pack_start(self.entry, True, True, 0)
            self.entry.show()
            self.entry.grab_focus()
        else:
            # Listbox
            self.combo = Gtk.ComboBoxText()
            for item in choices:
                self.combo.append_text(item)
            if default_text in choices:
                self.combo.set_active(choices.index(default_text))
            box.pack_start(self.combo, True, True, 0)
            self.combo.show()
            self.combo.grab_focus()
        button = Gtk.Button(_("OK"))
        button.connect("clicked", self.click)
        button.set_can_default(True)
        self.action_area.pack_start(button, True, True, 0)
        button.show()
        button.grab_default()
        if replace_button:
            button = Gtk.Button(_("Replace"))
            button.connect("clicked", self.replace)
            button.set_can_default(True)
            self.action_area.pack_start(button, True, True, 0)
            button.show()
        button = Gtk.Button(_("Cancel"))
        button.connect("clicked", self.quit)
        button.set_can_default(True)
        self.action_area.pack_start(button, True, True, 0)
        button.show()
        self.ret = ""
        self.set_icon_from_file(icon)
        
    def quit(self, window=None, event=None):
        """Quit the input dialog"""
        self.hide()
        self.ret = False
        self.response(0)
    def click(self, button):
        """Valid the input dialog"""
        self.hide()
        if self.choices:
            self.ret = self.choices[self.combo.get_active()]
        else:
            self.ret = self.entry.get_text()
        self.response(1)
    def replace(self, button):
        """Return the order to replace the other data"""
        self.hide()
        self.ret = True
        self.response(0)

def input_box(title=_("Input"), message="", default_text='', modal= True, \
        replace = False, choices = None):
    """Open an input box and return the result"""
    win = EntryDialog(message, default_text, modal=modal, \
                      replace_button = replace, choices = choices)
    win.set_title(title)
    win.run()
    return win.ret

def question(message, title=_("Question")):
    """Show a Yes/No (Question) message, and return True if Yes, False if No"""
    response = 0

    dialog = Gtk.MessageDialog(None,
                          Gtk.DialogFlags.MODAL,
                          Gtk.MessageType.QUESTION,
                          Gtk.ButtonsType.YES_NO,
                          message)
    dialog.set_title(title)
    dialog.set_icon_from_file(icon)
    
    if dialog.run() == Gtk.ResponseType.YES:
        response = 1
    
    dialog.destroy()

    return response

def info(message, cancel_button=False, title=_("Info")):
    """Info (OK/Cancel) message, return True if Ok, False if Cancel"""
    reponse = 0
    if cancel_button :
        buttons = Gtk.ButtonsType.OK_CANCEL
    else :
        buttons = Gtk.ButtonsType.OK
    dialog = Gtk.MessageDialog(None,
                          Gtk.DialogFlags.MODAL,
                          Gtk.MessageType.INFO,
                          buttons,
                          message)
    dialog.set_title(title)
    dialog.set_icon_from_file(icon)
    
    if dialog.run() == Gtk.ResponseType.OK:
        reponse = 1
    dialog.destroy()
    return reponse

def error(message, title="Error"):
    """Show an error message"""
    dialog = Gtk.MessageDialog(None,
                          Gtk.DialogFlags.MODAL,
                          Gtk.MessageType.WARNING,
                          Gtk.ButtonsType.OK,
                          message)
    dialog.set_title(title)
    dialog.set_icon_from_file(icon)
    dialog.run()
    dialog.destroy()
    return

def open_file(pattern_list=None, current_folder = None, \
        title=_('Open a file...')):
    """Show a dialog to open a file, return the filename or nothing if cancel
    
    pattern_list is like : [('format name',['ext1','ext2]),]
    
    """
    dialog_window = Gtk.FileChooserDialog(title,
                                        None,
                                        Gtk.FileChooserAction.OPEN,
                                        (Gtk.STOCK_CANCEL, 0, Gtk.STOCK_OK, 1))
    dialog_window.set_icon_from_file(icon)
    ok_button = dialog_window.action_area.get_children()[0]
    ok_button.set_can_default(True)
    ok_button.grab_default()
    # Choice a current folder
    if current_folder:
        dialog_window.set_current_folder(current_folder)
    # Make the pattern list
    if pattern_list != None :
        filter_name = _('All managed formats')
        all_ext = []
        for ext_list in [ext_list for format_name, ext_list in pattern_list \
                         if ext_list != ['*']] :
            all_ext += ext_list
        ffilter = Gtk.FileFilter()
        ffilter.set_name(filter_name)
        ffilter.add_custom(Gtk.FILE_FILTER_DISPLAY_NAME, file_filter, all_ext)
        dialog_window.add_filter(ffilter)
        for pattern in pattern_list :
            filter_name = pattern[0] + ' (*.' + ';*.'.join(pattern[1]) + ')'
            ffilter = Gtk.FileFilter()
            ffilter.set_name(filter_name)
            ffilter.add_custom(Gtk.FILE_FILTER_DISPLAY_NAME, file_filter, \
                               pattern[1])
            dialog_window.add_filter(ffilter)
    
    reponse = dialog_window.run() 
    fichier = dialog_window.get_filename()
    dialog_window.destroy()
    
    if reponse:
        return fichier
    else:
        return

def save_file(pattern_list=None, current_folder = None, current_name = None, \
        title = _('Save file as...')):
    """Show a dialog to save a file. 
    
    Return the filemane or nothing if cancel.
    Ask if the file already exist.
    pattern_list is like : [('format name',['ext1','ext2]),]
    
    """
    dialog_window = Gtk.FileChooserDialog(title,
                                          None,
                                          Gtk.FileChooserAction.SAVE,
                                          (Gtk.STOCK_CANCEL,0,Gtk.STOCK_OK,1))
    dialog_window.set_icon_from_file(icon)
    ok_button = dialog_window.action_area.get_children()[0]
    ok_button.set_can_default(True)
    ok_button.grab_default()
    # Choice a current folder
    if current_folder:
        dialog_window.set_current_folder(current_folder)
    # Suggest the current name
    if current_name:
        dialog_window.set_current_name(current_name)
    # Make the pattern list
    if pattern_list != None :
        for pattern in pattern_list :
            for ext in pattern[1]:
                filter_name = pattern[0] + ' (*.' + ext + ')'
                ffilter = Gtk.FileFilter()
                ffilter.set_name(filter_name)
                ffilter.add_custom(Gtk.FILE_FILTER_DISPLAY_NAME, \
                                   file_filter, [ext])
                dialog_window.add_filter(ffilter)
    
    reponse = dialog_window.run()
    if not reponse :
        dialog_window.destroy()
        return
    fichier = dialog_window.get_filename()
    #Check the extension:
    if not '.' in os.path.basename(fichier):
        current_ext = dialog_window.get_filter().get_name()
        current_ext = current_ext.split('(*')[1][:-1]
        fichier += current_ext
    
    #Check if the file alreday exist
    if not os.path.exists(fichier) or \
       question(_("This file already exist. Replace it?")):
        dialog_window.destroy()
        if reponse:
            return fichier
        else:
            return

def file_filter(filter_info, data):
    """Return if a file have an extension in the data list"""
    display_name = filter_info[2]
    for ext in data:
        if ext == '*' :
            return True
        if display_name[-len(ext)-1:].lower() == '.' + ext :
            return True
    return False
