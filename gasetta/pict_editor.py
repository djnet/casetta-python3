# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o First release, code from casetta_gtk
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""GUI functions to manage the picture editor tab"""
__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gdk

import cairo
from gi.repository import Pango
# from gi.repository import PangoCairo

import casetta

class PictureEditor:
    """This class contain all function needed to manage the picture editor"""
    
    def __init__(self, main_window):
        """Initialize the picture editor tab manager"""
        self.wmain = main_window
        self.signals = {'toggle_color_view': self.toggle_color_view,
                        'zoom_p': self.zoom_p,
                        'zoom_m': self.zoom_m,
                        'zoom_i': self.zoom_i}
        
        self.tabs = self.wmain.builder.get_object("tabs")
        self.picture_tab = self.tabs.get_nth_page(3)
        self.color_view = self.wmain.builder.get_object("color_view")
        self.orange_pen = self.wmain.builder.get_object("orange_pen")
        self.green_pen = self.wmain.builder.get_object("green_pen")
        self.blue_pen = self.wmain.builder.get_object("blue_pen")
        self.white_pen = self.wmain.builder.get_object("white_pen")
        self.color_mode = True
        self.zoom = 3
        self.data = None
        # Init of the gui
        self.wmain.builder.get_object("color_view").set_active(True)
        self.init_color_buttons()
        self.set_color_mode(True)
        self.init_draw_area()
    
    def init_color_buttons(self):
        """Initialize color buttons (fill the background color)"""
        # Orange button
        self.wmain.builder.get_object("orange_pen_color").modify_bg(\
                            Gtk.StateType.NORMAL, \
                            Gdk.Color(casetta.pictures.ORANGE[0] * 257, \
                                          casetta.pictures.ORANGE[1] * 257, \
                                          casetta.pictures.ORANGE[2] * 257))
        self.wmain.builder.get_object("green_pen_color").modify_bg(\
                            Gtk.StateType.NORMAL, \
                            Gdk.Color(casetta.pictures.GREEN[0] * 257, \
                                          casetta.pictures.GREEN[1] * 257, \
                                          casetta.pictures.GREEN[2] * 257))
        self.wmain.builder.get_object("blue_pen_color").modify_bg(\
                            Gtk.StateType.NORMAL, \
                            Gdk.Color(casetta.pictures.BLUE[0] * 257, \
                                          casetta.pictures.BLUE[1] * 257, \
                                          casetta.pictures.BLUE[2] * 257))
        self.wmain.builder.get_object("white_pen_color").modify_bg(\
                            Gtk.StateType.NORMAL, \
                            Gdk.Color(casetta.pictures.WHITE[0] * 257, \
                                          casetta.pictures.WHITE[1] * 257, \
                                          casetta.pictures.WHITE[2] * 257))
    
    def init_draw_area(self):
        """Initialize the drawing area"""
        self.drawing_area = Gtk.DrawingArea()
        self.wmain.builder.get_object("scrolled_pict_ed").add_with_viewport(\
                                                            self.drawing_area)
        self.drawing_area.show()
        self.drawing_area.realize()
        # self.drawable = self.drawing_area.window
        # self.drawable = self.wmain
        # self.colormap = self.drawing_area.get_colormap()
        # self.drawable_gc = self.drawable.new_gc( \
        #                         background=self.colormap.alloc_color("white")
        #                         )

        # cf.: https://developer.gnome.org/gtk3/stable/ch26s02.html
        self.drawing_area.connect("draw", self.draw_refresh)

        # cf.: https://lazka.github.io/pgi-docs/Gtk-3.0/classes/Widget.html
        self.drawing_area.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)

        self.drawing_area.connect("button-press-event", self.draw_point)
    
    def set_color_mode(self, color):
        """Set if the picture is display in color (True) or not"""
        if color:
            self.color_mode = True
            # Display color buttons
            self.orange_pen.show()
            self.green_pen.show()
            # Make the blue button to be blue
            self.wmain.builder.get_object("blue_pen_color").modify_bg(\
                            Gtk.StateType.NORMAL, \
                            Gdk.Color(casetta.pictures.BLUE[0] * 257, \
                                          casetta.pictures.BLUE[1] * 257, \
                                          casetta.pictures.BLUE[2] * 257))
        else:
            self.color_mode = False
            # Hide color buttons
            self.orange_pen.hide()
            self.green_pen.hide()
            # Make the blue button to be black
            self.wmain.builder.get_object("blue_pen_color").modify_bg(\
                            Gtk.StateType.NORMAL, \
                            Gdk.Color(0,0,0))
    
    def select(self, data):
        """Load/Hide the pictures editor"""
        if not (data.__class__ == casetta.data.Picture or 
                data.__class__ == casetta.data.ScreenCapture) :
            self.picture_tab.hide()
            self.data = None
        else:
            self.data = data
            self.zoom = 3
            self.set_color_mode(True)
            self.draw_refresh()
            self.picture_tab.show()
            self.tabs.set_current_page(3)
    
    def toggle_color_view(self, toggle_button):
        """Toggle the view between black&white and color mode"""
        self.set_color_mode(toggle_button.get_active())
        self.draw_refresh()
    
    def draw_refresh(self, area = None, event = None):
        """Refresh the drawing"""
        # Set the size of the drawing area (with zoom)
        self.drawing_area.set_size_request(128 * self.zoom, 64 * self.zoom)
        self.drawing_area.show()
        # Draw the picture
        pixel_list = self.data.get_picture()
        for index in range(len(pixel_list)):
            pixel = pixel_list[index]
            row = index / 128
            col = index % 128
            if self.color_mode or pixel == casetta.pictures.WHITE:
                red = pixel[0] * 257
                green = pixel[1] * 257
                blue = pixel[2] * 257
            elif not self.color_mode and pixel == casetta.pictures.BLUE:
                # Convert blue into black
                red = 0
                green = 0
                blue = 0
            else:
                # White
                red = 255 * 257
                green = 255 * 257
                blue = 255 * 257
            self.drawable_gc.set_foreground(self.colormap.alloc_color(red, green, blue))
            self.drawable.draw_rectangle(self.drawable_gc, True, \
                                         col * self.zoom, row * self.zoom, \
                                         self.zoom, self.zoom)
    
    def zoom_p(self, gui):
        """Increase the zoom"""
        if self.zoom < 16:
            self.zoom *= 2
            self.draw_refresh()
    
    def zoom_m(self, gui):
        """Decrease the zoom"""
        if self.zoom > 1:
            self.zoom /= 2
            self.draw_refresh()
    
    def zoom_i(self, gui):
        """Set the zoom to 1"""
        if self.zoom != 1:
            self.zoom = 1
            self.draw_refresh()
    
    def draw_point(self, drawing_area, event):
        """Draw a point in the selected color"""
        posx = int(event.x)
        posy = int(event.y)
        col = posx / self.zoom
        row = posy / self.zoom
        index = col + row * 128
        pixel_list = self.data.get_picture()
        # Get color:
        if self.orange_pen.get_active():
            pixel = casetta.pictures.ORANGE
        elif self.green_pen.get_active():
            pixel = casetta.pictures.GREEN
        elif self.blue_pen.get_active():
            pixel = casetta.pictures.BLUE
        else:
            pixel = casetta.pictures.WHITE
        red = pixel[0] * 257
        green = pixel[1] * 257
        blue = pixel[2] * 257
        if not self.color_mode and pixel == casetta.pictures.BLUE:
            red = 0
            green = 0
            blue = 0
        # Set the pixel in the data
        pixel_list[index] = pixel
        self.data.set_picture(pixel_list)
        # Print the point on the drawing area
        self.drawable_gc.set_foreground(self.colormap.alloc_color(red, green, blue))
        self.drawable.draw_rectangle(self.drawable_gc, True, \
                                     col * self.zoom, row * self.zoom, \
                                     self.zoom, self.zoom)
