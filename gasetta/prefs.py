# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o New default values for the internal transfer tool
# o Rewrite the module using classes
# o Remove Cas/color_all_commands settings
# o Enable the syntax highlighting by default
# o Add a prefs for end of line of ctf/newcat files (os dependent)
#
# # version 0.2.0:
#
# o first version (version number aligned on casetta_gtk)
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Preference input/output functions for Gasetta"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

# Imports
import os
import configparser

class Settings:
    """Class to save/load preferences"""
    
    def __init__(self):
        """Fill defaults values"""
        self.config_file = os.path.join(os.path.expanduser('~'), '.gasetta')
        # [General] section
        if os.name == 'nt':
            self.end_of_line = "win"
        elif os.name == 'mac':
            self.end_of_line = "mac"
        else:
            self.end_of_line = "posix"
        # [Program editor] section
        self.editor_font = "Courier 10 Pitch 12"
        self.enable_color = True
        self.command_color = "#A52A2A"
        self.variable_color = "#2E8B57"
        self.constant_color = "#FF00FF"
        self.comment_color = "#0000FF"
        # [Transfer tool] section
        self.transfer_tool = 'serial'
        self.port = '0'
        # [Transfer tool: cafix] section
        self.cafix_console = 'gnome-terminal -e'
        self.cafix_sudo = 'gksu'
        # [Transfer tool: other] section
        self.other_format = ''
        self.other_input = ''
        self.other_output = ''
    
    def load(self, config_file = None):
        """Load a config file"""
        if config_file:
            self.config_file = config_file
        try:
            config = configparser.ConfigParser()
            config.read(self.config_file)
            self.end_of_line = config.get("General", "end of line")
            self.editor_font = config.get("Program editor", "font")
            self.enable_color = config.getboolean("Program editor", \
                                                  "enable color")
            self.command_color = config.get("Program editor", "command color")
            self.variable_color = config.get("Program editor", "variable color")
            self.constant_color = config.get("Program editor", "constant color")
            self.comment_color = config.get("Program editor", "comment color")
            
            self.transfer_tool = config.get("Transfer tool", "transfer tool")
            self.port = config.get("Transfer tool", "port")
            
            self.cafix_console = config.get("Transfer tool: cafix", "console")
            self.cafix_sudo = config.get("Transfer tool: cafix", "sudo")
            
            self.other_format = config.get("Transfer tool: other", "format")
            self.other_input = config.get("Transfer tool: other", \
                                                            "receive command")
            self.other_output = config.get("Transfer tool: other", \
                                                            "send command")
        except IOError:
            return
        except configparser.NoOptionError:
            return
        except configparser.NoSectionError:
            return
    
    def define(self, name, val):
        """Change the setting name to the value val"""
        setattr(self, name, val)
        afile = open(self.config_file, 'w' )
        config = configparser.ConfigParser()
        config.add_section("General")
        config.set("General", "end of line", self.end_of_line)
        config.add_section("Program editor")
        config.set("Program editor", "font", self.editor_font)
        config.set("Program editor", "enable color", str(self.enable_color))
        config.set("Program editor", "command color", self.command_color)
        config.set("Program editor", "variable color", self.variable_color)
        config.set("Program editor", "constant color", self.constant_color)
        config.set("Program editor", "comment color", self.comment_color)
        config.add_section("Transfer tool")
        config.set("Transfer tool", "transfer tool", self.transfer_tool)
        config.set("Transfer tool", "port", self.port)
        config.add_section("Transfer tool: cafix")
        config.set("Transfer tool: cafix", "console", self.cafix_console)
        config.set("Transfer tool: cafix", "sudo", self.cafix_sudo)
        config.add_section("Transfer tool: other")
        config.set("Transfer tool: other", "format", self.other_format)
        config.set("Transfer tool: other", "receive command", self.other_input)
        config.set("Transfer tool: other", "send command", self.other_output)
        config.write(afile)
        afile.close()

# Make the prefs object
prefs = Settings()
