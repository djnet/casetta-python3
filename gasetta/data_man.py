# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o First release, code from casetta_gtk
# o When saerching the selected data, check the class
# o If possible, give a list of name when a data must be renamed
# o Add function to convert pictures into screen captures and sc into pictures
# o Add a function to make a program from a picture
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""GUI functions to manage data"""
__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import re

import casetta

import dialogs

class DataManager:
    """This class contain all function needed to manage data"""
    
    def __init__(self, main_window):
        """Initialize the data manager"""
        self.wmain = main_window
        self.signals = {'on_data_list_click' : self.on_data_list_click,
                        'new_program' : self.new_program,
                        'new_picture' : self.new_picture,
                        'new_screen_capture' : self.new_screen_capture,
                        'rename_data' : self.rename_data,
                        'delete_data' : self.delete_data,
                        'pict_to_sc' : self.convert_pict_to_sc,
                        'sc_to_pict' : self.convert_sc_to_pict,
                        'pict_to_prgm' : self.make_prgm_from_pict}
        
        # Init data list
        data_list = self.wmain.builder.get_object("dataList")
        self.data_treestore = Gtk.TreeStore(int, str)
        data_list.set_model(self.data_treestore)
        # add checkbox column
        data_ccb = Gtk.TreeViewColumn()
        data_ccb.set_resizable(False)
        data_list.append_column(data_ccb)
        # add the toggle renderer
        check = Gtk.CellRendererToggle()
        data_ccb.pack_start(check, expand = False)
        data_ccb.add_attribute(check, "active", 0)
        check.connect("toggled", self.select_data_to_export, \
                      (self.data_treestore, 0))
        data_ccb.set_visible(True)
        # add data column
        data_tvcolumn = Gtk.TreeViewColumn('Data')
        data_tvcolumn.set_resizable(False)
        data_list.append_column(data_tvcolumn)
        # add the text renderer
        data_cell = Gtk.CellRendererText()
        data_tvcolumn.pack_start(data_cell, True)
        data_tvcolumn.add_attribute(data_cell, 'text', 1)
        data_tvcolumn.set_visible(True)
        self.data_types = {}
        self.data_items = {}
        self.refresh_data_list()
    
    def refresh_data_list(self):
        """Fill/refresh the Data List"""
        self.data_treestore.clear()
        # make the dic of data type section
        self.data_types = {}
        for (data_type_id, data_name) in list(self.wmain.misc.data_names.items()):
            self.data_types[data_type_id] = self.data_treestore.\
                                            append(None,[False, data_name])
        #Fill childrens items
        self.data_items = {}
        for data in self.wmain.file_data:
            export = (self.wmain.file_data.data.index(data) in \
                      self.wmain.file_data.export)
            self.data_items[data] = self.data_treestore.\
                    append(self.data_types[data.dType], \
                            [export, data.get_name()])
    
    def check_name(self, data, rename=False):
        """Check the name of a data
        
        You can specify the rename flag to True to force to rename the data
        
        """
        # Make the syntax comment & how ask a new name (text field or list?)
        syntax_comment = ""
        choices = None
        regexp = data.name_rule
        limited_string = re.compile(r'^\^\.\{1,(.+)\}\$$')
        string_with_number = re.compile(r'^\^(.+)\[(\d)-(\d)\]\$$')
        limited_string_search = limited_string.search(regexp)
        string_with_number_search = string_with_number.search(regexp)
        if limited_string_search != None :
            (nbr_letters,) = limited_string_search.groups()
            syntax_comment = _('(maximum %s letters)') % nbr_letters
        elif string_with_number_search != None:
            (constant, first_nbr, second_nbr) = string_with_number_search.\
                                                                        groups()
            choices = [constant + str(i) for i in \
                       range(int(first_nbr), int(second_nbr) + 1)]
        while True:
            # Check the syntax of the name (or rename)
            while self.wmain.file_data.check_name(data) == 0 or rename:
                # Ask for a new name
                ret = dialogs.input_box( \
                        _('New name'), _("Please specify a new name for this \
data. %s") % syntax_comment, data.get_name(), True, False, choices)
                # If ret = '' (cancel) and rename = True : cancel
                if ret == False and rename:
                    return
                elif ret == False:
                    # Ask if the user want to delete the data
                    if dialogs.question(_("A data must have a valid name. Do \
you want to delete this data?")):
                        self.wmain.file_data.remove(data)
                        return
                else:
                    data.set_name(ret)
                rename = False
            # Check if the name is already used
            if self.wmain.file_data.check_name(data) == -1:
                rep = dialogs.input_box( \
                    _('New name'), _("This name is already used. \
Please specify a new name for this data. %s") % syntax_comment, \
                    data.get_name(), True, True, choices)
                if rep == True:
                    # Delete the data with the same name
                    for other_data in self.wmain.file_data:
                        if other_data.__class__ == data.__class__ and \
                                other_data.name == data.name and \
                                other_data is not data:
                            self.wmain.file_data.remove(other_data)
                elif rep == False:
                    # Ask if the user want to delete the data
                    if dialogs.question(_("Two data must have different \
names. Do you want to delete this data?")):
                        self.wmain.file_data.remove(data)
                        return
                else:
                    # Rename the data
                    data.set_name(rep)
            else:
                # The name is good
                return       
    
    def on_data_list_click(self, treeview, event):
        """On click on the data list"""
        posx = int(event.x)
        posy = int(event.y)
        pthinfo = treeview.get_path_at_pos(posx, posy)
        if pthinfo == None:
            return
        path, col = pthinfo[0:2]
        treeview.grab_focus()
        treeview.set_cursor(path, col, 0)
        model = treeview.get_model()
        iterat = model.get_iter(path)
        content = model[iterat]
        # content[0] -> checkbox
        # content[1] -> nom
        # content.parent[1] -> Translated type names
        # Get the class
        if content.parent != None:
            data_class = casetta.data.Data()
            for type_id, type_name in list(self.wmain.misc.data_names.items()):
                if type_name == content.parent[1]:
                    data_class = casetta.data.data_type_dic[type_id]
                    break
        else:
            data_class = None
        # Find the data
        try :
            [self.wmain.current_data] = [data for data in self.wmain.file_data \
                                         if data.get_name() == content[1] and \
                                            data.__class__ == data_class
                                        ]
        except IndexError:
            self.wmain.current_data = None
        except ValueError:
            # Two data with the same type and the same name
            self.wmain.current_data = None
        # Select the data
        self.wmain.select(self.wmain.current_data)
        if event.button == 3:
            #context menu
            if self.wmain.current_data == None:
                self.wmain.builder.get_object("sep_new").hide()
                self.wmain.builder.get_object("ctx_pict_to_prgm").hide()
                self.wmain.builder.get_object("ctx_pict_to_sc").hide()
                self.wmain.builder.get_object("ctx_sc_to_pict").hide()
                self.wmain.builder.get_object("sep_tools").hide()
                self.wmain.builder.get_object("renommer1").hide()
                self.wmain.builder.get_object("supprimer2").hide()
            elif self.wmain.current_data.__class__ == casetta.data.Data:
                # Unknown data type
                self.wmain.builder.get_object("sep_new").show()
                self.wmain.builder.get_object("ctx_pict_to_prgm").hide()
                self.wmain.builder.get_object("ctx_pict_to_sc").hide()
                self.wmain.builder.get_object("ctx_sc_to_pict").hide()
                self.wmain.builder.get_object("sep_tools").hide()
                self.wmain.builder.get_object("renommer1").hide()
                self.wmain.builder.get_object("supprimer2").show()
            else:
                if self.wmain.current_data.__class__ == casetta.data.Picture:
                    self.wmain.builder.get_object("ctx_pict_to_prgm").show()
                    self.wmain.builder.get_object("ctx_pict_to_sc").show()
                    self.wmain.builder.get_object("ctx_sc_to_pict").hide()
                    self.wmain.builder.get_object("sep_tools").show()
                elif self.wmain.current_data.__class__ == casetta.data.\
                                                            ScreenCapture:
                    self.wmain.builder.get_object("ctx_pict_to_prgm").show()
                    self.wmain.builder.get_object("ctx_pict_to_sc").hide()
                    self.wmain.builder.get_object("ctx_sc_to_pict").show()
                    self.wmain.builder.get_object("sep_tools").show()
                else:
                    self.wmain.builder.get_object("ctx_pict_to_prgm").hide()
                    self.wmain.builder.get_object("ctx_pict_to_sc").hide()
                    self.wmain.builder.get_object("ctx_sc_to_pict").hide()
                    self.wmain.builder.get_object("sep_tools").hide()
                self.wmain.builder.get_object("sep_new").show()
                self.wmain.builder.get_object("renommer1").show()
                self.wmain.builder.get_object("supprimer2").show()
            time = event.time
            self.wmain.builder.get_object("menu").popup(None, None, \
                                                None, event.button, time)
    
    def new_program(self, gui=None):
        """Make a new program"""
        new_prgm = self.wmain.file_data.new_record(casetta.data.Program)
        # The name must be checked (to force to give a name)
        self.check_name(new_prgm, True)
        if new_prgm.name == '':
            # The user has canceled the creation. Delete new_prgm
            try:
                self.wmain.file_data.remove(new_prgm)
            except ValueError:
                # The data have been removed by check_name
                pass
        self.refresh_data_list()
        self.wmain.file_changed = True
        self.wmain.refresh_title()
    
    def new_picture(self, gui=None):
        """Make a new picture"""
        new_picture = self.wmain.file_data.new_record(casetta.data.Picture)
        # Fill properties
        new_picture.color_byte = 0
        new_picture.pallet = casetta.pictures.DEFAULT_PALLET
        new_picture.set_picture([casetta.pictures.WHITE] * (128 * 64))
        # The name must be checked (to force to give a name)
        self.check_name(new_picture, True)
        if new_picture.name == '':
            # The user has canceled the creation. Delete new_picture
            try:
                self.wmain.file_data.remove(new_picture)
            except ValueError:
                # The data have been removed by check_name
                pass
        self.refresh_data_list()
        self.wmain.file_changed = True
        self.wmain.refresh_title()

    
    def new_screen_capture(self, gui=None):
        """Make a new screen capture"""
        new_sc = self.wmain.file_data.new_record(casetta.data.ScreenCapture)
        # Fill properties
        new_sc.color_byte = 0
        new_sc.pallet = casetta.pictures.DEFAULT_PALLET
        new_sc.set_picture([casetta.pictures.WHITE] * (128 * 64))
        # The name must be checked (to force to give a name)
        self.check_name(new_sc, True)
        if new_sc.name == '':
            # The user has canceled the creation. Delete new_sc
            try:
                self.wmain.file_data.remove(new_sc)
            except ValueError:
                # The data have been removed by check_name
                pass
        self.refresh_data_list()
        self.wmain.file_changed = True
        self.wmain.refresh_title()
    
    def rename_data(self, gui=None):
        """Rename a data"""
        if self.wmain.current_data == None:
            return
        # Run check_name and force the renaming
        self.check_name(self.wmain.current_data, True)
        self.refresh_data_list()
        self.wmain.file_changed = True
        self.wmain.refresh_title()
        self.wmain.select(None)
        
    def delete_data(self, gui):
        """Delete a data"""
        if self.wmain.current_data == None:
            return
        if dialogs.question(_('Do you really want to delete the data "%s"?') %\
                            self.wmain.current_data.get_name()):
            self.wmain.file_data.remove(self.wmain.current_data)
            self.wmain.current_data = None
            self.refresh_data_list()
            self.wmain.file_changed = True
            self.wmain.refresh_title()
            self.wmain.select(None)
    
    def select_data_to_export(self, gui, path, user_data):
        """Add/Remove a data to/from the export list"""
        if self.wmain.current_data == None:
            return
        model, column = user_data
        model[path][column] = not model[path][column]
        data_id = self.wmain.file_data.data.index(self.wmain.current_data)
        if model[path][column]:
            #Check if data_id of self.current_data is in self.file_data.export
            if not data_id in self.wmain.file_data.export:
                self.wmain.file_data.export.append(data_id)
        else:
            #Check if self.current_id is NOT in self.file_data.export
            if data_id in self.wmain.file_data.export:
                self.wmain.file_data.export.remove(data_id)
    
    def convert_pict_to_sc(self, gui=None):
        """Convert a picture into a screen capture"""
        new_sc = self.wmain.file_data.new_record(casetta.data.ScreenCapture)
        # Fill properties
        new_sc.color_byte = self.wmain.current_data.color_byte
        new_sc.pallet = self.wmain.current_data.pallet
        new_sc.raw_data = self.wmain.current_data.raw_data
        new_sc.name = self.wmain.current_data.name
        # The name must be checked
        self.check_name(new_sc)
        if new_sc in self.wmain.file_data.data:
            # Everything is ok, delete the old current_data
            self.wmain.file_data.remove(self.wmain.current_data)
            self.wmain.current_data = new_sc
            self.wmain.select(None)
            self.refresh_data_list()
            self.wmain.file_changed = True
            self.wmain.refresh_title()
    
    def convert_sc_to_pict(self, gui=None):
        """Convert a screen capture into a picture"""
        new_pict = self.wmain.file_data.new_record(casetta.data.Picture)
        # Fill properties
        new_pict.color_byte = self.wmain.current_data.color_byte
        new_pict.pallet = self.wmain.current_data.pallet
        new_pict.raw_data = self.wmain.current_data.raw_data
        new_pict.name = self.wmain.current_data.name
        # The name must be checked
        self.check_name(new_pict)
        if new_pict in self.wmain.file_data.data:
            # Everything is ok, delete the old current_data
            self.wmain.file_data.remove(self.wmain.current_data)
            self.wmain.current_data = new_pict
            self.wmain.select(None)
            self.refresh_data_list()
            self.wmain.file_changed = True
            self.wmain.refresh_title()
    
    def make_prgm_from_pict(self, gui=None):
        """Make a program from a picture/screen capture"""
        new_prgm = self.wmain.file_data.new_record(casetta.data.Program)
        new_prgm.set_text(self.wmain.current_data.get_program())
        new_prgm.name = self.wmain.current_data.name
        self.check_name(new_prgm)
        self.refresh_data_list()
        self.wmain.file_changed = True
        self.wmain.refresh_title()
