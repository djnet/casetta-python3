# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o First release, code from casetta_gtk
# o New *working* syntax highlighting
# o Add a search/replace function
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""GUI functions to manage the program editor tab"""
__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Pango

import casetta

import dialogs
import fill_equiv

class ProgramEditor:
    """This class contain all function needed to manage the program editor"""
    
    def __init__(self, main_window):
        """Initialize the program editor tab manager"""
        self.wmain = main_window
        self.signals = {'on_equivlist_button_press_event': \
                                        self.on_equivlist_button_press_event,
                        'equiv_plane_view': self.equiv_plane_view,
                        'equiv_casio_view': self.equiv_casio_view,
                        'set_prgm_base': self.on_base_prgm_toggled,
                        'set_prgm_password': self.on_password_changed,
                        'cut_text': self.cut_text,
                        'copy_text': self.copy_text,
                        'paste_text': self.paste_text,
                        'delete_text': self.delete_text,
                        'search': self.new_search,
                        'forward_search': self.next_search,
                        'backward_search': self.prev_search,
                        'replace_all': self.replace_all}
        # Accelerators for search next/previous
        # self.wmain.builder.get_object("search_next_menu").add_accelerator(\
        #                             "activate", \
        #                             self.wmain.accel_group, \
        #                             ord('G'), \
        #                             Gdk.ModifierType.CONTROL_MASK, \
        #                             Gtk.AccelFlags.VISIBLE)
        # self.wmain.builder.get_object("search_prev_menu").add_accelerator(\
        #                             "activate", \
        #                             self.wmain.accel_group, \
        #                             ord('G'), \
        #                             Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK, \
        #                             Gtk.AccelFlags.VISIBLE)
        
        self.tabs = self.wmain.builder.get_object("tabs")
        self.prgm_tab = self.tabs.get_nth_page(1)
        self.equiv_frame = self.wmain.builder.get_object("equiv_vbox")
        
        self.base = self.wmain.builder.get_object("base_prgm")
        self.password = self.wmain.builder.get_object("prgm_password")
        
        # Init the Equiv List
        equiv_list = self.wmain.builder.get_object("equivList")
        self.equiv_ts = Gtk.TreeStore(str)
        equiv_list.set_model(self.equiv_ts)
        equiv_tvc = Gtk.TreeViewColumn('equivalences')
        equiv_tvc.set_resizable(False)
        equiv_list.append_column(equiv_tvc)
        equiv_cell = Gtk.CellRendererText()
        equiv_tvc.pack_start(equiv_cell, True)
        equiv_tvc.add_attribute(equiv_cell, 'text', 0)
        # Fill the Equiv list
        fill_equiv.plane(self.equiv_ts)
        
        # Init text editor
        self.editor = self.wmain.builder.get_object("editor")
        self.ebuffer = self.editor.get_buffer()
        self.ebuffer.connect("changed", self.on_editor_change)
        # self.editor.set_wrap_mode(Gtk.WrapMode.NONE)
        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        self.editor.modify_font(Pango.FontDescription(\
                                                self.wmain.prefs.editor_font))
        self.start_search = self.ebuffer.get_start_iter()
        self.end_search = self.ebuffer.get_start_iter()
        # Set the way to color:
        #self.color = self.no_color
        self.color = self.new_color
        
        # Init the syntax coloration
        self.tag_table = self.ebuffer.get_tag_table()
        self.variable_tag = Gtk.TextTag.new("variable")
        self.variable_tag.set_property("foreground", \
                                       self.wmain.prefs.variable_color)
        self.tag_table.add(self.variable_tag)
        self.command_tag = Gtk.TextTag.new("command")
        self.command_tag.set_property("foreground", \
                                      self.wmain.prefs.command_color)
        self.tag_table.add(self.command_tag)
        self.constant_tag = Gtk.TextTag.new("constant")
        self.constant_tag.set_property("foreground", \
                                       self.wmain.prefs.constant_color)
        self.tag_table.add(self.constant_tag)
        self.comment_tag = Gtk.TextTag.new("comment")
        self.comment_tag.set_property("foreground", \
                                      self.wmain.prefs.comment_color)
        self.tag_table.add(self.comment_tag)
        self.commands = fill_equiv.make_equiv_list()
        self.commands.remove('\\List ')
        self.commands.remove('\\Mat ')
        self.command_max_len = 6
        for command in self.commands:
            if len(command) > self.command_max_len:
                self.command_max_len = len(command)
        self.variables = [chr(index) for index in range(65, 91)] + \
                         ['\\List ', '\\Mat ', '\\r', '\\theta']
        
        self.data = None
        self.loading = False
        self.search_text = ""
        
    def select(self, data):
        """Load/Hide the program editor"""
        if not data.__class__ == casetta.data.Program:
            self.prgm_tab.hide()
            self.equiv_frame.hide()
            self.data = None
        else:
            # Fill the editor
            self.loading = True
            self.data = data
            self.ebuffer.set_text(data.get_text())
            self.base.set_active(data.use_base)
            self.password.set_text(data.get_password())
            self.loading = False
            self.start_search = self.ebuffer.get_start_iter()
            self.end_search = self.ebuffer.get_start_iter()
            # Syntax coloration for all the text
            if self.wmain.prefs.enable_color :
                self.color()
            # Show the editor
            self.prgm_tab.show()
            self.equiv_frame.show()
            self.tabs.set_current_page(1)
    
    def on_editor_change(self, gui):
        """Save the content of the editor in the file data"""
        if self.data != None and not self.loading:
            self.wmain.file_changed = True
            self.wmain.refresh_title()
            # Synchronize with data
            self.data.set_text(self.ebuffer.get_text(\
                                                self.ebuffer.get_start_iter(), \
                                                self.ebuffer.get_end_iter()))
            # Refresh syntax coloration
            if self.wmain.prefs.enable_color :
                self.color(self.ebuffer.get_insert())
    
    def on_base_prgm_toggled(self, gui):
        """Set if the program use base calculs"""
        if self.data != None and not self.loading:
            self.data.use_base = gui.get_active()
            self.wmain.file_changed = True
            self.wmain.refresh_title()
        
    def on_password_changed(self, gui):
        """Set the program password"""
        if self.data != None and not self.loading:
            self.data.set_password(gui.get_text())
            self.wmain.file_changed = True
            self.wmain.refresh_title()
    
    def on_equivlist_button_press_event(self, treeview, event):
        """On click in the Equiv list"""
        posx = int(event.x)
        posy = int(event.y)
        pthinfo = treeview.get_path_at_pos(posx, posy)
        if pthinfo != None:
            path, col = pthinfo[0:2]
            treeview.grab_focus()
            treeview.set_cursor(path, col, 0)
        model = treeview.get_model()
        iterat = model.get_iter(path)
        content = model[iterat]
        # content[0] -> equiv to insert
        if not model.iter_has_child(iterat) :
            # insert equiv only if no children
            self.ebuffer.insert_at_cursor(content[0])
    
    def equiv_plane_view(self, gui):
        """Show the plane view of equiv list"""
        fill_equiv.plane(self.equiv_ts)
    
    def equiv_casio_view(self, gui):
        """Show the casio view of equiv list"""
        fill_equiv.casio(self.equiv_ts)
    
    def cut_text(self, gui):
        """Cut in the program editor (menu entry)"""
        if self.data == None:
            return
        self.ebuffer.cut_clipboard(self.clipboard, self.editor.get_editable())
        
    def copy_text(self, gui):
        """Copy in the program editor (menu entry)"""
        if self.data == None:
            return
        self.ebuffer.copy_clipboard(self.clipboard)
        
    def paste_text(self, gui):
        """Paste in the program editor (menu entry)"""
        if self.data == None:
            return
        self.ebuffer.paste_clipboard(self.clipboard, None, \
                                self.editor.get_editable())
        
    def delete_text(self, gui):
        """Delete in the program editor (menu entry)"""
        if self.data == None:
            return
        self.ebuffer.delete_selection(True, self.editor.get_editable())
    
    def no_color(self, line = None):
        """Do nothing (if you don't want the syntax coloration)"""
        pass
    
    def new_color(self, text_mark=None):
        """Color the text following the syntax (New way)"""
        if text_mark != None:
            start = self.ebuffer.get_iter_at_mark(text_mark)
            limit = start.copy()
            start.backward_line()
            limit.forward_to_line_end()
        else:
            start = self.ebuffer.get_start_iter()
            limit = self.ebuffer.get_end_iter()
        self.ebuffer.remove_all_tags(start, limit)
        
        while not start.compare(limit) > -1:
            second = start.copy()
            step = 0
            while step < self.command_max_len:
                second.forward_char()
                step += 1
                if start.get_text(second) == "'":
                    # comment
                    second.forward_to_line_end()
                    self.ebuffer.apply_tag_by_name("comment", start, second)
                    start = second
                    break
                elif start.get_text(second) == '"':
                    # string
                    try:
                        second = second.forward_search('"', \
                                          Gtk.TextSearchFlags.TEXT_ONLY, limit)[1]
                    except TypeError:
                        break
                    self.ebuffer.apply_tag_by_name("constant", start, second)
                    start = second
                    break
                elif start.get_text(second).isdigit():
                    # digit
                    self.ebuffer.apply_tag_by_name("constant", start, second)
                    start = second
                    break
                elif start.get_text(second) in self.variables:
                    # variables
                    self.ebuffer.apply_tag_by_name("variable", start, second)
                    start = second
                    break
                elif start.get_text(second) in self.commands:
                    # commands
                    self.ebuffer.apply_tag_by_name("command", start, second)
                    start = second
                    break
            if not start == second:
                start.forward_char()
    
    def new_search(self, gui):
        """Ask for a new search"""
        if self.data == None:
            return
        text = dialogs.input_box(title = _("Search"),
                                 message = _("Search:"),
                                 default_text = self.search_text)
        if text != False:
            self.search_text = text
            self.start_search = self.ebuffer.get_start_iter()
            self.end_search = self.ebuffer.get_start_iter()
            self.search()
    
    def next_search(self, gui):
        """Go to the next instance"""
        self.search()
    
    def prev_search(self, gui):
        """Go to the previous instance"""
        self.search(previous = True)
    
    def search(self, previous = False):
        """Search the next/previous instance of self.search_text"""
        if self.data == None or self.search_text == None:
            return
        if not previous:
            res = \
                    self.end_search.forward_search(self.search_text, \
                                                    Gtk.TextSearchFlags.TEXT_ONLY)
        else:
            res = \
                    self.start_search.backward_search(self.search_text, \
                                                      Gtk.TextSearchFlags.TEXT_ONLY)
        if res != None:
            (self.start_search, self.end_search) = res
            self.ebuffer.select_range(self.start_search, self.end_search)
    
    def replace_all(self, gui):
        """Search some text and replace all instances by another text"""
        if self.data == None:
            return
        text_to_search = dialogs.input_box(title = _("Search and replace"),
                                           message = _("Search:"),
                                           default_text = self.search_text)
        if text_to_search == False:
            return
        replace_by = dialogs.input_box(title = _("Search and replace"),
                                       message = _("Replace all instances of \
'%s' by:") % text_to_search,
                                       default_text = "")
        if replace_by == False:
            return
        current_search = self.ebuffer.get_start_iter().forward_search(
                                                    text_to_search,
                                                    Gtk.TextSearchFlags.TEXT_ONLY)
        while current_search != None:
            offset = current_search[0].get_offset()
            self.ebuffer.delete(current_search[0], current_search[1])
            pos_iter = self.ebuffer.get_iter_at_offset(offset)
            self.ebuffer.insert(pos_iter, replace_by)
            pos_iter = self.ebuffer.get_iter_at_offset(offset)
            current_search = pos_iter.forward_search(
                                                text_to_search,
                                                Gtk.TextSearchFlags.TEXT_ONLY)
