# Gasetta fr (french) translation file
# This file is a part of the Casetta project
# Copyright (C) Florian Birée, 2006-2007
# Florian Birée <florian@biree.name>, 2006-2007.
#
msgid ""
msgstr ""
"Project-Id-Version: Gasetta\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-07-31 00:47+0200\n"
"PO-Revision-Date: 2007-07-28 19:33+0200\n"
"Last-Translator: Florian Birée <florian@biree.name>\n"
"Language-Team: French <florian@biree.name>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"

#: backup_manager.py:179 gasetta.glade:812
msgid "Select a data to see its properties."
msgstr "Sélectionnez une donnée pour voir ses propriétés."

#: backup_manager.py:182 properties.py:84
msgid "yes"
msgstr "oui"

#: backup_manager.py:184 properties.py:86
msgid "no"
msgstr "non"

#: backup_manager.py:186 properties.py:88
msgid "no password"
msgstr "pas de mot de passe"

#: backup_manager.py:190
msgid ""
"Data type: %(type)s\n"
"Name: %(name)s\n"
"Use base calculation: %(base)s\n"
"Password: %(password)s"
msgstr ""
"Type de donnée : %(type)s\n"
"Nom : %(name)s\n"
"Utilise des calculs de bases : %(base)s\n"
"Mot de passe : %(password)s"

#: data_man.py:135
msgid "(maximum %s letters)"
msgstr "(maximum %s caractères)"

#: data_man.py:146 data_man.py:163
msgid "New name"
msgstr "Nouveau nom"

#: data_man.py:146
msgid "Please specify a new name for this data. %s"
msgstr "Veuillez spécifier un nouveau nom pour cette donnée. %s"

#: data_man.py:153
msgid "A data must have a valid name. Do you want to delete this data?"
msgstr ""
"Une donnée doit avoir un nom valide. Voulez-vous supprimer cette donnée ?"

#: data_man.py:163
msgid "This name is already used. Please specify a new name for this data. %s"
msgstr ""
"Ce nom est déjà utilisé. Veuillez spécifier un nouveau nom pour cette "
"donnée. %s"

#: data_man.py:175
msgid "Two data must have different names. Do you want to delete this data?"
msgstr ""
"Deux données doivent avoir des noms différents. Voulez-vous vraiment "
"supprimer cette donnée ?"

#: data_man.py:339
msgid "Do you really want to delete the data \"%s\"?"
msgstr "Voulez-vous vraiment supprimer la donnée « %s » ?"

#: dev_man.py:91 dev_man.py:358
msgid "Serial error: %s"
msgstr "Port série : %s"

#: dev_man.py:98 dev_man.py:329
msgid ""
"The command '%s' is not in your path. To use the current transfer tool, you "
"must install this command."
msgstr ""
"La commande « %s » n'est pas dans votre path. Pour utiliser l'outil de "
"transfert actuel, vous devez installer cette commande."

#: dev_man.py:119
msgid "You must configure your transfer tool first in the preference window."
msgstr ""
"Vous devez d'abord configurer un outil de transfert dans la fenêtre "
"préférence."

#: dev_man.py:131
msgid "You must select a transfer tool first in the preference window."
msgstr ""
"Vous devez d'abord sélectionner un outil de transfert dans la fenêtre "
"préférence."

#: dev_man.py:139
msgid ""
"You haven't choice data to send to your calculator. Select first data by "
"using the checkboxes in the list at the left. Next retry to send data."
msgstr ""
"Vous n'avez pas choisi de donnée à envoyer à votre calculatrice. "
"Sélectionnez d'abord des données en utilisant les cases à cocher dans la "
"liste de gauche. Ensuite, réessayez."

#: dev_man.py:148
msgid "Run your calculator, and set it in the state 'Receiving'."
msgstr "Allumez votre calculatrice et mettez-là dans l'état « Receiving »."

#: dev_man.py:149
msgid "Transfer"
msgstr "Transfert"

#: dev_man.py:170
msgid "Header"
msgstr "En-tête"

#: dev_man.py:177
msgid "%(percent)i%% - %(current)i/%(total)i bytes"
msgstr "%(percent)i%% - %(current)i/%(total)i octets"

#: dev_man.py:183 dev_man.py:293
msgid "Transfering..."
msgstr "Transfert..."

#: dev_man.py:196
msgid "Do you want to replace %(name)s?"
msgstr "Voulez-vous remplacer %(name)s ?"

#: dev_man.py:219
msgid ""
"Your calculator port is not set in the cafix configuration. Do you want to "
"set the serial port from the preferences window in the cafix configuration "
"(this must be as root and at each reboot) ?"
msgstr ""
"Le port de votre calculatrice n'est pas défini dans la configuration de "
"cafix. Voulez-vous définir le port série de la fenêtre des préférences comme "
"port pour cafix (cela doit être fait en tant que root et à chaque "
"redémarrage) ?"

#: dev_man.py:239
msgid "You can now send data from the calculator"
msgstr "Vous pouvez maintenant envoyer des données depuis votre calculatrice."

#: dev_man.py:296
msgid "Waiting..."
msgstr "En attente..."

#: dev_man.py:332
msgid ""
"An error occur during the transfer. Check the configuration of your transfer "
"tool."
msgstr ""
"Une erreur est arrivée pendant le transfert. Vérifiez la configuration de "
"votre outil de transfert."

#: dev_man.py:335
msgid "The calculator is out of memory."
msgstr "La mémoire de la calculatrice est pleine."

#: dev_man.py:337
msgid ""
"The transfer aborted because of a checksum error. \n"
"Please check your link."
msgstr ""
"Le transfert a été annulé à cause d'une erreur de somme de contrôle. \n"
"Veuillez vérifier votre câble."

#: dev_man.py:340
msgid "The calculator refuse to overwrite %s."
msgstr "La calculatrice refuse d'écraser %s."

#: dev_man.py:342
msgid "The calculator sent an unknown message (%s)."
msgstr "La calculatrice a envoyé un message inconnu (%s)."

#: dev_man.py:345
msgid "The transfer has been aborted"
msgstr "Le transfert a été annulé."

#: dev_man.py:347
msgid "Unknown data header or data header error."
msgstr "En-tête de donnée inconnu ou erreur d'en-tête."

#: dev_man.py:350
msgid "All selected data cannot be transfered."
msgstr "Toutes les données sélectionnées ne peuvent pas être transférées."

#: dev_man.py:355
msgid ""
"Warning: %s cannot be transfered. You must unselect it to do the transfer."
msgstr ""
"Attention: %s ne peut pas être transféré. Vous devez le dé-sélectionner "
"poureffectuer le transfert."

#: dialogs.py:105
msgid "OK"
msgstr "Valider"

#: dialogs.py:112
msgid "Replace"
msgstr "Remplacer"

#: dialogs.py:117
msgid "Cancel"
msgstr "Annuler"

#: dialogs.py:144
msgid "Input"
msgstr "Entrée"

#: dialogs.py:153
msgid "Question"
msgstr "Question"

#: dialogs.py:172
msgid "Info"
msgstr "Information"

#: dialogs.py:206
msgid "Open a file..."
msgstr "Ouvrir un fichier"

#: dialogs.py:225
msgid "All managed formats"
msgstr "Tous les formats gérés"

#: dialogs.py:252
msgid "Save file as..."
msgstr "Sauvegarder sous"

#: dialogs.py:298
msgid "This file already exist. Replace it?"
msgstr "Ce fichier existe déjà. Le remplacer ?"

#: file_man.py:77 file_man.py:187
msgid "All files"
msgstr "Tous les fichiers"

#: file_man.py:98 file_man.py:196
msgid ""
"The format of this file wasn't detected. \n"
"It wont be opened."
msgstr ""
"Le format de ce fichier n'a pas été détecté. \n"
"Il ne sera pas ouvert."

#: file_man.py:102
msgid "The file cannot be opened."
msgstr "Le fichier ne peut pas être ouvert"

#: file_man.py:115
msgid "Do you want to save this file?"
msgstr "Voulez-vous sauvegarder ce fichier ?"

#: file_man.py:116
msgid "Unsaved file"
msgstr "Fichier non-sauvegardé"

#: file_man.py:142
msgid "An empty file cannot be saved."
msgstr "Un fichier vide ne peut pas être sauvegardé."

#: file_man.py:159 file_man.py:232
msgid "Gasetta can't find the file format. Please specify it."
msgstr ""
"Gasetta ne peut pas trouver le format de fichier. Veuillez le spécifier."

#: file_man.py:164 file_man.py:237
msgid ""
"The output format you choice can not  manage all input data.\n"
"Please choose another output format."
msgstr ""
"Le format de sortie que vous avez choisi ne peut pas gérer toutes les "
"données d'entrée.\n"
"Veuillez choisir un autre format de sortie."

#: file_man.py:170 file_man.py:241
msgid ""
"Warning: the output format can not manage the followed data types: %s. Do "
"you really want to save the current file in this format?"
msgstr ""
"Attention : le format de sortie ne peut pas gérer les types de données "
"suivant : %s. Voulez-vous vraiment enregistrer le fichier courant dans ce "
"format ?"

#: file_man.py:190
msgid "Import a file..."
msgstr "Importer un fichier"

#: file_man.py:209
msgid ""
"You haven't choice data to export. Select first data by using the checkboxes "
"in the list to the left. Next retry to export."
msgstr ""
"Vous n'avez pas choisi de donnée à envoyer à votre calculatrice. "
"Sélectionnez d'abord des données en utilisant les cases à cocher dans la "
"liste de gauche. Réessayez ensuite."

#: file_man.py:224
msgid "Export data"
msgstr "Exporter"

#: gasetta.py:137
msgid "http://casetta.tuxfamily.org/doc"
msgstr "http://casetta.tuxfamily.org/fr/doc"

#: gasetta.py:224
msgid "No name"
msgstr "Sans nom"

#: misc.py:107
msgid "Newcat format"
msgstr "Fichiers newcat"

#: misc.py:108
msgid "Cafix files"
msgstr "Fichiers cafix"

#: misc.py:109
msgid "Fa-122/Fa-123 files"
msgstr "Fichiers de FA-122/Fa-123"

#: misc.py:110
msgid "Fx-Interface files"
msgstr "Fichiers de Fx-Interface"

#: misc.py:111
msgid "Fx-Interface backups"
msgstr "Backups de Fx-Interface"

#: misc.py:112
msgid "Fx-Interface Pro backups"
msgstr "Backups de Fx-Interface Pro"

#: misc.py:113
msgid "Fx-Interface Pro files"
msgstr "Fichiers de Fx-Interface Pro"

#: misc.py:114
msgid "Fa-124 files"
msgstr "Fichiers de Fa-124"

#: misc.py:115
msgid "Calculator Text Format"
msgstr ""

#: misc.py:116
msgid "Casemul format"
msgstr "Format de Casemul"

#: misc.py:117
msgid "Casiolink/Fa-121 format"
msgstr "Format de Casiolink et de Fa-121"

#: misc.py:118
msgid "Picture format"
msgstr "Fichiers image"

#: misc.py:124
msgid "%s files"
msgstr "Fichiers %s"

#: misc.py:128
msgid "Unknowns"
msgstr "Inconnus"

#: misc.py:128
msgid "Unknown"
msgstr "Inconnu"

#: misc.py:129
msgid "Programs"
msgstr "Programmes"

#: misc.py:129 gasetta.glade:615
msgid "Program"
msgstr "Programme"

#: misc.py:130
msgid "Backups"
msgstr "Sauvegardes"

#: misc.py:130
msgid "Backup"
msgstr "Sauvegarde"

#: misc.py:131
msgid "Pictures"
msgstr "Images"

#: misc.py:131 gasetta.glade:625
msgid "Picture"
msgstr "Image"

#: misc.py:132
msgid "Screen captures"
msgstr "Captures d'écran"

#: misc.py:132 gasetta.glade:638
msgid "Screen capture"
msgstr "Capture d'écran"

#: prgm_editor.py:319
msgid "Search"
msgstr "Rechercher"

#: prgm_editor.py:320 prgm_editor.py:357
msgid "Search:"
msgstr "Rechercher :"

#: prgm_editor.py:356 prgm_editor.py:361
msgid "Search and replace"
msgstr "Rechercher et remplacer"

#: prgm_editor.py:362
msgid "Replace all instances of '%s' by:"
msgstr "Remplacer toutes les occurrences de « %s » par :"

#: properties.py:68
msgid "Select data in the left list to edit it or obtain informations on it."
msgstr ""
"Sélectionnez une donnée dans la liste de gauche pour l'éditer ou obtenir des "
"informations sur elle."

#: properties.py:74
msgid "N/A"
msgstr "Non disponible"

#: properties.py:75
msgid ""
"%(data_type)s\n"
"Name: %(name)s\n"
"Date: %(date)s\n"
"Size: %(size)d bytes\n"
msgstr ""
"%(data_type)s\n"
"Nom : %(name)s\n"
"Date : %(date)s\n"
"Taille : %(size)d octets\n"

#: properties.py:91
msgid ""
"Use base calculation: %(use_base)s\n"
"Password: %(password)s\n"
msgstr ""
"Utilise des calculs de bases : %(use_base)s\n"
"Mot de passes : %(password)s\n"

#: wprefs.py:114
msgid "cafix is not in your path"
msgstr "cafix n'est pas dans votre path"

#: gasetta.glade:7
msgid "Gasetta"
msgstr ""

#: gasetta.glade:22
msgid "_File"
msgstr "_Fichier"

#: gasetta.glade:65
msgid "Import datas from another file."
msgstr "Importer des données depuis un autre fichier."

#: gasetta.glade:66
msgid "_Import"
msgstr "_Importer"

#: gasetta.glade:81
msgid "Export selected data to another file"
msgstr "Exporter des données depuis un autre fichier."

#: gasetta.glade:82
msgid "E_xport"
msgstr "E_xporter"

#: gasetta.glade:115
msgid "_Edit"
msgstr "_Édition"

#: gasetta.glade:164
msgid "Search a text."
msgstr "Rechercher un texte."

#: gasetta.glade:165
msgid "gtk-find"
msgstr ""

#: gasetta.glade:175
msgid "Search the next instance of the text."
msgstr "Recherche la prochaine occurrence du texte."

#: gasetta.glade:176
msgid "Forward search"
msgstr "Rechercher le suivant"

#: gasetta.glade:185
msgid "Search the previous instance of the text."
msgstr "Recherche l'occurrence précédente du texte."

#: gasetta.glade:186
msgid "Backward search"
msgstr "Rechercher le précédent"

#: gasetta.glade:195
msgid "Replace all instances of a text."
msgstr "Remplacer toutes les occurrences du texte."

#: gasetta.glade:196
msgid "gtk-find-and-replace"
msgstr ""

#: gasetta.glade:223
msgid "Ca_lculator"
msgstr "Ca_lculatrice"

#: gasetta.glade:230
msgid "Send datas to the calculator."
msgstr "Envoyer des données à la calculatrice."

#: gasetta.glade:231
msgid "Sen_d data"
msgstr "En_voyer des données"

#: gasetta.glade:246
msgid "Receive datas from the calculator."
msgstr "Recevoir des données depuis la calculatrice."

#: gasetta.glade:247
msgid "_Receive data"
msgstr "_Recevoir des données"

#: gasetta.glade:266
msgid "_Help"
msgstr "_Aide"

#: gasetta.glade:274
msgid "Gasetta manual on the web."
msgstr "Manuel de Gasetta sur internet"

#: gasetta.glade:275
msgid "gtk-help"
msgstr ""

#: gasetta.glade:307
msgid "Nouveau fichier"
msgstr ""

#: gasetta.glade:318
msgid "Ouvrir un fichier"
msgstr ""

#: gasetta.glade:329
msgid "Enregistrer un fichier"
msgstr ""

#: gasetta.glade:349
msgid "Send selected data do the calculator."
msgstr "Envoyer des données à la calculatrice."

#: gasetta.glade:350
msgid "Send"
msgstr "Envoyer"

#: gasetta.glade:362
msgid "Receive data from the calculator."
msgstr "Recevoir des données depuis la calculatrice."

#: gasetta.glade:363
msgid "Receive"
msgstr "Recevoir"

#: gasetta.glade:421
msgid "Show as list"
msgstr "Afficher en liste"

#: gasetta.glade:422
msgid "List"
msgstr "Liste"

#: gasetta.glade:432
msgid "Casio"
msgstr ""

#: gasetta.glade:501
msgid "Actions:"
msgstr "Actions :"

#: gasetta.glade:534
msgid "Make a program to draw this picture"
msgstr "Créer un programme pour dessiner cette image"

#: gasetta.glade:535 gasetta.glade:1116
msgid "Make a program"
msgstr "Créer un programme"

#: gasetta.glade:545
msgid "Convert this picture into a screen capture"
msgstr "Convertir cette image en capture d'écran"

#: gasetta.glade:546 gasetta.glade:1133
msgid "Convert into screen capture"
msgstr "Convertir en capture d'écran"

#: gasetta.glade:559
msgid "Convert this screen capture into a picture"
msgstr "Convertir cette capture d'écran en image"

#: gasetta.glade:560 gasetta.glade:1148
msgid "Convert into picture"
msgstr "Convertir en image"

#: gasetta.glade:582
msgid "Make a new:"
msgstr "Créer un nouveau :"

#: gasetta.glade:661
msgid "Properties"
msgstr "Propriétés"

#: gasetta.glade:682
msgid "Set if the program use base calculation"
msgstr "Définir si le programme utilise des calculs sur les bases"

#: gasetta.glade:683
msgid "Use base"
msgstr "Utiliser des calculs sur les bases"

#: gasetta.glade:692
msgid "Password:"
msgstr "Mot de passe :"

#: gasetta.glade:702
msgid "Set the password of the program"
msgstr "Chercher le mot de passe d'un programme"

#: gasetta.glade:744
msgid "Program editor"
msgstr "Éditeur de programmes"

#: gasetta.glade:785
msgid "Extract selected"
msgstr "Extraire la sélection"

#: gasetta.glade:799
msgid "Extract all"
msgstr "Extraire tout"

#: gasetta.glade:835
msgid "Backup manager"
msgstr "Gestionnaire de sauvegardes"

#: gasetta.glade:858
msgid "Color view"
msgstr "Vue en couleur"

#: gasetta.glade:876
msgid "Orange"
msgstr "Orange"

#: gasetta.glade:897
msgid "Green"
msgstr "Vert"

#: gasetta.glade:918
msgid "Blue"
msgstr "Bleu"

#: gasetta.glade:939
msgid "White"
msgstr "Blanc"

#: gasetta.glade:957
msgid "gtk-zoom-in"
msgstr ""

#: gasetta.glade:974
msgid "gtk-zoom-out"
msgstr ""

#: gasetta.glade:991
msgid "gtk-zoom-100"
msgstr ""

#: gasetta.glade:1033
msgid "Picture editor"
msgstr "Éditeur d'images'"

#: gasetta.glade:1059
msgid "Make a new program."
msgstr "Créer un nouveau programme."

#: gasetta.glade:1060
msgid "_New program"
msgstr "_Nouveau programme"

#: gasetta.glade:1076
msgid "Make a new picture."
msgstr "Créer une nouvelle image."

#: gasetta.glade:1077
msgid "New _picture"
msgstr "Nouvelle _image"

#: gasetta.glade:1093
msgid "Make a new screen capture"
msgstr "Créer une nouvelle capture d'écran"

#: gasetta.glade:1094
msgid "New _screen capture"
msgstr "Nouvelle _capture d'écran"

#: gasetta.glade:1115
msgid "Make a program to draw this picture."
msgstr "Créer un programme pour dessiner cette image."

#: gasetta.glade:1132
msgid "Convert this picture into a screen capture."
msgstr "Convertir cette image en capture d'écran."

#: gasetta.glade:1147
msgid "Convert this screen capture into a picture."
msgstr "Convertir cette capture d'écran en image."

#: gasetta.glade:1168
msgid "Rename this data"
msgstr "Renommer cette donnée"

#: gasetta.glade:1169
msgid "_Rename"
msgstr "_Renommer"

#: gasetta.glade:1192
msgid "About Gasetta"
msgstr "À propos de Gasetta"

#: gasetta.glade:1198
msgid ""
"Copyright © 2006-2007, Florian Birée\n"
"Copyright © 2006, Achraf Cherti"
msgstr ""

#: gasetta.glade:1200
msgid ""
"Gasetta is a casio data manager and a format converter.\n"
"Gasetta is based on the python package casetta."
msgstr ""
"Gasetta est un outil de manipulation de données et un convertisseur de "
"fichiers pour les calculatrices Casio.\n"
"Gasetta est basé sur le module python casetta."

#: gasetta.glade:1203
msgid "Casetta project website"
msgstr "Site web du projet Casetta"

#: gasetta.glade:1551
msgid "translator-credits"
msgstr "Traduction française : Florian Birée <florian@biree.name>"

#: gasetta.glade:1568
msgid "Preferences"
msgstr "Préférences"

#: gasetta.glade:1589
msgid ""
"Posix (GNU/Linux, BSD, MacOS X, Unix)\n"
"MacOS (pre X)\n"
"Windows"
msgstr ""
"Posix (GNU/Linux, BSD, MacOS X, Unix)\n"
"MacOS (pre X)\n"
"Windows"

#: gasetta.glade:1604
msgid "End of line (for newcat and ctf formats):"
msgstr "Fin de ligne (pour les formats newcat et ctf) :"

#: gasetta.glade:1619
msgid "General"
msgstr "Général"

#: gasetta.glade:1637
msgid "Font:"
msgstr "Police :"

#: gasetta.glade:1648
msgid "Select a font"
msgstr "Choisissez une police"

#: gasetta.glade:1678
msgid "Enable syntax highlighting"
msgstr "Activer la coloration syntaxique"

#: gasetta.glade:1697 gasetta.glade:1760 gasetta.glade:1776 gasetta.glade:1792
msgid "Select a color"
msgstr "Sélectionnez une couleur"

#: gasetta.glade:1711
msgid "Commands color:"
msgstr "Couleur des commandes :"

#: gasetta.glade:1721
msgid "Variables color:"
msgstr "Couleur des variables :"

#: gasetta.glade:1734
msgid "Constants color:"
msgstr "Couleur des constantes :"

#: gasetta.glade:1747
msgid "Comments color:"
msgstr "Couleur des commentaires :"

#: gasetta.glade:1816
msgid "<b>Syntax highlighting</b>"
msgstr "<b>Coloration syntaxique</b>"

#: gasetta.glade:1837
msgid "Programs editor"
msgstr "Éditeur de programmes"

#: gasetta.glade:1857
msgid "Serial port:"
msgstr "Port série :"

#: gasetta.glade:1892
msgid "None"
msgstr "Aucun"

#: gasetta.glade:1909
msgid "Serial link (recommended)"
msgstr "Câble série (recommandé)"

#: gasetta.glade:1989
msgid "Console:"
msgstr "Console :"

#: gasetta.glade:2000
msgid "Sudo:"
msgstr "Sudo :"

#: gasetta.glade:2016
msgid "cafix settings"
msgstr "Paramètres de cafix"

#: gasetta.glade:2031
msgid "Other tool"
msgstr "Autre outil"

#: gasetta.glade:2060
msgid "Temporary file format:"
msgstr "Format des fichiers temporaires :"

#: gasetta.glade:2093
msgid "Receive command:"
msgstr "Commande pour l'envoi :"

#: gasetta.glade:2120
msgid "Send command:"
msgstr "Commande pour la réception :"

#: gasetta.glade:2148
msgid ""
"In receive and send command, you can use the following variables: %file for "
"a temporary file in the format you choice, and %port for the port you choice."
msgstr ""
"Dans les commandes d'envoi et de réception, vous pouvez utiliser les "
"variables suivantes : %file pour un fichier temporaire dans le format choisi "
"et %port pour le port série sélectionné."

#: gasetta.glade:2162
msgid "Other tool settings"
msgstr "Paramètres de l'autre outil"

#: gasetta.glade:2182
msgid "Transfer tool"
msgstr "Outil de transfert"

#: gasetta.glade:2238
msgid "label"
msgstr ""

#: gasetta.glade:2270
msgid "gtk-cancel"
msgstr "Annuler"
