# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o First release, code from casetta_gtk
# o The properties tab now display base/password for programs
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""GUI functions to manage the properties tab"""
__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import casetta

class PropertiesManager:
    """This class contain all function needed to manage the properties tab"""
    
    def __init__(self, main_window):
        """Initialize the properties tab manager"""
        self.wmain = main_window
        self.signals = {}
        
        self.info_label = self.wmain.builder.get_object("info")
        self.act_label = self.wmain.builder.get_object("act_label")
        self.pict2prgm = self.wmain.builder.get_object("act_convert_pict_to_prgm")
        self.pict2sc = self.wmain.builder.get_object("act_convert_pict_to_sc")
        self.sc2pict = self.wmain.builder.get_object("act_convert_sc_into_pict")
        
    def select(self, data):
        """Change the properties tab following data"""
        # Dispaly informations about the data
        if data == None:
            message = _("Select data in the left list to edit \
it or obtain informations on it.")
        else:
            if data.date != None:
                date = data.date.strftime('%d %b %Y')
            else:
                date = _('N/A')
            message = _("%(data_type)s\nName: %(name)s\nDate: %(date)s\n\
Size: %(size)d bytes\n") % \
                {'data_type': self.wmain.misc.data_names_sing[data.dType], \
                 'name': data.get_name(), \
                 'date': date, \
                 'size': len(data.raw_data)}
            # Specific data type informations
            if data.__class__ == casetta.data.Program:
                if data.use_base:
                    use_base = _("yes")
                else:
                    use_base = _("no")
                if data.get_password() == '':
                    password = _("no password")
                else:
                    password = data.get_password()
                message += _("Use base calculation: %(use_base)s\n\
Password: %(password)s\n") % {'use_base' : use_base, \
                              'password' : password}
        self.info_label.set_text(message)
        # Hide/Show actions
        if data.__class__ == casetta.data.Picture:
            self.act_label.show()
            self.pict2prgm.show()
            self.pict2sc.show()
            self.sc2pict.hide()
        elif data.__class__ == casetta.data.ScreenCapture:
            self.act_label.show()
            self.pict2prgm.show()
            self.pict2sc.hide()
            self.sc2pict.show()
        else:
            self.act_label.hide()
            self.pict2prgm.hide()
            self.pict2sc.hide()
            self.sc2pict.hide()
