# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o First release, code from casetta_gtk
# o Add backup extracter functions
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""GUI functions to manage the backup manager tab"""
__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import casetta

class BackupManager:
    """This class contain all function needed to manage the backup manager"""
    
    def __init__(self, main_window):
        """Initialize the backup tab manager"""
        self.wmain = main_window
        self.signals = {'bck_list_select': self.bck_list_select,
                        'bck_extract_selected': self.bck_extract_selected,
                        'bck_extract_all': self.bck_extract_all}
        
        self.tabs = self.wmain.builder.get_object("tabs")
        self.backup_tab = self.tabs.get_nth_page(2)
        self.info_lbl = self.wmain.builder.get_object("bck_info")
        self.data_inside = {}
        self.current_name = ""
        # Init data-from-backup list
        data_list = self.wmain.builder.get_object("bck_list")
        self.data_treestore = Gtk.TreeStore(str)
        data_list.set_model(self.data_treestore)
        # add data column
        data_tvcolumn = Gtk.TreeViewColumn('Data')
        data_tvcolumn.set_resizable(False)
        data_list.append_column(data_tvcolumn)
        # add the text renderer
        data_cell = Gtk.CellRendererText()
        data_tvcolumn.pack_start(data_cell, True)
        data_tvcolumn.add_attribute(data_cell, 'text', 0)
        data_tvcolumn.set_visible(True)
        self.data_types = {}
        self.data_items = {}
        
    def select(self, data):
        """Load/Hide the backup manager"""
        if not data.__class__ == casetta.data.Backup:
            self.backup_tab.hide()
        else:
            self.make_data_dic(self.wmain.current_data.find_program_list())
            self.current_name = ""
            self.bck_infos()
            self.refresh_bck_list()
            self.backup_tab.show()
            self.tabs.set_current_page(2)
    
    def refresh_bck_list(self):
        """Refresh the list of data in the backup"""
        self.data_treestore.clear()
        # make the dic of data type section
        self.data_types = {}
        #for (data_type_id, data_name) in self.wmain.misc.data_names.items():
        #    self.data_types[data_type_id] = self.data_treestore.\
        #                                    append(None,[False, data_name])
        prgm_name = self.wmain.misc.data_names['program']
        self.data_types['program'] = self.data_treestore.append(None, \
                                                                [prgm_name])
        #Fill childrens items
        self.data_items = {}
        for name in self.data_inside:
            self.data_items[name] = self.data_treestore.append(\
                                                self.data_types['program'], \
                                                [self.data_inside[name][0]])
    
    def bck_list_select(self, treeview, event):
        """Select a data in the list (and display info)"""
        posx = int(event.x)
        posy = int(event.y)
        pthinfo = treeview.get_path_at_pos(posx, posy)
        if pthinfo == None:
            return
        path, col = pthinfo[0:2]
        treeview.grab_focus()
        treeview.set_cursor(path, col, 0)
        model = treeview.get_model()
        iterat = model.get_iter(path)
        content = model[iterat]
        # content[0] -> text_name
        # content.parent[0] -> Translated type names
        # Get the class
        if content.parent == None:
            self.current_name = ""
        else:
            self.current_name = ""
            for name in self.data_inside:
                if self.data_inside[name][0] == content[0]:
                    self.current_name = name
                    break
        # Display informations
        self.bck_infos()
    
    def bck_extract_selected(self, gui):
        """Extract selected data"""
        if self.current_name == "":
            return
        extracted_data = self.wmain.current_data.get_program_by_name(\
                                                            self.current_name)
        self.wmain.file_data.data.append(extracted_data)
        self.wmain.data_man.check_name(extracted_data)
        self.wmain.data_man.refresh_data_list()
        self.wmain.file_changed = True
        self.wmain.refresh_title()
    
    def bck_extract_all(self, gui):
        """Extract all data"""
        extracted_data_list = []
        for name in self.data_inside:
            extracted_data_list.append(\
                            self.wmain.current_data.get_program_by_name(name))
        self.wmain.file_data.data += extracted_data_list
        for data in extracted_data_list:
            self.wmain.data_man.check_name(data)
        self.wmain.data_man.refresh_data_list()
        self.wmain.file_changed = True
        self.wmain.refresh_title()
    
    def make_data_dic(self, program_list):
        """Save the program_list in a self.data_inside
        
        self.data_inside is a dic like:
        {'name': (text_name, use_base, offset, password)}
        """
        self.data_inside = {}
        for (name, use_base, offset, password) in program_list:
            self.data_inside[name] = (casetta.catdic.text_encode(name, True),
                                      use_base, offset, password)
    
    def bck_infos(self):
        """Display informations about the selected data"""
        if self.current_name == "":
            self.info_lbl.set_text(_("Select a data to see its properties."))
        else:
            if self.data_inside[self.current_name][1]:
                use_base = _("yes")
            else:
                use_base = _("no")
            if self.data_inside[self.current_name][3] == '':
                password = _("no password")
            else:
                password = casetta.catdic.text_encode(\
                                self.data_inside[self.current_name][3], True)
            self.info_lbl.set_text(_("Data type: %(type)s\nName: %(name)s\n\
Use base calculation: %(base)s\nPassword: %(password)s") % \
                        {
                            'type': self.wmain.misc.data_names_sing['program'],
                            'name': self.data_inside[self.current_name][0],
                            'base': use_base,
                            'password': password
                        })
