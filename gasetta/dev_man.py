# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o First release, code from casetta_gtk
# o Check if /dev/casio has the right target
# o Clean cafix temp file
# o Allow cafix to be used in Konsole
# o Manage the serial transfer tool (the transfer can be canceled)
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""GUI functions to manage transfer tools"""
__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import os
import tempfile
import threading
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import GObject
import serial.serialutil
import time

import casetta

import dialogs

class DeviceManager:
    """This class contain all function needed to manage ransfer tools"""
    
    def __init__(self, main_window):
        """Initialize the file manager"""
        self.wmain = main_window
        self.signals = {'send': self.send,
                        'receive': self.receive,
                        'cancel_transfer': self.serial_cancel}
        
        self.wtransfer = self.wmain.builder.get_object("wtransfer")
        self.data_name = self.wmain.builder.get_object("data_name")
        self.progressbar = self.wmain.builder.get_object("progressbar")
    
    def init_tool(self):
        """Check if the current tansfer tool is configured, else do it.
        
        Return a transfer tool object, or None if the configuration is bad.
        """
        # Serial
        if self.wmain.prefs.transfer_tool == 'serial':
            if not hasattr(self, 'serial'):
                Serial = casetta.devices.tool_dic['serial']
                try:
                    self.serial = Serial(self.wmain.prefs.port,
                                         self.serial_status,
                                         self.serial_overwrite)
                except serial.serialutil.SerialException as serial_error:
                    dialogs.error(_("Serial error: %s") % serial_error.message)
                    return None
            return self.serial
        # Cafix
        elif self.wmain.prefs.transfer_tool == 'cafix':
            # Command exists
            if not self.wmain.misc.command_exist('cafix'):
                dialogs.error(_("The command '%s' is not in your \
path. To use the current transfer tool, you must install this command.") % \
                              'cafix')
                return None
            # Port exists
            elif not self.cafix_check_port():
                return None
            else:
                if not hasattr(self, 'cafix'):
                    Cafix = casetta.devices.tool_dic['cafix']
                    self.cafix = Cafix(self.cafix_receive, self.cafix_send)
                return self.cafix
        # Other
        elif self.wmain.prefs.transfer_tool == 'other':
            if not hasattr(self, 'other'):
                # Get the Other class:
                Other = casetta.devices.tool_dic['other']
                format_class = casetta.formats.format_dic.get(
                                                self.wmain.prefs.other_format,
                                                None)
                if format_class == None:
                    dialogs.error(_("You must configure your transfer tool \
first in the preference window."))
                    return None
                self.other = Other(format_class,
                                   self.wmain.prefs.other_input,
                                   self.wmain.prefs.other_output,
                                   self.wmain.prefs.port,
                                   self.other_receive,
                                   self.other_send)
            return self.other
        # None
        else:
            dialogs.error(_("You must select a transfer tool first \
in the preference window."))
            return None
    
    def send(self, gui):
        """Send data to the device"""
        # Check if some data are selected
        if self.wmain.file_data.export == []:
            dialogs.error(_("You haven't choice data to send to your \
calculator. Select first data by using the checkboxes in the list at \
the left. Next retry to send data."))
            return
        # Try to get a transfer tool
        tool = self.init_tool()
        if tool == None:
            return
        #Print the 'select receive state on casio' message
        if not dialogs.info(_("Run your calculator, and set it in \
the state 'Receiving'."), True, _("Transfer")):
            return
        # Run the transfer thread
        transfer_thread = Transfer(self, tool, send = True)
        transfer_thread.start()
    
    def receive(self, gui):
        """Receive data from the device"""
        # Try to get a transfer tool
        tool = self.init_tool()
        if tool == None:
            return
        # Run the transfer thread
        transfer_thread = Transfer(self, tool, receive = True)
        transfer_thread.start()
    
    # Tool-specific functions:
    # For serial
    def serial_status(self, data_name, current_size, total_size, is_header = False):
        """Display the status of the serial transfer tool"""
        if is_header:
            GObject.idle_add(self.data_name.set_text, _("Header"))
        else:
            GObject.idle_add(self.data_name.set_text, data_name)
        try:
            fraction = float(current_size) / float(total_size)
        except ZeroDivisionError:
            fraction = 0.
        message = _("%(percent)i%% - %(current)i/%(total)i bytes") % {
                            'percent': fraction * 100,
                            'current': current_size,
                            'total': total_size}
        GObject.idle_add(self.progressbar.set_text, message)
        GObject.idle_add(self.progressbar.set_fraction, fraction)
        GObject.idle_add(self.wtransfer.set_title, _("Transfering..."))
        GObject.idle_add(self.wtransfer.show)
    
    def serial_overwrite(self, data_name):
        """Ask if the user want to overwrite a data (run by serial)"""
        class Rep:
            """This class define an objet wich contain the answser"""
            def __init__(self):
                """Initialize the anwser"""
                self.answer = None
            def ask(self):
                """Ask the question to get the answer"""
                self.answer = dialogs.question(
                        _("Do you want to replace %(name)s?") % {'name': data_name})
        rep = Rep()
        GObject.idle_add(rep.ask)
        # Wait the answer
        while rep.answer == None:
            time.sleep(1)
        return rep.answer
    
    def serial_cancel(self, gui):
        """Set the cancel flag of the serial transfer tool"""
        self.serial.cancel = True
    
    # For cafix
    def cafix_check_port(self) :
        """Check if /dev/casio exist and has the right target
        
        If not, ask to make the link as root.
        Return True if /dev/casio is Ok, False else.
        
        """
        # Check if the port exists and has the right target
        if not os.path.exists('/dev/casio') or \
                    os.path.realpath('/dev/casio') != self.wmain.prefs.port:
            rep = dialogs.question(_("Your calculator port is not set in the \
cafix configuration. Do you want to set the serial port from the preferences \
window in the cafix configuration (this must be as root and at each reboot) ?"))
            if rep :
                command = '%(sudo)s "ln -s -f %(port)s /dev/casio"' % {
                                'sudo': self.wmain.prefs.cafix_sudo,
                                'port': self.wmain.prefs.port}
                os.system(command)
                return os.path.exists('/dev/casio')
            else:
                return False
        else:
            return True
    
    def cafix_receive(self, tmp_folder):
        """Receive data from a casio using cafix"""
        script = open(os.path.join(tmp_folder,'receive.sh'),'w')
        ctn = "#!/bin/sh\n"
        ctn += "# Receive script made by Gasetta\n"
        ctn += "cd %s\n" % tmp_folder
        ctn += "echo %s\n" % _('You can now send data from the calculator')
        ctn += "cafix -r\n"
        script.write(ctn)
        script.close()
        # Run tmp_folder/receive.sh in a console
        # For konsole : 'sh' '%s'
        command = self.wmain.prefs.cafix_console.replace("%command", "sh").\
                    replace("%args", os.path.join(tmp_folder,'receive.sh'))
        os.system(command)
        os.remove(os.path.join(tmp_folder, 'receive.sh'))
    
    def cafix_send(self, tmp_folder):
        """Send data to a casio using cafix"""
        script_filename = tempfile.mkstemp('.sh')[1]
        script = open(script_filename,'w')
        ctn = "#!/bin/sh\n"
        ctn += "# Send script made by Gasetta\n"
        ctn += "cd %s\n" % tmp_folder
        ctn += "cafix -s *\n"
        script.write(ctn)
        script.close()
        # Run script in a console
        command = self.wmain.prefs.cafix_console.replace("%command", "sh").\
                    replace("%args", script_filename)
        os.system(command)
        os.remove(script_filename)
    
    # For the other tool
    def other_receive(self, cmdline):
        """Receive data from a casio using another transfer tool"""
        os.system(cmdline)
    
    def other_send(self, cmdline):
        """Send data to a casio using another transfer tool"""
        os.system(cmdline)

# Send/Receive thread
class Transfer(threading.Thread):
    """This class make the transfer in another thread"""
    
    def __init__(self, dev_man, tool, send = False, receive = False):
        """Initialize the thread"""
        threading.Thread.__init__(self)
        self.dev_man = dev_man
        self.tool = tool
        self.send = send
        self.receive = receive
        if send:
            self.action = self.run_send
        elif receive:
            self.action = self.run_receive
    
    def run(self):
        """Run method, this is the code that runs while thread is alive."""
        GObject.idle_add(self.dev_man.wtransfer.set_title, _("Transfering..."))
        GObject.idle_add(self.dev_man.progressbar.set_text, "")
        GObject.idle_add(self.dev_man.progressbar.set_fraction, 0.)
        GObject.idle_add(self.dev_man.data_name.set_text, _("Waiting..."))
        GObject.idle_add(self.dev_man.wtransfer.show)
        # Run the right action
        self.action()
    
    def run_send(self):
        """Call the exception manager (and do nothing more)"""
        self.exception_manager()
        GObject.idle_add(self.dev_man.wtransfer.hide)
    
    def run_receive(self):
        """Call the exception manager, and add data (with names check)"""
        received_file = self.exception_manager()
        GObject.idle_add(self.dev_man.wtransfer.hide)
        if received_file == None:
            # Something is wrong, abort
            return
        new_data_list = self.dev_man.wmain.file_data.add_records(received_file)
        for data in new_data_list:
            self.dev_man.wmain.data_man.check_name(data)
        GObject.idle_add(self.dev_man.wmain.data_man.refresh_data_list)
        self.dev_man.wmain.file_changed = True
        GObject.idle_add(self.dev_man.wmain.refresh_title)
    
    def exception_manager(self):
        """Send or receive data, and manage exceptions"""
        try:
            if self.send:
                self.dev_man.wmain.file_data.send(self.tool, export = True)
                return None
            elif self.receive:
                return self.tool.receive()
        except casetta.errors.NoCommand as command:
            message = _("The command '%s' is not in your path. To use \
the current transfer tool, you must install this command.") % command
        except casetta.errors.ToolNotConfigured:
            message = _("An error occur during the transfer. Check the \
configuration of your transfer tool.")
        except casetta.errors.CalcOutOfMemory:
            message = _("The calculator is out of memory.")
        except casetta.errors.ChecksumError:
            message = _("The transfer aborted because of a checksum error. \
\nPlease check your link.")
        except casetta.errors.CannotOverwrite as data_name:
            message = _("The calculator refuse to overwrite %s.") % data_name
        except casetta.errors.BadAnswerFromCalc as byte:
            message = _("The calculator sent an unknown message (%s).") % \
                          hex(ord(byte))
        except casetta.errors.TransferAborted:
            message = _("The transfer has been aborted")
        except casetta.errors.HeaderError:
            message = _('Unknown data header or data header error.')
        except casetta.errors.DataNoManaged as not_managed:
            if not_managed.data_type_list == ['all']:
                message = _("All selected data cannot be transfered.")
            else:
                not_managed_data = [self.dev_man.wmain.misc.\
                                                        data_names[data.dType]
                                    for data in not_managed.data_type_list]
                message = _("Warning: %s cannot be transfered. You must \
unselect it to do the transfer.") % ", ".join(not_managed_data)
        except serial.serialutil.SerialException as serial_error:
            message = _("Serial error: %s") % serial_error.message
        GObject.idle_add(dialogs.error, message)
        return None
