#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" This is a part of Casetta project.

    setup.py is created by Achraf Cherti <achrafcherti@gmail.com>

    (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
    (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
    
    Website: <URL:http://casetta.tuxfamily.org>
    
    This program is under the GNU/GPL.
    
"""

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html

from distutils.core import setup
import os

pjoin = os.path.join

setup(
    name="casetta",
    version="0.3.8",
    url="http://https://gitlab.com/djnet/casetta-python3",
    former_url="http://casetta.tuxfamily.org",
    download_url="http://casetta.tuxfamily.org/download",
    description="CASIO graphics calculator programs manager",
    long_description= """\
Casetta is a python package wich can convert casio data in various formats.
Casetta can also transfert data with the calculator.
Casetta_cli is a command line interface for casetta, and casetta_gtk a
graphical interface using the GTK toolkit.
""",
    author="Florian Birée",
    author_email="florian@biree.name",
    license="GNU General Public License",
    platforms=["Unix", "Windows"],
    classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Environment :: X11 Applications :: GTK',
          'Environment :: Win32 (MS Windows)',
          'Environment :: MacOS X',
          'Intended Audience :: End Users/Desktop',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU General Public License (GPL)',
          'Natural Language :: English',
          'Natural Language :: French',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Microsoft :: Windows',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Utilities',
          ],
    scripts=['w32_postinst.py'],
    packages=['casetta'],
    package_data={'casetta': [pjoin("casetta", "changelog"),
                              pjoin("casetta", "license"),
                              pjoin("casetta", "authors"),
                              pjoin("casetta", "README")]},
    data_files=[
        # casetta_cli
            # bin
            ("bin", [pjoin("casetta_cli", "launcher", "casetta")]),

            # share/casetta_cli
            (pjoin("share", "casetta_cli"), [
                    pjoin("casetta_cli", "casetta_cli.py"),
                    pjoin("casetta_cli", "prefs.py"),
                    ]),
            # share/doc/casetta_cli
            (pjoin("share", "doc", "casetta_cli"), [
                    pjoin("casetta_cli", "changelog"),
                    pjoin("casetta_cli", "license"),
                    pjoin("casetta_cli", "authors"),
                    pjoin("casetta_cli", "README"),
                    ]),
            # share/man/man1
            (pjoin("share", "man", "man1"), [
                    pjoin("casetta_cli", "man", "casetta.1.gz"),
                    ]),
            # share/doc/casetta_cli/examples
            (pjoin("share", "doc", "casetta_cli", "examples"), [
                    pjoin("casetta_cli", "examples", "empty.newcat"),
                    pjoin("casetta_cli", "examples", "helloworld.newcat"),
                    ]),
        # gasetta
            # bin
            ("bin", [pjoin("gasetta", "launcher", "gasetta")]),
            # share/gasetta
            (pjoin("share", "gasetta"), [
                    pjoin("gasetta", "backup_manager.py"),
                    pjoin("gasetta", "gasetta.ui"),
                    pjoin("gasetta", "gasetta.ico"),
                    pjoin("gasetta", "gasetta.py"),
                    pjoin("gasetta", "gasetta.svg"),
                    pjoin("gasetta", "data_man.py"),
                    pjoin("gasetta", "dev_man.py"),
                    pjoin("gasetta", "dialogs.py"),
                    pjoin("gasetta", "file_man.py"),
                    pjoin("gasetta", "fill_equiv.py"),
                    pjoin("gasetta", "misc.py"),
                    pjoin("gasetta", "pict_editor.py"),
                    pjoin("gasetta", "prefs.py"),
                    pjoin("gasetta", "prgm_editor.py"),
                    pjoin("gasetta", "properties.py"),
                    pjoin("gasetta", "wprefs.py"),
                    ]),
            # share/applications
            (pjoin("share", "applications"), [pjoin("gasetta",
                                                    "applications",
                                                    "gasetta.desktop")
                                             ]),
            # share/mime/packages
            (pjoin("share", "mime", "packages"), [pjoin("gasetta",
                                                        "mime", "packages",
                                                        "gasetta.xml")
                                                 ]),
            # share/doc/gasetta
            (pjoin("share", "doc", "gasetta"), [
                    pjoin("gasetta", "changelog"),
                    pjoin("gasetta", "license"),
                    pjoin("gasetta", "authors"),
                    pjoin("gasetta", "README"),
                    ]),
            # share/man/man1
            (pjoin("share", "man", "man1"), [
                    pjoin("gasetta", "man", "gasetta.1.gz"),
                    ]),
            # share/locale/*
            (pjoin("share", "locale", "fr", "LC_MESSAGES"),
             [pjoin("gasetta", "locale", "fr", "LC_MESSAGES",
                    "gasetta.mo")])
    ]
)

# vim:ai:et:sw=4:ts=4:sts=4:tw=78:fenc=utf-8
