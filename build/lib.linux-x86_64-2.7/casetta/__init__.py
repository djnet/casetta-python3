# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
#  Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o Add the management of cat backups
#   o Split this file:
#       - exceptions are now in errors.py
#       - data management classes are now in data.py
#       - formats management functions and variables are now in formats.py
#       - fxi, fxb,fxm and fxd save and open functions are now in fxi.py
#       - cat and newcat functions, text and cat decode and encode functions
#         and base16 cat functions are now in catdic.py
#       - g1r open function is now in fa124.py
#       - cafix save and open functions are now in cafix.py
#       - ctf functions are now in ctfdic.py
#       - devices management functions are now in devices.py
#       - is_base function is now in catdic.py
#
# # 0.2.1 version:
#
#   o version number aligned on ctfdic.py
#
# # 0.2.0 version:
#
#   This file was split in two file: this file is the CORE part, and the 
#   casetta_cli.py is the CLI part. See this file for changelog of this part.
#
#   o Change the formats and datas way of internal description
#   o Add a check_name function
#   o Add a copy function to copy a Data
#   o Add a split_by_data_type function
#   o Add a date metadata (datetime.date type)
#   o All save functions can now save all or a part of datas (export flag)
#   o Change the way to work of casio_import and casio_export
#   o New exception system
#   o casio_import fill the date metadata
#   o New data type: backups
#   o cafix format can managed backup datas
#   o casio format can managed backup datas
#   o Add the fxb format
#   o Add the fxm format (read only)
#   o Add the date metadata management for newcat format (newcat 0.2)
#   o newcat format can managed backup (in input/output) (newcat 0.2)
#   o Add a password metadata (string type), managed in cat and newcat 0.2
#   o Add the fxd format (read only)
#   o Add the g1r format (read only - with base management)
#   o Correct a bug in text/cat encode/decode about *, /, "\DispR-Tbl" and
#     "\DrawWeb"
#   o Allow a file format to have many extensions
#   o Internal base programs management
#   o Base programs management in followed format:
#      newcat, cat, fxd, fxi
#   o Management of the Calculator Text Format for programs, in read and write
#     access, following the pre-1 (Revision A) version, avaible in:
#     <URL:http://members.lycos.co.uk/rfam/archive/ctf/ctf.html>
#   o Auto-detection of base programs in cafix and ctf formats
#   o Add a title option in text/cat encode/decode function to convert equivs 
#     in titles and passwords
#   o Management of specials characters in title and passwords of cat and 
#     newcat formats
#   o New way to parse/write cat and newcat formats, many bug fix (such as the
#     capacity bug)
#   o New way to manage transfers tool
#   o New transfer tool: CaS
#   o New transfer tool: other
#   o Replace os.tmpnam() by os.tempnam()
#   o Disable the CaS transfer tool (the other transfer tool can be used 
#     with CaS)
#   o Use tempfile.mk[ds]temp() instead of os.tempnam() (security reason)
#
# # 0.1.2 version:
#
#   o 'or' bug fix in the fxiDecode function
#   o change the way to found the name of a data in the fxi 
#     format (work with any name length)
# 
# # 0.1.1 version:
#
#   o many bug fix
#
# # 0.1.0 version:
#
#   o first release
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Python module for conversions into casio programs files"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

# Public imports for the package
from . import data
from . import formats
from . import devices
from . import devices_serial
from . import errors
from . import catdic
from . import pictures
from . import backups
