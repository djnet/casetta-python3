# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release (code from __init__.py)
#   o Add exceptions for the serial transfer tool
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Exceptions used in casetta"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0d"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

class FormatCantOpen(Exception):
    """Exception raised when a format cannot open a file (write only)"""

    def __init__(self, format):
        Exception.__init__(self)
        self.format = format

    def __str__(self):
        return repr(self.format.name)

class FormatCantSave(Exception):
    """Exception raised when a format canot save a file (read only)"""

    def __init__(self, format):
        Exception.__init__(self)
        self.format = format

    def __str__(self):
        return repr(self.format.name)


class ToolNotFound(Exception):
    """Exception raised when a tool cannot be used 
    
    (the open function is not found for this tool or the command is not 
    in the path)"""

    def __init__(self, tool):
        Exception.__init__(self)
        self.tool = tool

    def __str__(self):
        return repr(self.tool)

class FormatNotFound(Exception):
    """Exception raised when a function cannot select the right format"""
    pass

class DataNoManaged(Exception):
    """Exception raised when a format cannot manage some data type"""

    def __init__(self, data_type_list):
        Exception.__init__(self)
        self.data_type_list = data_type_list

    def __str__(self):
        return repr(self.data_type_list)

class NoCommand(Exception):
    """Exception raised when a command is not found in the PATH"""

    def __init__(self, command):
        Exception.__init__(self)
        self.command = command

    def __str__(self):
        return repr(self.command)

class ToolNotConfigured(Exception):
    """Exception raised when a mistake arrive using a transfer tool"""
    pass

class CalcOutOfMemory(Exception):
    """To be raised when the calculator is out of memory during a transfer"""
    pass
    
class ChecksumError(Exception):
    """To be raised when the calulator answer a CRC error"""
    pass

class CannotOverwrite(Exception):
    """To be raised when the calculator refuse to overwrite a data"""
    
    def __init__(self, data_name):
        Exception.__init__(self)
        self.data_name = data_name
        
    def __str__(self):
        return repr(self.data_name)

class BadAnswerFromCalc(Exception):
    """To be raised if an unexpected answer is sended by the calculator."""
    
    def __init__(self, byte):
        Exception.__init__(self)
        self.byte = byte
    
    def __str__(self):
        return repr(chr(ord(self.byte)))

class TransferAborted(Exception):
    """To be raised when a transfer is aborted."""
    pass

class HeaderError(Exception):
    """To be raised when the calculator answer an header error or send 
    a bad/ unknown header."""
    pass
