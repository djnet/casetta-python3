# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release (code from __init__.py)
#   o Add picture-specific data properties
#   o Add new classes for each data type
#   o Change methods in FileData to have a better behaviour
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Data management classes for casetta"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import re
from . import catdic
from . import pictures
from . import backups
from . import errors

class Data:
    """Data manipulation class"""
    name_rule = r'^.'
    dType = 'unknown'
    def __init__(self, name = '', raw_data = '', data_type = 'unknown', \
            date = None):
        """Make an empty data"""
        self.name = name
        self.raw_data = raw_data
        self.dType = data_type
        self.date = date

    def __repr__(self):
        """Descript this data"""
        return "<Data '%s', type: '%s'>" % (self.name, self.dType)
    
    def copy(self):
        """Creat a new Data object from this object."""
        return Data(self.name, self.raw_data, self.dType, self.date)
    
    def get_name(self):
        """Return the name in an human readable format"""
        return catdic.text_encode(self.name, True)
        
    def set_name(self, new_name):
        """Set the name from a newcat-like string."""
        self.name = catdic.text_decode(new_name, True)

class Program(Data):
    """Class for a casio program"""
    name_rule = r'^.{1,8}$'
    dType = 'program'
    def __init__(self, name = '', raw_data = '', date = None, password = "", \
            use_base = False):
        """Make an empty program"""
        Data.__init__(self, name, raw_data, self.dType, date)
        # Program specific properties
        self.password = password
        self.use_base = use_base
    
    def copy(self):
        """Creat a new Program object from this object."""
        return Program(self.name, self.raw_data, self.date, \
                       self.password, self.use_base)

    def get_text(self):
        """Return the program in an human readable format"""
        return catdic.text_encode(self.raw_data)

    def set_text(self, text):
        """Set the program from a newcat-like string"""
        self.raw_data = catdic.text_decode(text)
    
    def get_password(self):
        """Return the password in an human readable format"""
        return catdic.text_encode(self.password, True)
        
    def set_password(self, new_password):
        """Set the password from a newcat-like string."""
        self.password = catdic.text_decode(new_password, True)

class Backup(Data):
    """Class for a casio backup"""
    name_rule = r'^.'
    dType = 'backup'
    def __init__(self, name = '', raw_data = '', date = None):
        """Make an empty backup"""
        Data.__init__(self, name, raw_data, self.dType, date)
    
    def find_program_list(self):
        """Find the program list from the backup
        
        Return a list like [(name1, use_base1, offset1, password1),... ]
        
        Where name1 is the raw name of the first program ;
        use_base1 is True if the program use base calculation, False else ;
        offset1 is the offset of the first program ;
        password1 is the raw password of the first program.
        """
        return backups.find_prgm_list(self.raw_data)
    
    def get_program_by_name(self, program_name):
        """Return a program object from its name"""
        # Get metadata list
        metadata_list = self.find_program_list()
        # Find metadata for program_name
        for (name, use_base, offset, password) in metadata_list:
            if name == program_name:
                return Program(name = name,
                               raw_data = backups.find_prgm_data(offset, 
                                                                 self.raw_data),
                               use_base = use_base,
                               password = password)
    
    def copy(self):
        """Creat a new Backup object from this object."""
        return Backup(self.name, self.raw_data, self.date)
    
class Picture(Data):
    """Class for a casio picture"""
    name_rule = r'^Picture[1-6]$'
    dType = 'picture'
    def __init__(self, name = '', raw_data = '', date = None, pallet = None, \
            color_byte = None):
        """Make an empty picture"""
        Data.__init__(self, name, raw_data, self.dType, date)
        # self.pallet must be a list of characters as ['o','g','b','w']
        self.pallet = pallet
        self.color_byte = color_byte
    
    def copy(self):
        """Creat a new Picture object from this object."""
        return Picture(self.name, self.raw_data, self.date, self.pallet, \
                       self.color_byte)
    
    def get_picture(self):
        """Return a list of pixels from the picture"""
        return pictures.raw_to_data(self.raw_data, \
                                    self.color_byte, \
                                    self.pallet)
    
    def set_picture(self, pixels_list):
        """Set the picture from a list of pixels"""
        self.raw_data = pictures.data_to_raw(pixels_list, \
                                             wanted_pallet = self.pallet)
    
    def change_pallet(self, new_pallet, headers = None):
        """Convert the picture from the current pallet to new_pallet.
        
        You can add a header using headers (like pictures.sheets_to_raw).
        """
        self.raw_data = pictures.sheets_to_raw(\
                            pictures.change_sheets_pallet(\
                                    pictures.raw_to_sheets( \
                                        self.raw_data, \
                                        self.color_byte, \
                                        len(self.pallet)), \
                                    self.pallet, new_pallet), \
                            headers)
        self.pallet = new_pallet
        if headers != None:
            # Change the color_byte
            self.color_byte = len(headers[0])

    def get_program(self):
        """Return a program in newcat-like format to draw the picture."""
        return pictures.img_to_basic(self.get_picture())

class ScreenCapture(Picture):
    """Class for a screen capture"""
    name_rule = r'^.'
    dType = 'screencapture'
    def __init__(self, name = '', raw_data = '', date = None, pallet = None, \
            color_byte = None):
        """Make an empty screen capture"""
        Picture.__init__(self, name, raw_data, date, pallet, color_byte)
    
    def copy(self):
        """Creat a new ScreenCapture object from this object."""
        return ScreenCapture(self.name, self.raw_data, self.date, self.pallet, \
                       self.color_byte)

class FileData:
    """Data file manipulation class"""
    def __init__(self, filename = None, format = None):
        """Make a FileData.
        
        If filename is filled, the file given is opened.
        If format is None, the format is autodetected."""
        self.data = []
        self.export = []
        if filename != None:
            self.import_file(filename, format)

    def __repr__(self):
        """Descript this data"""
        out = "<File of %s casio data:\n" % str(len(self.data))
        for data_index in range(len(self.data)) :
            out += "#%s: %s\n" % (str(data_index), \
                                  self.data[data_index].__repr__())
        out += '>'
        return out

    def __getitem__(self, index):
        """The same of self.data[index]"""
        return self.data[index]

    def export_list(self):
        """Return the list of data to be exported."""
        out_list = []
        for index in self.export:
            out_list.append(self.data[index])
        return out_list

    def remove(self, data):
        """Remove the data record."""
        self.data.remove(data)
        # clear the export list because index list has changed
        self.export = []
    
    def new_record(self, data_class, arg_list = [], arg_dic = {}):
        """Make a new data from the data_class (Program, Backup, etc)
        
        The new data object is append to the file data, and the
        the data object is returned.
        arg_list and arg_dic can be used to give arguments in the class
        initialization.
        The name of the returned data should be checked.
        """
        new_data = data_class(*arg_list, **arg_dic)
        self.data.append(new_data)
        return new_data

    def save(self, filename, format=None, ignore_warnings=False, \
             export=False):
        """Save in (a) file(s) the data.

        If all data aren't managed by this format, raise DataNoManaged
        with the list of data type not managed (['all'] for all ;-) )
        Use ignore_warnings = True to save managed data in this format.
        """
        if format == None:
            from .formats import choose_format
            format = choose_format(filename)
        if not format.write:
            raise errors.FormatCantSave(format)
        if not ignore_warnings :
            no_managed_data = self.check_no_managed_data(format, export)
            if no_managed_data != []:
                raise errors.DataNoManaged(no_managed_data)
        afile = format(filename)
        if export:
            afile.save(self.export_list())
        else:
            afile.save(self.data)
        
    def add_records(self, file_data):
        """Add records from another file_data.
        
        Return the list of new records.
        The name of returned data should be checked.
        """
        self.data += file_data.data
        return file_data.data
    
    def import_file(self, filename, format = None):
        """Import all records from filename.
        
        Return the list of new records.
        The name of returned data should be checked.
        """
        if format == None:
            from .formats import choose_format
            format = choose_format(filename)
        if not format.read:
            raise errors.FormatCantOpen(format)
        imported_file = format(filename).open_file()
        return self.add_records(imported_file)

    def list_data_types(self, export=False):
        """Return the list of data types used"""
        ret = []
        if export:
            data_list = self.export_list()
        else :
            data_list = self.data
        for data in data_list:
            if not data.__class__ in ret:
                ret.append(data.__class__)
        return ret

    def check_name(self, checked_data):
        """Return 1 if the name of this data is correct
        
        Return 0 if the name syntax is bad
        Return -1 if the name is already used"""
        # check the syntax of the name
        if re.search(checked_data.name_rule, checked_data.name) == None:
            return 0
        # check if already employed
        for data in self.data:
            if data is not checked_data and \
                    data.__class__ == checked_data.__class__ and \
                    data.name == checked_data.name:
                return -1
        return 1

    def copy_record(self, data):
        """Copy data. Return the copy.
        
        The name of the returned data must be changed.
        """
        copy = data.copy()
        self.data.append(copy)
        return copy

    def split_by_data_type(self):
        """Split this file_data by data type.

        Return a dictionary of file data with data classes as keys.
        
        """
        ret = {}
        for data in self.data:
            if not data.__class__ in list(ret.keys()):
                ret[data.__class__] = FileData()
            ret[data.__class__].data += data.copy()
        return ret

    def check_no_managed_data(self, format, export = False):
        """ Return the list of data classes no managed in format.

        Return [] if all data type are managed, ['all'] if nothing is managed
    
        """
        ret = []
        for data_class in self.list_data_types(export) :
            if not data_class in format.managed_data:
                ret.append(data_class)
        ret.sort()
        fd_list = self.list_data_types()
        fd_list.sort()
        if fd_list == ret :
            ret = ['all']
        return ret
    
    def send(self, tool, export = False):
        """Send data using tool."""
        if export:
            tool.send(self.export_list())
        else:
            tool.send(self.data)

# Static data:
data_type_list = [Data, Program, Backup, Picture, ScreenCapture]
data_type_dic = {'unknown' : Data,
                 'program' : Program,
                 'backup' : Backup,
                 'picture' : Picture,
                 'screencapture' : ScreenCapture}
