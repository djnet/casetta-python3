# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
#  Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#  o Get some code from __init__.py:
#    ctf_open and ctf_save functions
#  o Remove the end file byte from raw data and add it to raw data.
#  o Add options brackets only if needed
#  o Correct the replacement of * and / in ctf_decode
#  o Change the raw parser to have normal Disp ctf tokens
#  o New CtfFile class
#  o Allow to change the end of line in saved files
#
# # 0.2.1 version:
#
#  o Change some non-escaped backslashs
#  o Correct tokens PxlTest, StoV-Win, RclV-Win
#
# # 0.2.0 version:
#
#  o first version (version number aligned on casetta)
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Python module to convert Calculator Text Files tokens for casetta"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import re
from . import catdic
from . import data as datacls

# File format management class
class CtfFile:
    """Calculator Text Format manager."""
    
    name = 'ctf'
    managed_data = [datacls.Program]
    read = True
    write = True
    list_ext = ['ctf', 'txt']
    end_of_line = '\n'
    def __init__(self, filename = None):
        """Make a ctf file object with filename"""
        self.filename = filename
        
    def open_file(self):
        """Open a file in the Calculator Text Format"""
        # Build the FileData
        file_data = datacls.FileData()
        # Open the file and get the content
        ctf_file = open(self.filename, 'r')
        ctn = ctf_file.read()
        ctf_file.close()
        # Uniformize end lien characters (and del wrong end line (\\\n)
        ctn = ctn.replace('\r\n','\n')
        ctn = ctn.replace('\r', '\n')
        ctn = ctn.replace('\\\n', '')
        # Begin to parse the file
        block_type = None # may be 'editor','oldprog','text','raw',None
        #current_index = -1
        data = None
        for line in ctn.split('\n'):
            if line[:2] == '@@':
                # New block
                meta_data_pattern = re.compile(r'''
                ^           # Start of line
                @@          # Start of the block
                \s*         # CaS add a space (not in specification)
                ([\S]+)     # Block type
                \s+         # Separator
                ([^(]+)     # Any non ( character: name
                \s*         # the separator space
                (\(.*\))*   # options
                \s*         # some space
                $           # end of line
                ''', re.VERBOSE)
                meta_data_list = meta_data_pattern.findall(line)[0]
                block = meta_data_list[0].lower()
                name = meta_data_list[1]
                if name == '':
                    name = 'oldprog'
                if name[-1] == ' ':
                    name = name[:-1]
                options = meta_data_list[2].replace('(', '').replace(')', '')
                if block == 'prog' or block == 'program':
                    if '"' in name:
                        block = 'editor'
                    else:
                        block = 'oldprog'
                if block == 'oldprog':
                    block_type = 'oldprog'
                    new_data = file_data.new_record(datacls.Program)
                    new_data.name = ctf_decode(name, True)
                elif block == 'editor':
                    block_type = 'editor'
                    new_data = file_data.new_record(datacls.Program)
                    new_data.name = ctf_decode(name.replace('"', ''), True)
                    if options != '':
                        new_data.password = options
                elif block == 'text':
                    block_type = 'text'
                    #current_index = -1
                elif block == 'raw':
                    block_type = 'raw'
                    #current_index = -1
                else:
                    block_type = None
                    #current_index = -1
            elif line[:1] == '#':
                # Comment, ignored
                pass
            elif block_type == 'editor' or block_type == 'oldprog':
                # Delete inline comments
                re_com = re.compile('(\s+;[^"]*)$')
                line = re_com.sub('', line)
                # Replace space
                re_space = re.compile('(\s{2,})[^"]*$')
                line = re_space.sub(' ', line)
                re_space = re.compile('(\s+)$')
                line = re_space.sub('', line)
                # Parse tokens
                line = ctf_decode(line)
                # remove | (tokens separations)
                line = line.replace('|', '')
                # replace special characters @bnnn
                re_spe = re.compile('(@[bhxo].{3})')
                while re_spe.search(line) != None:
                    start, stop = re_spe.search(line).span()
                    spe_str = line[start:stop]
                    if spe_str[1] == 'd':
                        base = 10
                    elif spe_str[1] == 'h' or spe_str[1] == 'x':
                        base = 16
                    elif spe_str[1] == 'o':
                        base = 8
                    caract = chr(int(spe_str[2:], base))
                    line = line[:start] + caract + line[stop:]
                if line != '':
                    if line[-1] != '\x0C':
                        line += '\r'
                    new_data.raw_data += line
        for data in file_data.data:
            if data.__class__ == datacls.Program:
                data.raw_data += '\xff' # add the end file byte
                data.use_base = catdic.is_base_program(data.raw_data)
        return file_data

    def save(self, data_list):
        """Save a file in the Calculator Text Format"""
        ## Unix end of line
        ctf_file = open(self.filename, 'w')
        ctn = ''
        # For each data, convert in the ctf format
        for data in data_list:
            block_type = ''
            if data.__class__ == datacls.Program:
                block_type = '@@Program'
            if block_type != '':
                # Set the header and metadata
                ctn += block_type + ' "' + ctf_encode(data.name, True) + '"'
                if data.password != '':
                    ctn += ' (' + ctf_encode(data.password, True) + ')'
                ctn += '\n'
                # Write ctf data
                ctn += ctf_encode(data.raw_data) + 2 * '\n'
        # change the end of line, if needed
        ctn = ctn.replace('\r', '\n')
        if self.end_of_line != '\n':
            ctn = ctn.replace('\n', self.end_of_line)
        # join in a newcat file
        ctf_file.write(ctn)
        ctf_file.close()

# Encode / Decode functions
def ctf_decode(text, is_title = False):
    """Convert ctf text into raw data"""
    if is_title:
        for spe_carac, tokens in list(token_title.items()):
            for atoken in tokens:
                text = text.replace(atoken, spe_carac)
        return text
    else:
        output_text = ''
        into_str = False
        cpos = 0
        re_aster = re.compile(r'([^\\])\*')
        text = re_aster.sub('\\1\xA9', text)
        re_slash = re.compile(r'([^\\])/')
        text = re_slash.sub('\\1\xB9', text)
        while cpos < len(text):
            if text[cpos] == '"':
                into_str = not into_str
            if not into_str:
                tokens_dic = tokens_by_len
            else:
                tokens_dic = tokens_for_str
            is_replaced = False
            len_list = list(tokens_dic.keys())
            len_list.reverse()
            for tok_len in len_list:
                if text[cpos:cpos + tok_len] in tokens_dic[tok_len]:
                    output_text += car_for_token[text[cpos:cpos + tok_len]]
                    cpos = cpos + tok_len - 1
                    is_replaced = True
                    break
            if not is_replaced:
                output_text += text[cpos]
            cpos += 1
        output_text = output_text.replace('\\*', '*')
        output_text = output_text.replace('\\/', '/')
        return output_text

def ctf_encode(text, is_title = False):
    """Convert raw data into ctf text"""
    if is_title:
        for spe_carac in list(token_title.keys()):
            text = text.replace(spe_carac, token_title[spe_carac][0])
        return text
    else:
        text = text.replace('*', '\\*')
        text = text.replace('/', '\\/')
        text = text.replace('\xA9', '*')
        text = text.replace('\xB9', '/')
        text = text.replace('\xff', '')
        out = ''
        index = 0
        while index < len(text):
            replaced = False
            for spe_carac in len2 + len1:
                if text[index:index + len(spe_carac)] == spe_carac:
                    out += tokens_for[spe_carac][0]
                    index += len(spe_carac)
                    replaced = True
                    break
            if not replaced:
                out += text[index]
                index += 1
        ## Replace Unix end of line in Windows end of line
        return out

# Tokens management code
def add2dic(atoken, dic):
    """Add the token in a dictionary of len of token"""
    if len(atoken) not in dic:
        dic[len(atoken)] = []
    dic[len(atoken)].append(atoken)

# Tokens store format:
# tokens_for['\x00\x11']=['first', 'second']
# for token exception (wich not match in a string) : the not_in_string list

token_title = {}
not_in_string = []
tokens_for = {}
tokens_for['\xCD'] = ['\\r']                        # r (as theta)
token_title['\xCD'] = ['\\r']
tokens_for['\xCE'] = ['\\th']                       # theta
token_title['\xCE'] = ['\\th']
tokens_for['\x89'] = ['+']                          # plus
token_title['\x89'] = ['+']
tokens_for['\x99'] = ['-']                          # minus
token_title['\x99'] = ['-']
#tokens_for['\xA9'] = ['*']                         # multiply
token_title['\xA9'] = ['*']
#tokens_for['\xB9'] = ['/']                         # divide
token_title['\xB9'] = ['/']
tokens_for['\x87'] = ['(-)']                        # Unary -
tokens_for['\x0C'] = ['_\n', '_']                   # disp triangle thingy
tokens_for['\x0E'] = ['->']                         # assignment arrow
tokens_for['\x13'] = ['=>']                         # 'then' arrow
tokens_for['\x10'] = ['<=']
tokens_for['\x11'] = ['<>']
tokens_for['\x12'] = ['>=']
tokens_for['\x3C'] = ['<']
tokens_for['\x3D'] = ['=']
tokens_for['\x3E'] = ['>']
tokens_for['\x14'] = ['\\f1', 'f1']                 # function memories f1 - f6
tokens_for['\x15'] = ['\\f2', 'f2']
tokens_for['\x16'] = ['\\f3', 'f3']
tokens_for['\x17'] = ['\\f4', 'f4']
tokens_for['\x18'] = ['\\f5', 'f5']
tokens_for['\x19'] = ['\\f6', 'f6']
tokens_for['\x1A'] = ['\\A']                # hexadecimal numbers 0xA to 0xF
tokens_for['\x1B'] = ['\\B']
tokens_for['\x1C'] = ['\\C']
tokens_for['\x1D'] = ['\\D']
tokens_for['\x1E'] = ['\\E']
tokens_for['\x1F'] = ['\\F']
#tokens_for['\x??'] = ['\\%']
#tokens_for['\x2A'] = ['\\*']
#tokens_for['\x2F'] = ['\\/']
tokens_for['\x5F'] = ['\\_']
tokens_for['\xC0'] = ['Ans', 'ans']                 # Answer memory 
tokens_for['\xbb'] = ['%']                          # fraction delimiter
tokens_for['\xA8'] = ['^']                          # x^y
tokens_for['\x0f'] = ['\\exp']                      # E (*10^)
tokens_for['\x8C'] = ['\\dms', '\\.']       # degrees minutes seconds separator
tokens_for['\xD0'] = ['\\pi']
#tokens_for['\x??'] = ['Mcl']
tokens_for['\xd3'] = ['Rnd']
#tokens_for['\x??'] = ['Scl']
#tokens_for['\x??'] = ['Defm']
#tokens_for['\x??'] = ['\\yen']                     # Yen symbol

# Scientific functions

tokens_for['\x81'] = ['sin ']
tokens_for['\x82'] = ['cos ']
tokens_for['\x83'] = ['tan ']
tokens_for['\x91'] = ['arcsin ', 'asin ']
tokens_for['\x92'] = ['arccos ', 'acos ']
tokens_for['\x93'] = ['arctan ', 'atan ']
tokens_for['\xA1'] = ['sinh ']
tokens_for['\xA2'] = ['cosh ']
tokens_for['\xA3'] = ['tanh ']
tokens_for['\xB1'] = ['arsinh ']
tokens_for['\xB2'] = ['arcosh ']
tokens_for['\xB3'] = ['artanh ']
tokens_for['\x85'] = ['ln ']
tokens_for['\xf7\x5c'] = ['log ']
tokens_for['\xa5'] = ['e^']
tokens_for['\xb5'] = ['\\10^']
tokens_for['\x86'] = ['\\sqrt']
tokens_for['\x8b'] = ['\\sqr', '\\^2']
tokens_for['\x96'] = ['\\cbrt']
tokens_for['\x9B'] = ['\\rcp', '\\^-1']
tokens_for['\xB8'] = ['\\nrt']
tokens_for['\x80'] = ['Pol(']
tokens_for['\xa0'] = ['Rec(']
tokens_for['\x7f\x4c'] = ['\\sum(']
tokens_for['\x7f\x26'] = ['$dx(', '\\Integrate(']
tokens_for['\x8d'] = ['d/dx']
tokens_for['\x97'] = ['Abs ']
tokens_for['\xa6'] = ['Int ']
tokens_for['\xb6'] = ['Frac ']
tokens_for['\xde'] = ['Intg ']

# Probability

tokens_for['\xab'] = ['!']
tokens_for['\x88'] = ['nPr', '\\perm']
not_in_string.append('nPr')
tokens_for['\x98'] = ['nCr', '\\comb']
not_in_string.append('nCr')
tokens_for['\xc1'] = ['Ran#']

# Display

tokens_for['\xd1'] = ['Cls']
tokens_for['\xd9'] = ['Norm']
tokens_for['\xdd'] = ['Eng ']
tokens_for['\xe4'] = ['Sci ']
tokens_for['\xe3'] = ['Fix ']
tokens_for['\xda'] = ['Deg']
tokens_for['\xdb'] = ['Rad']
tokens_for['\xdc'] = ['Gra']
tokens_for['\xda'] = ['Deg']
tokens_for['\x9c'] = ['(deg)']
tokens_for['\xac'] = ['(rad)']
tokens_for['\xbc'] = ['(grad)', '(gra)']


# Graphics

tokens_for['\xe0'] = ['Plot ']
tokens_for['\xe1'] = ['Line']
tokens_for['\xea'] = ['Factor ']
tokens_for['\xee'] = ['Graph Y=']
tokens_for['\xf0'] = ['Graph Y>']
tokens_for['\xf1'] = ['Graph Y<']
tokens_for['\xf2'] = ['Graph Y>=']
tokens_for['\xf3'] = ['Graph Y<=']
tokens_for['\xf4'] = ['Graph r=']
tokens_for['\xf7\x62'] = ['Graph X=']
tokens_for['\xf5'] = ['Graph (X,Y)=']
tokens_for['\xef'] = ['Graph $']

# Programming

tokens_for['\xe8'] = ['Dsz ']
tokens_for['\xe9'] = ['Isz ']
tokens_for['\xe2'] = ['Lbl ']
tokens_for['\xec'] = ['Goto ']
tokens_for['\xed'] = ['Prog ']

# Engineering symbols

tokens_for['\x01'] = ['\\femto']
tokens_for['\x02'] = ['\\pico']
tokens_for['\x03'] = ['\\nano']
tokens_for['\x04'] = ['\\micro']
tokens_for['\x05'] = ['\\milli']
tokens_for['\x06'] = ['\\kilo']
tokens_for['\x07'] = ['\\Mega']
tokens_for['\x08'] = ['\\Giga']
tokens_for['\x09'] = ['\\Tera']
tokens_for['\x0a'] = ['\\Peta']
tokens_for['\x0b'] = ['\\Exa']

# Complex numbers

tokens_for['\x7f\x50'] = ['\\i', '\\j']
tokens_for['\x7f\x21'] = ['Det ']
tokens_for['\x7f\x22'] = ['Arg ']
tokens_for['\x7f\x23'] = ['Conjg ']
tokens_for['\x7f\x24'] = ['ReP ', 'Rep ']
tokens_for['\x7f\x25'] = ['ImP ', 'Imp ']

# Matrices

tokens_for['\x7f\x40'] = ['Mat ']
tokens_for['\x7f\x41'] = ['Trn ']
tokens_for['\x7f\x42'] = ['*Row']
tokens_for['\x7f\x43'] = ['*Row+']
tokens_for['\x7f\x44'] = ['Row+']
tokens_for['\x7f\x45'] = ['Swap']

# Table

tokens_for['\x7f\xa0'] = ['\\a(n)']
tokens_for['\x7f\xa1'] = ['\\a(n+1)']
tokens_for['\x7f\xa2'] = ['\\a(n+2)']
tokens_for['\x7f\x64'] = ['\\(n)']
tokens_for['\x7f\xa4'] = ['\\a(0)']
tokens_for['\x7f\xa6'] = ['\\a(1)']

# Base n

tokens_for['\xd4'] = ['Dec']
tokens_for['\xd5'] = ['Hex']
tokens_for['\xd6'] = ['Bin']
tokens_for['\xd7'] = ['Oct']
tokens_for['\x94'] = ['\\d']
tokens_for['\x84'] = ['\\h']
tokens_for['\xb4'] = ['\\b']
tokens_for['\xa4'] = ['\\o']
tokens_for['\xba'] = ['and']
tokens_for['\xaa'] = ['or']
tokens_for['\x9a'] = ['xor']
tokens_for['\x8a'] = ['xnor']
tokens_for['\xb7'] = ['Neg ']

# Stats

#tokens_for['\x??'] = ['\\num']
tokens_for['\x9f'] = ['\\sumx']
tokens_for['\xcf'] = ['\\sumy']
tokens_for['\xdf'] = ['\\sumxy']
tokens_for['\x8f'] = ['\\sumsqrx']
tokens_for['\xbf'] = ['\\sumsqry']
tokens_for['\x8f'] = ['\\sumsqrx']
tokens_for['\xc2'] = ['\\xbar']
tokens_for['\xc3'] = ['\\ybar']
tokens_for['\xc4'] = ['\\xsd', '\\xo(n)']
tokens_for['\xc6'] = ['\\ysd', '\\yo(n)']
tokens_for['\xc5'] = ['\\xsdpop', '\\xo(n-1)']
tokens_for['\xc7'] = ['\\ysdpop', '\\yo(n-1)']
#tokens_for['\x??'] = ['\\regA', '\\yint']              # regression Y intercept
#tokens_for['\x??'] = ['\\regB', '\\slope']             # regression gradient
tokens_for['\xca'] = ['\\cor', '\\correlation']
tokens_for['\xcb'] = ['\\xhat']
tokens_for['\xcc'] = ['\\yhat']
#tokens_for['\x??'] = ['\\CL']                          # statistics data entry
#tokens_for['\x??'] = ['\\DT']                          # statistics data entry
#tokens_for['\x??'] = ['\\DTx']                     # stored stats data accesses
#tokens_for['\x??'] = ['\\DTy']                     # stored stats data accesses
#tokens_for['\x??'] = ['\\DTf']                     # stored stats data accesses
tokens_for['\xfb'] = ['\\P(']                   # Normal distribution functions
tokens_for['\xfc'] = ['\\Q(']
tokens_for['\xfd'] = ['\\R(']
tokens_for['\xfe'] = ['\\t(']

# VAR menu

tokens_for['\x7f\x00'] = ['Xmin']
tokens_for['\x7f\x01'] = ['Xmax']
tokens_for['\x7f\x02'] = ['Xscl']
tokens_for['\x7f\x04'] = ['Ymin']
tokens_for['\x7f\x05'] = ['Ymax']
tokens_for['\x7f\x06'] = ['Yscl']
tokens_for['\x7f\x08'] = ['Tmin', '\\tmin']
not_in_string.append('Tmin')
tokens_for['\x7f\x09'] = ['Tmax', '\\tmax']
not_in_string.append('Tmax')
tokens_for['\x7f\x60'] = ['Sim Coef']
tokens_for['\x7f\x61'] = ['Ply Coef']
#tokens_for['\x??'] = ['Sim X', '\\simx']               # Simultaneous answers
                                                        # (and Y, Z, T, U, V)
#tokens_for['\x??'] = ['Ply X1', '\\plyx1']             # Polynomial ansers
                                                        # and  Ply X2  Ply X3
tokens_for['\x7f\x90'] = ['F Result', '\\fresult']
tokens_for['\x7f\x91'] = ['F Start', '\\fstart']
tokens_for['\x7f\x92'] = ['F End', '\\fend']
tokens_for['\x7f\x93'] = ['F pitch', '\\fpitch']
tokens_for['\x7f\x94'] = ['R Result', '\\rresult']
tokens_for['\x7f\x95'] = ['R Start', '\\rstart']
tokens_for['\x7f\x96'] = ['R End', '\\rend']
#tokens_for['\x7f\x??'] = ['R pitch', '\\rpitch']       # VAR R stuff
tokens_for['\x7f\xf0'] = ['\\gY']
tokens_for['\x7f\xf1'] = ['\\gr']
tokens_for['\x7f\xf2'] = ['\\Xt']
tokens_for['\x7f\xf3'] = ['\\Yt']

# 9800
tokens_for['\x7f\x34'] = ['Orange ']
tokens_for['\x7f\x35'] = ['Blue ']
tokens_for['\x7f\x36'] = ['Green ']
#tokens_for['\x7f\x??'] = ['List X', '\\listx']         # List stuff (9800+)
#tokens_for['\x7f\x??'] = ['List Y', '\\listy']         # List stuff (9800+)

tokens_for['\xeb'] = ['ViewWindow', 'Range']
tokens_for['\x7f\x0a'] = ['Tptch', '\\tptch', 'Tscl', '\\tscl']
not_in_string.append('Tptch')
not_in_string.append('Tscl')

# 9850 specific

tokens_for['\x7f\x8f'] = ['Getkey', 'Getkey ', 'GetKey ']
tokens_for['\x7f\x0d'] = ['D Start', '\\dstart']
tokens_for['\x7f\x0e'] = ['D End', '\\dend']
tokens_for['\x7f\x0f'] = ['D pitch', '\\dpitch']
tokens_for['\x7f\xa5'] = ['\\a(2)']
tokens_for['\x7f\xa7'] = ['\\b(n)']
tokens_for['\x7f\xa8'] = ['\\b(n+1)']
tokens_for['\x7f\xa9'] = ['\\b(n+2)']
tokens_for['\x7f\xaa'] = ['\\b(0)']
tokens_for['\x7f\xab'] = ['\\b(1)']
tokens_for['\x7f\xac'] = ['\\b(2)']
tokens_for['\x7f\xad'] = ['\\a(nStart)']
tokens_for['\x7f\xae'] = ['\\b(nStart)']
tokens_for['\x7f\x4c'] = ['Sum ']
tokens_for['\x7f\x4d'] = ['Prod ']
tokens_for['\x7f\x4e'] = ['Percent ']
tokens_for['\x7f\x51'] = ['List ']
tokens_for['\x7f\x4f'] = ['Cuml ']
tokens_for['\x7f\x27'] = ['d2/dx2(']
tokens_for['\x7f\x28'] = ['Solve(']
tokens_for['\x7f\x46'] = ['Dim ']
tokens_for['\x7f\x47'] = ['Fill ']
tokens_for['\x7f\x4a'] = ['List->Mat(']
tokens_for['\x7f\x4b'] = ['Mat->List(']
tokens_for['\x7f\x10'] = ['RightXmin', '\\rxmin']
tokens_for['\x7f\x11'] = ['RightXmax', '\\rxmax']
tokens_for['\x7f\x12'] = ['RightXscl', '\\rxscl']
tokens_for['\x7f\x14'] = ['RightYmin', '\\rymin']
tokens_for['\x7f\x15'] = ['RightYmax', '\\rymax']
tokens_for['\x7f\x16'] = ['RightYscl', '\\ryscl']
tokens_for['\x7f\x18'] = ['RightTmin', '\\rtmin']
tokens_for['\x7f\x19'] = ['RightTmax', '\\rtmax']
tokens_for['\x7f\x1a'] = ['RightTscl', '\\rtscl']
tokens_for['\x7f\x2a'] = ['Fmin(']
tokens_for['\x7f\x2b'] = ['Fmax(']
tokens_for['\x7f\x2c'] = ['Seq(']
tokens_for['\x7f\x99'] = ['Min(']
tokens_for['\x7f\x2e'] = ['Mean(']
tokens_for['\x7f\x2f'] = ['Median(']
tokens_for['\x7f\x31'] = ['P/L-Orange', '\\plorange']
tokens_for['\x7f\x33'] = ['P/L-Green', '\\plgreen']
tokens_for['\x7f\x32'] = ['P/L-Blue', '\\plblue']
tokens_for['\x7f\x48'] = ['Identity(']
tokens_for['\x7f\x49'] = ['Augment(']
tokens_for['\x7f\x62'] = ['Sim Result', '\\simresult']
tokens_for['\x7f\x63'] = ['Ply Result', '\\plyresult']
tokens_for['\xf7\x00'] = ['If ']
tokens_for['\xf7\x01'] = ['Then ']
tokens_for['\xf7\x02'] = ['Else ']
tokens_for['\xf7\x03'] = ['IfEnd', 'EndIf']
tokens_for['\xf7\x04'] = ['For ']
tokens_for['\xf7\x05'] = [' To ']
tokens_for['\xf7\x06'] = ['Step ']
tokens_for['\xf7\x07'] = ['Next']
tokens_for['\xf7\x08'] = ['While ']
tokens_for['\xf7\x09'] = ['WhileEnd', 'EndWhile']
tokens_for['\xf7\x0a'] = ['Do']
tokens_for['\xf7\x0b'] = ['LpWhile ', 'EndWhile ']
tokens_for['\xf7\x0c'] = ['Return']
tokens_for['\xf7\x0d'] = ['Break']
tokens_for['\xf7\x0e'] = ['Stop']
tokens_for['\x7f\xb0'] = [' And ']
tokens_for['\x7f\xb1'] = [' Or ']
tokens_for['\x7f\xb3'] = ['Not ']
tokens_for['\xf7\x10'] = ['Locate ']
tokens_for['\xf7\x11'] = ['Send(']
tokens_for['\xf7\x12'] = ['Receive(']
tokens_for['\xf7\x18'] = ['ClrText', '\\ctext']
tokens_for['\xf7\x19'] = ['ClrGraph', '\\cgraph']
tokens_for['\xf7\x20'] = ['ClrList', '\\clist']
tokens_for['\xf7\x60'] = ['S-WindAuto', '\\swauto']
tokens_for['\xf7\x61'] = ['S-WindMan', '\\swman']
tokens_for['\xf7\x70'] = ['G-Connect', '\\gcon']
tokens_for['\xf7\x71'] = ['G-Plot', '\\gplot']
tokens_for['\xf7\x78'] = ['BG-None', '\\bgnone']
tokens_for['\xf7\x79'] = ['BG-Pict', '\\bgpict']
tokens_for['\xf7\x88'] = ['VarRange', '\\vrange']
tokens_for['\xf7\x93'] = ['StoPict ']
tokens_for['\xf7\x94'] = ['RclPict ']
tokens_for['\xf7\x97'] = ['StoV-Win ']
tokens_for['\xf7\x98'] = ['RclV-Win ']
tokens_for['\xf7\xa0'] = ['Tangent ']
tokens_for['\xf7\xa1'] = ['Normal ']
tokens_for['\xf7\xa2'] = ['Inverse ']
tokens_for['\xf7\xa3'] = ['Vertical ']
tokens_for['\xf7\xa4'] = ['Horizontal ']
tokens_for['\xf7\xa5'] = ['Text ']
tokens_for['\xf7\xa6'] = ['Circle ']
tokens_for['\xf7\xa7'] = ['F-Line ']
tokens_for['\xf7\xa8'] = ['PlotOn ']
tokens_for['\xf7\xa9'] = ['PlotOff ']
tokens_for['\xf7\xaa'] = ['PlotChg ']
tokens_for['\xf7\xab'] = ['PxlOn ']
tokens_for['\xf7\xac'] = ['PxlOff ']
tokens_for['\xf7\xad'] = ['PxlChg ']
tokens_for['\xf7\xaf'] = ['PxlTest ']
tokens_for['\xf7\xb2'] = ['VarList1']
tokens_for['\xf7\xb3'] = ['VarList2']
tokens_for['\xf7\xb4'] = ['VarList3']
tokens_for['\xf7\xb5'] = ['VarList4']
tokens_for['\xf7\xb6'] = ['VarList5']
tokens_for['\xf7\xb7'] = ['VarList6']
tokens_for['\xf7\xb8'] = ['File1']
tokens_for['\xf7\xb9'] = ['File2']
tokens_for['\xf7\xba'] = ['File3']
tokens_for['\xf7\xbb'] = ['File4']
tokens_for['\xf7\xbc'] = ['File5']
tokens_for['\xf7\xbd'] = ['File6']
tokens_for['\xf7\x7a'] = ['GridOff', '\\goff']
tokens_for['\xf7\x7d'] = ['GridOn', '\\gon']
tokens_for['\xf7\xc0'] = ['FuncOn', '\\fon']
tokens_for['\xf7\xd0'] = ['FuncOff', '\\foff']
tokens_for['\xf7\xc1'] = ['SimulOn', '\\son']
tokens_for['\xf7\xd1'] = ['SimulOff', '\\soff']
tokens_for['\xf7\xc2'] = ['AxesOn', '\\aon']
tokens_for['\xf7\xd2'] = ['AxesOff', '\\aoff']
tokens_for['\xf7\xc3'] = ['CoordOn', '\\con']
tokens_for['\xf7\xd3'] = ['CoordOff', '\\coff']
tokens_for['\xf7\xc4'] = ['LabelOn', '\\labon']
tokens_for['\xf7\xd4'] = ['LabelOff', '\\laboff']
tokens_for['\xf7\xc5'] = ['DerivOn', '\\don']
tokens_for['\xf7\xd5'] = ['DerivOff', '\\doff']
tokens_for['\xf7\xc6'] = ['LocusOn', '\\locon']
tokens_for['\xf7\xd6'] = ['LocusOff', '\\locoff']
tokens_for['\xf7\xc7'] = ['EdispOn', '\\eon']
tokens_for['\xf7\xd7'] = ['EdispOff', '\\eoff']

tokens_by_len = {}
tokens_for_str = {}
car_for_token = {}
for spe_car, token_list in list(tokens_for.items()):
    for token in token_list:
        if token[-1] == ' ':
            tok_space = True
        else:
            tok_space = False
        if token.lower() == token:
            tok_lower = True
        else:
            tok_lower = False
        add2dic(token, tokens_by_len)
        car_for_token[token] = spe_car
        add2dic('\\' + token, tokens_by_len)
        car_for_token['\\' + token] = spe_car
        if not tok_lower:
            add2dic(token.lower(), tokens_by_len)
            car_for_token[token.lower()] = spe_car
        if tok_space:
            add2dic(token[:-1], tokens_by_len)
            car_for_token[token[:-1]] = spe_car
            add2dic('\\' + token[:-1], tokens_by_len)
            car_for_token['\\' + token[:-1]] = spe_car
            if not tok_lower:
                add2dic(token[:-1].lower(), tokens_by_len)
                car_for_token[token[:-1].lower()] = spe_car
        if not token in not_in_string:
            add2dic(token, tokens_for_str)

len1 = []
len2 = []
for spe_car in tokens_for:
    if len(spe_car) == 1:
        len1.append(spe_car)
    else:
        len2.append(spe_car)
