# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Pictures file format manager for casetta"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

from PIL import Image
import os
from . import pictures
from . import data as datacls

# File format management class
class ImageFile:
    """Standard picture file format manager."""
    
    name = 'pil'
    managed_data = [datacls.Picture, datacls.ScreenCapture]
    read = True
    write = True
    list_ext = ['bmp', 'jpg', 'jpeg', 'png', 'gif']
    def __init__(self, filename = None):
        """Make a picture file object with filename"""
        self.filename = filename

    def open_file(self):
        """Open an image in any PIL-managed format, return a filedata"""
        # Build the FileData
        file_data = datacls.FileData()
        new_data = file_data.new_record(datacls.Picture)
        # Write metadata
        new_data.name = 'Picture1'
        new_data.pallet = pictures.DEFAULT_PALLET
        new_data.color_byte = 0
        # Write data
        img = Image.open(self.filename)
        new_data.raw_data = pictures.data_to_raw(pictures.normalize(img), \
                                                 pictures.DEFAULT_PALLET)
        return file_data

    def save(self, data_list):
        """Save a picture"""
        folder = os.path.dirname(self.filename)
        ext = os.path.splitext(self.filename)[1]
        for data in data_list:
            if data.__class__ == datacls.Picture or \
                    data.__class__ == datacls.ScreenCapture :
                # make a filename from the name
                if len(data_list) == 1 and \
                        os.path.basename(self.filename) != '':
                    # try to use filename as name
                    data_filename = self.filename
                else:
                    data_filename = os.path.join(folder, data.name + ext)
                # write data
                img = Image.new('RGB', pictures.CASIO_SIZE) 
                img.putdata(pictures.raw_to_data(data.raw_data, \
                            data.color_byte, data.pallet))
                img.save(data_filename)
