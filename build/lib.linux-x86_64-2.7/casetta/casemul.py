# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2007 Florian Birée aka Thesa <florian@biree.name>
#
# Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o First release
#   o Add the management of casemul files (*.cas) for programs (read/write)
#   o New CasemulFile class
#   o Change the \Disp behaviour
#   o Add the DOTALL flag in regular expressions
#   o Allow to read/write casemul pictures
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Functions and token dictionaries to manage casemul files (*.cas)"""

__author__ = "Florian Birée"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2007, Florian Birée"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

import re
from . import data as datacls
from . import catdic
from . import pictures

# File format management class
class CasemulFile:
    """Casemul file format manager."""
    
    name = 'casemul'
    managed_data = [datacls.Program, datacls.Picture]
    read = True
    write = True
    list_ext = ['cas']
    def __init__(self, filename = None):
        """Make a casemul file object with filename"""
        self.filename = filename

    def open_file(self):
        """Open a casemul file, return a FileData object"""
        # Build the FileData
        file_data = datacls.FileData()
        # Open the file and get the content
        casemul_file = open(self.filename, 'r')
        ctn = casemul_file.read()
        casemul_file.close()
        #Remove the file header
        re_file_header = re.compile(r'''
            (^ACFS                  # Begining of the file
            [\x00][\x01][\x00]{2}[\x18]
            .[\x00]{4}.{3}[\x18][\x00]{7}
            RSEC[\x00][\x01][\x00]{2}[\x24][\x00]{3}
            .{4}    # Number of each data type
            <[\x00]{3}
            .{4}    # len file header + prog block
            .{4}    # len file header + prog and pict block
            .{4}    # len file header + prog, pict and mat block
            [\x00]
            .{3})
            ''', re.VERBOSE | re.DOTALL)
        ctn = re_file_header.sub('', ctn)
        del re_file_header
        #Make the re for a program record
        data_pattern = re.compile('''
            [\x00]{3}                       # 3 last bytes of len of filename
                                            # always null if len(filename)<=8
            ([^\x00]+)                      # Any non \x00 characters: filename
            .{4}                            # First program len
            (RPGO)                          # Data type (only programs)
            [\x00][\x01][\x00]{2}           # Separator
            [\x10][\x00]{3}
            .{4}                            # Second program len
            ([^\x00]+)                      # Any non \x00 characters : raw data
            ($|[^\x00])
            ''', re.VERBOSE | re.DOTALL)
        data_list = data_pattern.findall(ctn)
        for record in data_list:
            # record = ('FILE NAME', 'DATA TYPE', 'RAW DATA')
            if record[1] == 'RPGO' :
                new_data = file_data.new_record(datacls.Program)
                new_data.name = casemul_decode(record[0], True)
                new_data.raw_data = casemul_decode(record[2])
                new_data.use_base = catdic.is_base_program(record[2])
        del data_pattern, data_list
        #Make the re for a picture record
        picture_pattern = re.compile('''
            .[\x00]{3}                      # len of filename (3 last bytes
                                            # always null if len(filename)=8)
            (Picture[1-9])                  # Filename
            .{4}                            # First picture len
            (IPTC)                          # Data type (only pictures)
            [\x00][\x01][\x00]{2}           # Separator
            [\x10][\x00]{3}
            .{4}                            # Second picture len
            (.{8192})                       # 8192 bytes : the picture
            ''', re.VERBOSE | re.DOTALL)
        picture_list = picture_pattern.findall(ctn)
        for record in picture_list:
            # record = ('FILE NAME', 'DATA TYPE', 'PICTURE DATA')
            if record[1] == 'IPTC' :
                if int(record[0][-1]) < 7:
                    # Casemul has 9 pictures. If the picture is > 6 then
                    # it will be saved as screencapture
                    new_data = file_data.new_record(datacls.Picture)
                else:
                    new_data = file_data.new_record(datacls.ScreenCapture)
                new_data.name = casemul_decode(record[0], True)
                new_data.raw_data = casemul_picture_decode(record[2])
                new_data.pallet = pictures.DEFAULT_PALLET
                new_data.color_byte = 0
        del picture_pattern, record, picture_list
        return file_data

    def save(self, data_list):
        """Save data from data_list in a casemul file"""
        casemul_file = open(self.filename, 'w')
        # Make type records
        prog_nbr, pict_nbr, mat_nbr, list_nbr = 0, 0, 0, 0
        prog_rec, pict_rec, mat_rec, list_rec = '', '', '', ''
        for data in data_list:
            if data.__class__ == datacls.Program:
                casemul_data = casemul_encode(data.raw_data)
                casemul_name = casemul_encode(data.name, True)
                # Make the record header
                tmp_rec = cas_len(len(casemul_name))
                tmp_rec += casemul_name
                tmp_rec += cas_len(len(casemul_data) + 16)
                tmp_rec += 'RPGO' # Data type
                tmp_rec += '\x00\x01' + '\x00' * 2 + '\x10' + '\x00' * 3
                tmp_rec += cas_len(len(casemul_data))
                # Make the record body (convert text)
                tmp_rec += casemul_data
                # Add to programs records
                prog_rec += tmp_rec
                prog_nbr += 1
            elif data.__class__ == datacls.Picture:
                casemul_data = casemul_picture_encode(data.get_picture())
                casemul_name = casemul_encode(data.name, True)
                # Make the picture header
                tmp_rec = cas_len(len(casemul_name))
                tmp_rec += casemul_name
                tmp_rec += cas_len(len(casemul_data) + 16)
                tmp_rec += 'IPTC' # Data type
                tmp_rec += '\x00\x01' + '\x00' * 2 + '\x10' + '\x00' * 3
                tmp_rec += '\x80\x40\x00\x00' #cas_len(len(casemul_data))
                # Make the record body
                tmp_rec += casemul_data
                # Add to picture records
                pict_rec += tmp_rec
                pict_nbr += 1
        # Add the file header
        #   This header is composed by static data and variable data
        #   Here, variable data is pick from a sample cas file.
        var_data1 = '\xfd\xb1\x7e'
        var_data2 = '\x01M_'
        file_header = 'ACFS\x00\x01\x00\x00' + '\x18' + '\x00' * 4
        file_header += var_data1
        file_header += '\x18' + '\x00' * 7 + 'RSEC\x00\x01\x00\x00'
        file_header += '\x24' + '\x00' * 3
        file_header += chr(prog_nbr) + chr(pict_nbr) + chr(mat_nbr) + \
                       chr(list_nbr)
        file_header += '<' + '\x00' * 3
        file_header += cas_len(60 + len(prog_rec))
        file_header += cas_len(60 + len(prog_rec) + len(pict_rec))
        file_header += cas_len(60 + len(prog_rec) + len(pict_rec) + \
                       len(mat_rec))
        file_header += '\x00' + var_data2
        # Make the file
        ctn = file_header + prog_rec + pict_rec + mat_rec + list_rec
        # Write file
        casemul_file.write(ctn)
        casemul_file.close()

# Decode/Encode functions
def casemul_encode(raw_data, title=False):
    """Convert raw text to casemul-like text"""
    if title:
        # For the moment, no conversion
        return raw_data
    # Remove the end caract ('\xff')
    raw_data = raw_data.replace('\xff', '')
    # Replace ctokens by tokens
    out = ''
    cpos = 0
    ctoken_list, token_for = get_r2cas_data()
    while cpos < len(raw_data):
        replaced = False
        for ctoken in ctoken_list:
            if ctoken in raw_data[cpos:cpos + len(ctoken)]:
                out += token_for[ctoken]
                cpos = cpos + len(ctoken) - 1
                replaced = True
                break
        if not replaced :
            out += raw_data[cpos]
        cpos += 1
    # Change end line ('\r' to '\r\n')
    out = out.replace('\r', '\r\n')
    return out

def casemul_decode(casemul_data, title=False):
    """Convert casemul-like text to raw text"""
    if title:
        # For the moment, no conversion
        return casemul_data
    #Change end line '\r\n' to '\r'
    casemul_data = casemul_data.replace('\r\n', '\r')
    #Convert ctokens in tokens
    out = ''
    cpos = 0
    token_list, ctoken_for = get_cas2r_data()
    while cpos < len(casemul_data):
        replaced = False
        for token in token_list:
            if token in casemul_data[cpos:cpos + len(token)]:
                out += ctoken_for[token]
                cpos = cpos + len(token) - 1
                replaced = True
                break
        if not replaced :
            out += casemul_data[cpos]
        cpos += 1
    # Add the last caract ('\xff')
    out += '\xff'
    return out

def casemul_picture_encode(pixel_list):
    """Convert a pixel list into casemul picture"""
    # Convert the row sorted data into col sorted data
    col_sorted_data = [pictures.WHITE] * (128 * 64)
    for index in range(len(pixel_list)):
        col = index % 128
        row = index / 128
        new_index = (col * 64) + row
        col_sorted_data[new_index] = pixel_list[index]
    casemul_pict = ''
    for pixel in col_sorted_data:
        if pixel == pictures.ORANGE:
            casemul_pict += '\x01'
        elif pixel == pictures.GREEN:
            casemul_pict += '\x02'
        elif pixel == pictures.BLUE:
            casemul_pict += '\x03'
        else:
            casemul_pict += '\x00'
    return casemul_pict

def casemul_picture_decode(casemul_picture):
    """Convert casemul picture into casio picture
    
    The pallet will be pictures.DEFAULT_PALLET, and the colour byte 0.
    """
    col_sorted_data = []
    for byte in casemul_picture:
        if ord(byte) == 1:
            col_sorted_data.append(pictures.ORANGE)
        elif ord(byte) == 2:
            col_sorted_data.append(pictures.GREEN)
        elif ord(byte) == 3:
            col_sorted_data.append(pictures.BLUE)
        else:
            col_sorted_data.append(pictures.WHITE)
    # Convert the col sorted data into row sorted data
    row_sorted_data = [pictures.WHITE] * (128 * 64)
    for index in range(len(col_sorted_data)):
        col = index / 64
        row = index % 64
        new_index = (row * 128) + col
        row_sorted_data[new_index] = col_sorted_data[index]
    # Convert the row sorted data into casio picture
    return pictures.data_to_raw(row_sorted_data)

# Misc functions
def cas_len(txt_len, output_len=4):
    """Return a string of hexadecimal in a casemul like style
    
    txt_len is the lenght to be encoded,
    output_len is the lenght of the output string, in bytes.
    """
    len_str = '0' * (output_len*2 - len(hex(txt_len)[2:])) + hex(txt_len)[2:]
    out = ''
    for index in range(len(len_str)/2):
        if index == 0:
            out += chr(int(len_str[-2:], 16))
        else:
            out += chr(int(len_str[-(index*2)-2:-(index*2)], 16))
    return out


# Casemul token data and functions
def get_r2cas_data():
    """Return necessary data to make the convertion
    
    Return the sorted by descendant len list of ctoken and the dictionary of
    tokens"""
    len1 = []
    len2 = []
    token_for = {}
    for ctoken in r2cas:
        if len(ctoken) == 2:
            len2.append(ctoken)
        else:
            len1.append(ctoken)
        token_for[ctoken] = r2cas[ctoken][0]
    return (len2 + len1, token_for)

def get_cas2r_data():
    """Return necessary data to make the convertion
    
    Return the sorted by descendant len list of tokens and the dictionary of
    ctokens"""
    len_dic = {}
    ctoken_for = {}
    for ctoken in r2cas:
        for token in r2cas[ctoken]:
            if not len(token) in len_dic:
                len_dic[len(token)] = []
            len_dic[len(token)].append(token)
            ctoken_for[token] = ctoken
    len_list = list(len_dic.keys())
    len_list.sort()
    len_list.reverse()
    token_list = []
    for alen in len_list:
        token_list += len_dic[alen]
    return (token_list, ctoken_for)

r2cas = {}
r2cas['\x01'] = ['fempto']
r2cas['\x02'] = ['\xc1']
r2cas['\x03'] = ['\xc2']
r2cas['\x04'] = ['\xc3']
r2cas['\x05'] = ['\xc4']
r2cas['\x06'] = ['\xc5']
r2cas['\x07'] = ['\xc6']
r2cas['\x08'] = ['\xc7']
r2cas['\x09'] = ['\xc8']
r2cas['\x0a'] = ['\xc9']
r2cas['\x0b'] = ['\xca']
#r2cas['\x0c'] = ['\xbb']
r2cas['\x0c'] = ['\xbb\r']
#r2cas['\x0d'] = ['']
r2cas['\x0e'] = ['\x87']
r2cas['\x0f'] = ['\x94']
r2cas['\x10'] = ['\xae']
r2cas['\x11'] = ['\xac']
r2cas['\x12'] = ['\xaf']
r2cas['\x13'] = ['\xee']
r2cas['\x14'] = ['f\xf1']
r2cas['\x15'] = ['f\xf2']
r2cas['\x16'] = ['f\xf3']
r2cas['\x17'] = ['f\xf4']
r2cas['\x18'] = ['f\xf5']
r2cas['\x19'] = ['f\xf6']
r2cas['\x1a'] = ['hA']
r2cas['\x1b'] = ['hB']
r2cas['\x1c'] = ['hC']
r2cas['\x1d'] = ['hD']
r2cas['\x1e'] = ['hE']
r2cas['\x1f'] = ['hF']
#r2cas['\x3a'] = ['']
r2cas['\x3c'] = ['\x3c']
r2cas['\x3d'] = ['\x3d']
r2cas['\x3e'] = ['\x3e']
r2cas['\x3f'] = ['\x3f']
r2cas['\x7f\x00'] = ['Xmin']
r2cas['\x7f\x01'] = ['Xmax']
r2cas['\x7f\x02'] = ['Xscl']
r2cas['\x7f\x04'] = ['Ymin']
r2cas['\x7f\x05'] = ['Ymax']
r2cas['\x7f\x06'] = ['Yscl']
r2cas['\x7f\x08'] = ['T\x96min']
r2cas['\x7f\x09'] = ['T\x96max']
r2cas['\x7f\x0a'] = ['T\x96ptch']
r2cas['\x7f\x0b'] = ['Xfct']
r2cas['\x7f\x0c'] = ['Yfct']
r2cas['\x7f\x0d'] = ['D Start']
r2cas['\x7f\x0e'] = ['D End']
r2cas['\x7f\x0f'] = ['D pitch']
r2cas['\x7f\x10'] = ['RightXmin']
r2cas['\x7f\x11'] = ['RightXmax']
r2cas['\x7f\x12'] = ['RightXscl']
r2cas['\x7f\x14'] = ['RightYmin']
r2cas['\x7f\x15'] = ['RightYmax']
r2cas['\x7f\x16'] = ['RightYscl']
r2cas['\x7f\x18'] = ['RightT\x96min']
r2cas['\x7f\x19'] = ['RightT\x96max']
r2cas['\x7f\x1a'] = ['RightT\x96ptch']
r2cas['\x7f\x20'] = ['Max(']
r2cas['\x7f\x21'] = ['Det ']
r2cas['\x7f\x22'] = ['Arg ']
r2cas['\x7f\x23'] = ['Conj ']
r2cas['\x7f\x24'] = ['ReP ']
r2cas['\x7f\x25'] = ['ImP ']
r2cas['\x7f\x26'] = ['d/dx(']
r2cas['\x7f\x27'] = ['d2\xa4dxc(']
r2cas['\x7f\x28'] = ['Solve(']
r2cas['\x7f\x29'] = ['\x98(']
#r2cas['\x7f\x2a'] = ['\x7f\x2a'] # Fmin(
r2cas['\x7f\x2b'] = ['FMax(']
r2cas['\x7f\x2c'] = ['Seq(']
r2cas['\x7f\x99'] = ['Min(']
r2cas['\x7f\x2e'] = ['Mean(']
#r2cas['\x7f\x2f'] = ['\x7f\x2f'] # Median(
r2cas['\x7f\x31'] = ['P/L-Orange']
r2cas['\x7f\x32'] = ['P/L-Blue']
r2cas['\x7f\x33'] = ['P/L-Green']
r2cas['\x7f\x34'] = ['Orange ']
r2cas['\x7f\x35'] = ['Blue ']
r2cas['\x7f\x36'] = ['Green ']
r2cas['\x7f\x37'] = ['OrangeG ']
r2cas['\x7f\x38'] = ['BlueG ']
r2cas['\x7f\x39'] = ['GreenG ']
r2cas['\x7f\x40'] = ['Mat ']
r2cas['\x7f\x41'] = ['Trn ']
r2cas['\x7f\x42'] = ['*Row ']
r2cas['\x7f\x43'] = ['*Row+ ']
r2cas['\x7f\x44'] = ['Row+ ']
r2cas['\x7f\x45'] = ['Swap ']
r2cas['\x7f\x46'] = ['Dim ']
r2cas['\x7f\x47'] = ['Fill(']
r2cas['\x7f\x48'] = ['Identity ']
r2cas['\x7f\x49'] = ['Augment(']
r2cas['\x7f\x4a'] = ['List\x87Mat(']
r2cas['\x7f\x4b'] = ['Mat\x87List(']
r2cas['\x7f\x4c'] = ['Sum ']
r2cas['\x7f\x4d'] = ['Prod ']
r2cas['\x7f\x4e'] = ['Percent ']
r2cas['\x7f\x4f'] = ['Cuml ']
r2cas['\x7f\x50'] = ['\xb8']
r2cas['\x7f\x51'] = ['List ']
r2cas['\x7f\x60'] = ['Sim Coef']
r2cas['\x7f\x61'] = ['Ply Coef']
r2cas['\x7f\x62'] = ['Sim Result']
r2cas['\x7f\x63'] = ['Ply Result']
r2cas['\x7f\x6a'] = ['List1']
r2cas['\x7f\x6b'] = ['List2']
r2cas['\x7f\x6c'] = ['List3']
r2cas['\x7f\x6d'] = ['List4']
r2cas['\x7f\x6e'] = ['List5']
r2cas['\x7f\x6f'] = ['List6']
r2cas['\x7f\x76'] = ['Q\xf1']
r2cas['\x7f\x77'] = ['Q\xf3']
r2cas['\x7f\x78'] = ['x\xf1']
r2cas['\x7f\x79'] = ['y\xf1']
r2cas['\x7f\x7a'] = ['x\xf2']
r2cas['\x7f\x7b'] = ['y\xf2']
r2cas['\x7f\x7c'] = ['x\xf3']
r2cas['\x7f\x7d'] = ['y\xf3']
r2cas['\x7f\x8f'] = ['Getkey']
r2cas['\x7f\x90'] = ['F Result']
r2cas['\x7f\x91'] = ['F Start']
r2cas['\x7f\x92'] = ['F End']
r2cas['\x7f\x93'] = ['F pitch']
r2cas['\x7f\x94'] = ['R Result']
r2cas['\x7f\x95'] = ['R Start']
r2cas['\x7f\x96'] = ['R End']
r2cas['\x7f\xa0'] = ['a\xcc']
r2cas['\x7f\xa1'] = ['a\xcc+']
r2cas['\x7f\xa3'] = ['\xcc']
r2cas['\x7f\xa7'] = ['b\xcc']
r2cas['\x7f\xa8'] = ['b\xcc+']
r2cas['\x7f\xad'] = ['a\xccStart']
r2cas['\x7f\xae'] = ['b\xccStart']
r2cas['\x7f\xb0'] = [' And ']
r2cas['\x7f\xb1'] = [' Or ']
r2cas['\x7f\xb3'] = ['Not ']
r2cas['\x7f\xf0'] = ['Y']
#r2cas['\x7f\xf1'] = ['r']
r2cas['\x7f\xf2'] = ['Xt']
r2cas['\x7f\xf3'] = ['Yt']
r2cas['\x80'] = ['Pol(']
r2cas['\x81'] = ['sin ']
r2cas['\x82'] = ['cos ']
r2cas['\x83'] = ['tan ']
r2cas['\x84'] = ['Hex>']
r2cas['\x85'] = ['ln ']
r2cas['\x86'] = ['\xa5']
r2cas['\x87'] = ['\x2d']
r2cas['\x88'] = ['\x9f']
r2cas['\x89'] = ['\x2b']
r2cas['\x8a'] = ['xnor']
r2cas['\x8b'] = ['\xe2']
r2cas['\x8c'] = ['\xa2']
r2cas['\x8d'] = ['\x9b(']
r2cas['\x8e'] = ['Mod']
r2cas['\x8f'] = ['\x98x\xe2']
r2cas['\x91'] = ['sin\xfc ']
r2cas['\x92'] = ['cos\xfc ']
r2cas['\x93'] = ['tan\xfc ']
r2cas['\x94'] = ['Dec>']
r2cas['\x95'] = ['log ']
r2cas['\x96'] = ['\xe3\xa5']
r2cas['\x97'] = ['Abs ']
r2cas['\x98'] = ['\x8c']
r2cas['\x99'] = ['-']   ###
r2cas['\x9a'] = ['xor']
r2cas['\x9c'] = ['\x86']
r2cas['\x9e'] = ['Med']
r2cas['\x9f'] = ['\x98x']
r2cas['\xa0'] = ['Rec(']
r2cas['\xa1'] = ['sinh ']
r2cas['\xa2'] = ['cosh ']
r2cas['\xa3'] = ['tanh ']
r2cas['\xa4'] = ['Oct>']
r2cas['\xa5'] = ['exp ']
r2cas['\xa6'] = ['Int ']
r2cas['\xa7'] = [' Not ']
r2cas['\xa8'] = ['^']
r2cas['\xa9'] = ['\xa3']
r2cas['\xaa'] = ['or']
r2cas['\xab'] = ['!']
r2cas['\xac'] = ['Rad>']
r2cas['\xad'] = ['minY']
r2cas['\xae'] = ['minX']
#r2cas['\xaf'] = ['n']
r2cas['\xb1'] = ['sinh\xfc ']
r2cas['\xb2'] = ['cosh\xfc ']
r2cas['\xb3'] = ['tanh\xfc ']
r2cas['\xb4'] = ['Bin>']
r2cas['\xb5'] = ['\xa0']
r2cas['\xb6'] = ['Frac ']
r2cas['\xb7'] = ['Neg ']
r2cas['\xb8'] = ['\xb0\xa5']
r2cas['\xb9'] = ['\xa4']
r2cas['\xba'] = ['and']
r2cas['\xbb'] = ['\xa1']
r2cas['\xbc'] = ['\x84']
r2cas['\xbd'] = ['maxY']
r2cas['\xbe'] = ['maxX']
r2cas['\xbf'] = ['\x98y\xe2']
r2cas['\xc0'] = ['Ans']
r2cas['\xc1'] = ['Ran#']
r2cas['\xc3'] = ['\xb3']
r2cas['\xc4'] = ['x\x93n']
r2cas['\xc5'] = ['x\x93n\xfd']
r2cas['\xc6'] = ['y\x93n']
r2cas['\xc7'] = ['y\x93n\xfd']
#r2cas['\xca'] = ['r']
r2cas['\xcb'] = ['\xb4']
r2cas['\xcc'] = ['\xb5']
#r2cas['\xcd'] = ['r']
r2cas['\xce'] = ['\x96']
r2cas['\xcf'] = ['\x98y']
r2cas['\xd0'] = ['\x9a']
r2cas['\xd1'] = ['Cls']
r2cas['\xd3'] = ['Rnd']
r2cas['\xd4'] = ['Dec']
r2cas['\xd5'] = ['Hex']
r2cas['\xd6'] = ['Bin']
r2cas['\xd7'] = ['Oct']
r2cas['\xd9'] = ['Norm']
r2cas['\xda'] = ['Deg']
r2cas['\xdb'] = ['Rad']
r2cas['\xdc'] = ['Gra']
r2cas['\xdd'] = ['Eng']
r2cas['\xde'] = ['Intg  ']
r2cas['\xdf'] = ['\x98xy']
r2cas['\xe0'] = ['Plot ']
r2cas['\xe1'] = ['Line']
r2cas['\xe2'] = ['Lbl ']
r2cas['\xe3'] = ['Fix ']
r2cas['\xe4'] = ['Sci ']
r2cas['\xe8'] = ['Dsz ']
r2cas['\xe9'] = ['Isz ']
r2cas['\xea'] = ['Factor ']
r2cas['\xeb'] = ['ViewWindow ']
r2cas['\xec'] = ['Goto ']
r2cas['\xed'] = ['Prog ']
r2cas['\xee'] = ['Graph Y=']
r2cas['\xef'] = ['Graph \x9b ']
r2cas['\xf0'] = ['Graph Y>']
r2cas['\xf1'] = ['Graph Y<']
r2cas['\xf2'] = ['Graph Y\xaf']
r2cas['\xf3'] = ['Graph Y\xae']
r2cas['\xf4'] = ['Graph r=']
r2cas['\xf5'] = ['Graph(X,Y)=d(']
r2cas['\xf7\x00'] = ['If ']
r2cas['\xf7\x01'] = ['Then ']
r2cas['\xf7\x02'] = ['Else ']
r2cas['\xf7\x03'] = ['IfEnd']
r2cas['\xf7\x04'] = ['For ']
r2cas['\xf7\x05'] = ['To ']
r2cas['\xf7\x06'] = ['Step ']
r2cas['\xf7\x07'] = ['Next']
r2cas['\xf7\x08'] = ['While ']
r2cas['\xf7\x09'] = ['WhileEnd']
r2cas['\xf7\x0a'] = ['Do']
r2cas['\xf7\x0b'] = ['LpWhile  ']
r2cas['\xf7\x0c'] = ['Return']
r2cas['\xf7\x0d'] = ['Break']
r2cas['\xf7\x0e'] = ['Stop']
r2cas['\xf7\x10'] = ['Locate ']
r2cas['\xf7\x11'] = ['Send(']
r2cas['\xf7\x12'] = ['Receive(']
r2cas['\xf7\x18'] = ['ClrText']
r2cas['\xf7\x19'] = ['ClrGraph']
r2cas['\xf7\x1a'] = ['ClrList']
r2cas['\xf7\x20'] = ['DrawGraph']
r2cas['\xf7\x22'] = ['DrawDyna']
r2cas['\xf7\x23'] = ['DrawStat']
r2cas['\xf7\x24'] = ['DrawFTG-Con']
r2cas['\xf7\x25'] = ['DrawFTG-Plt']
r2cas['\xf7\x26'] = ['DrawR-Con']
r2cas['\xf7\x27'] = ['DrawR-Plt']
r2cas['\xf7\x28'] = ['DrawR-SumCon']
r2cas['\xf7\x29'] = ['DrawR-SumPlt']
r2cas['\xf7\x2a'] = ['DrawWeb ']
r2cas['\xf7\x2e'] = ['DispF-Tbl']
r2cas['\xf7\x2f'] = ['DispR-Tbl']
r2cas['\xf7\x40'] = ['1-Variable ']
r2cas['\xf7\x41'] = ['2-Variable ']
r2cas['\xf7\x42'] = ['LinearReg ']
r2cas['\xf7\x43'] = ['Med-MedLine ']
r2cas['\xf7\x44'] = ['QuadReg ']
r2cas['\xf7\x45'] = ['CubicReg ']
r2cas['\xf7\x46'] = ['QuartReg ']
r2cas['\xf7\x47'] = ['LogReg ']
r2cas['\xf7\x48'] = ['ExpReg ']
r2cas['\xf7\x49'] = ['PowerReg ']
r2cas['\xf7\x4a'] = ['S-Gph1 ']
r2cas['\xf7\x4b'] = ['S-Gph2 ']
r2cas['\xf7\x4c'] = ['S-Gph3 ']
r2cas['\xf7\x4d'] = ['Square']
r2cas['\xf7\x4e'] = ['Cross']
r2cas['\xf7\x4f'] = ['Dot']
r2cas['\xf7\x50'] = ['Scatter']
r2cas['\xf7\x51'] = ['xyLine']
r2cas['\xf7\x52'] = ['Hist']
r2cas['\xf7\x53'] = ['MedBox']
r2cas['\xf7\x54'] = ['MeanBox']
r2cas['\xf7\x55'] = ['N-Dist']
r2cas['\xf7\x56'] = ['Broken']
r2cas['\xf7\x57'] = ['Linear']
r2cas['\xf7\x58'] = ['Med-Med']
r2cas['\xf7\x59'] = ['Quad']
r2cas['\xf7\x5a'] = ['Cubic']
r2cas['\xf7\x5b'] = ['Quart']
r2cas['\xf7\x5c'] = ['Log']
r2cas['\xf7\x5d'] = ['Exp']
r2cas['\xf7\x5e'] = ['Power']
r2cas['\xf7\x60'] = ['S-WindAuto']
r2cas['\xf7\x61'] = ['S-WindMan']
r2cas['\xf7\x62'] = ['Graph X=']
r2cas['\xf7\x63'] = ['Y=Type']
r2cas['\xf7\x64'] = ['r=Type']
r2cas['\xf7\x65'] = ['ParamType']
r2cas['\xf7\x67'] = ['X=cType']
r2cas['\xf7\x6a'] = ['Y>Yype']
r2cas['\xf7\x6b'] = ['Y<Type']
r2cas['\xf7\x6c'] = ['Y\xafType']
r2cas['\xf7\x6d'] = ['Y\xaeType']
r2cas['\xf7\x70'] = ['G-Connect']
r2cas['\xf7\x71'] = ['G-Plot']
r2cas['\xf7\x78'] = ['BG-None']
r2cas['\xf7\x79'] = ['BG-Pict ']
r2cas['\xf7\x7a'] = ['GridOff']
r2cas['\xf7\x7d'] = ['GridOn']
r2cas['\xf7\x80'] = ['D Var ']
r2cas['\xf7\x88'] = ['VarRange']
r2cas['\xf7\x90'] = ['a\xccType']
r2cas['\xf7\x91'] = ['a\xcc+Type']
r2cas['\xf7\x93'] = ['StoPict ']
r2cas['\xf7\x94'] = ['RclPict ']
r2cas['\xf7\x95'] = ['StoGMEM ']
r2cas['\xf7\x96'] = ['RclGMEM ']
r2cas['\xf7\x97'] = ['StoV-Win ']
r2cas['\xf7\x98'] = ['RclV-Win ']
r2cas['\xf7\xa0'] = ['Tangent ']
r2cas['\xf7\xa1'] = ['Normal ']
r2cas['\xf7\xa2'] = ['Inverse ']
r2cas['\xf7\xa3'] = ['Vertical ']
r2cas['\xf7\xa4'] = ['Horizontal ']
r2cas['\xf7\xa5'] = ['Text ']
r2cas['\xf7\xa6'] = ['Circle ']
r2cas['\xf7\xa7'] = ['F-Line ']
r2cas['\xf7\xa8'] = ['PlotOn ']
r2cas['\xf7\xa9'] = ['PlotOff ']
r2cas['\xf7\xaa'] = ['PlotChg ']
r2cas['\xf7\xab'] = ['PxlOn ']
r2cas['\xf7\xac'] = ['PxlOff ']
r2cas['\xf7\xad'] = ['PxlChg ']
r2cas['\xf7\xaf'] = ['PxlTest ']
r2cas['\xf7\xb0'] = ['SortA(']
r2cas['\xf7\xb1'] = ['SortD(']
r2cas['\xf7\xb2'] = ['VarList1']
r2cas['\xf7\xb3'] = ['VarList2']
r2cas['\xf7\xb4'] = ['VarList3']
r2cas['\xf7\xb5'] = ['VarList4']
r2cas['\xf7\xb6'] = ['VarList5']
r2cas['\xf7\xb7'] = ['VarList6']
r2cas['\xf7\xb8'] = ['File1']
r2cas['\xf7\xb9'] = ['File2']
r2cas['\xf7\xba'] = ['File3']
r2cas['\xf7\xbb'] = ['File4']
r2cas['\xf7\xbc'] = ['File5']
r2cas['\xf7\xbd'] = ['File6']
r2cas['\xf7\xc0'] = ['FuncOn']
r2cas['\xf7\xc1'] = ['SimulOn']
r2cas['\xf7\xc2'] = ['AxesOn']
r2cas['\xf7\xc3'] = ['CoordOn']
r2cas['\xf7\xc4'] = ['LabelOn']
r2cas['\xf7\xc5'] = ['DerivOn']
r2cas['\xf7\xc6'] = ['LocusOn']
r2cas['\xf7\xc7'] = ['\x98DispOn']
r2cas['\xf7\xc8'] = ['G SelOn ']
r2cas['\xf7\xc9'] = ['T SelOn ']
r2cas['\xf7\xca'] = ['D SelOn ']
r2cas['\xf7\xcb'] = ['R SelOn ']
r2cas['\xf7\xcc'] = ['DrawOn']
r2cas['\xf7\xd0'] = ['FuncOff']
r2cas['\xf7\xd1'] = ['SimulOff']
r2cas['\xf7\xd2'] = ['AxesOff']
r2cas['\xf7\xd3'] = ['CoordOff']
r2cas['\xf7\xd4'] = ['LabelOff']
r2cas['\xf7\xd5'] = ['DerivOff']
r2cas['\xf7\xd6'] = ['LocusOff']
r2cas['\xf7\xd7'] = ['\x98DispOff']
r2cas['\xf7\xd8'] = ['G SelOff ']
r2cas['\xf7\xd9'] = ['T SelOff ']
r2cas['\xf7\xda'] = ['D SelOff ']
r2cas['\xf7\xdb'] = ['R SelOff ']
r2cas['\xf7\xdc'] = ['DrawOff']
r2cas['\xfb'] = ['P(']
r2cas['\xfc'] = ['Q(']
r2cas['\xfd'] = ['R(']
r2cas['\xfe'] = ['t(']
#r2cas['\xff'] = ['']
