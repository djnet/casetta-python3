#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" This is a part of Casetta project.

    Post installation script for win32 system
    
    This script creat a shortcut for Gasetta in the desktop and the
    start menu, and remove them at the uninstallation

    (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
    (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
    
    Website: <URL:http://casetta.tuxfamily.org>
    
    This program is under the GNU/GPL.
    
"""

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html

import os
import sys

if sys.argv[1] == '-install':
    python_path = sys.prefix
    # Get the pythonw.exe path (pythonw.exe don't show any console)
    pyw_path = os.path.abspath(os.path.join(python_path, 'pythonw.exe'))
    # Find the Gasetta installation path
    gasetta_dir = os.path.abspath(os.path.join(python_path, 'share', \
                                                    'gasetta'))
    # Get filenames of gasetta.py and gasetta.ico
    ico_path = os.path.join(gasetta_dir, 'gasetta.ico')
    script_path = os.path.join(gasetta_dir, 'gasetta.py')
    
    # Make shortcuts
    # For each shortcut directory, try to make the shortcut
    # in the common (eg ALL USERS) directory, else in the current
    # user directory.
    
    # Desktop shortcut
    try:
        desktop_path = get_special_folder_path("CSIDL_COMMON_DESKTOPDIRECTORY")
    except OSError:
        desktop_path = get_special_folder_path("CSIDL_DESKTOPDIRECTORY")
    
    create_shortcut(pyw_path,                                   # Target
                    "Casio calculators data manager",           # Description
                    os.path.join(desktop_path, 'gasetta.lnk'),  # Filename
                    script_path,                                # Argument
                    gasetta_dir,                                # Work directory
                    ico_path                                    # Icon filname
                    )
    file_created(os.path.join(desktop_path, 'gasetta.lnk'))
    
    # Shortcut in the Start menu
    try:
        start_path = get_special_folder_path("CSIDL_COMMON_PROGRAMS")
    except OSError:
        start_path = get_special_folder_path("CSIDL_PROGRAMS")
    programs_path = os.path.join(start_path, "Casetta project")
    
    try :
        os.mkdir(programs_path)
    except OSError:
        pass
    directory_created(programs_path)
    
    create_shortcut(pyw_path,                                   # Target
                    "Casio calculators data manager",           # Description
                    os.path.join(programs_path, 'gasetta.lnk'),  # Filename
                    script_path,                                # Argument
                    gasetta_dir,                                # Work directory
                    ico_path                                    # Icon filname
                    )
    file_created(os.path.join(programs_path, 'gasetta.lnk'))
    
    # End (youpi-message)
    sys.stdout.write("Shortcuts created.\n")
    sys.exit()
