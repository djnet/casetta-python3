#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name>
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
#  Website: <URL:http://casetta.tuxfamily.org>
#
# Version 0.3.0
#
# changelog:
#
# # 0.3.0 version:
#
#   o some compatibility modifications for casetta 0.3
#   o now manage the internal serial transfer tool
#   o now manage port numbers (0, 1, ...) for pySerial
#   o some change to work with casetta 0.3 classes
#   o new check name system
#   o catch new exception about formats limitations
#   o Add an option to choice the end of line for ctf and newcat
#   o Add the --extract option to extract data from a password
#
# # 0.2.0 version:
#
#   This file was a part of casetta in olders version. Here is the changelog
#   from the casetta CLI part:
#
#   o split casetta into the CORE part (casetta module) and the CLI part (this
#     file)
#   o rewrite the CLI part to work with the new casetta module
#   o add the --split option
#   o add the --not-interactive option
#   o add the multi-file-names input
#   o add the --help option (a syntax error just show a little message).
#   o return a non zero status when an error occur (2 for bad syntax, 1 in 
#     other case)
#   o write errors and warnings on the stderr file
#   o add a new module: prefs.py to manage the ~/.casetta settings file
#   o add the management of two new transfer tool: CaS and an user defined
#     transfert tool
#   o add the --set_default option which save the current port/transfert tool
#     as default
#   o add the --edit option to edit the settings of the user defined transfert
#     tool
#   o Disable the CaS transfer tool (CaS can be used with the other transfer
#     tool)
#   o Allow any /dev/tty* posix serial port (not only /dev/ttySn or
#      /dev/ttyUSBn)
#
# # olders version:
#
#   see casetta changelog
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Command line tool to manage data from Casio calculators"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__license__ = "GPL"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

# Imports
import os, sys
import casetta
import prefs

# Help
syntax = """Command line tool to manage data from Casio calculators.

Usage: casetta [OPTION] INPUT [INPUT_FORMAT] OUTPUT [OUTPUT_FORMAT]
or:    casetta -e
       casetta -h

  OPTIONS :
  -d, --set_default     set the current transfer tool, the current port and
                        the end of line as default.
  -e, --edit_tool       let you edit the user defined transfer tool command 
                        line. You can also edit the ~/.casetta file.
  -h, --help            show this message
  -l, --end_of_line=SYS set the end of line used to save newcat and ctf files.
                        SYS can be WIN (for windows), MAC (for pre-X MacOS) or
                        UNIX (for GNU/Linux, BSD, MacOS X...).
                        The default value depend of your system.
  -n, --not_interactive casetta exit instead of asking something, and return
                        an error status (only for file convertions)
  -s, --split           produce one file by data type in OUTPUT directory
                        with this option, you must specify OUTPUT_FORMAT.
  -x, --extract         extract all programs in the first backup found, and
                        save them in OUTPUT. If no backup is found, this is 
                        ignored.

  INPUT and OUTPUT must be a file name, a path, a port or the keyword 'casio',
  depend of the format. If format isn't specified, casetta will try to 
  find it thanks to the filename extension. This may fail in some case,
  just specify the format.
  The default port is the first serial port, /dev/ttyS0 on posix, com1 on DOS.
  INPUT can be multiple file names, separated by a colon ":". In this case,
  all data from all files will be join in one file.
  INPUT_FORMAT and OUTPUT_FORMAT can be a file format, or a transfer tool.
  They can be ommited if the filename have a correct extension or if you
  want to use the default transfert tool.
  
  Available formats are:
  
  newcat    Newcat format, a cat-like format with more equivalents of the 
            special characters, and returns to the line of your choice, to edit
            yours programs in any editor (manage programs, backups, pictures and
            screen captures).
            This format is recommended.
  ctf       Calculator text format (manage programs).
  cafix     Original cafix format. For output, files will be saved in the
            dirname of OUTPUT (manage programs, backups, pictures and screen
            captures).
  cas       Format of Casiolink (only usable in input, manage programs,
            backups, pictures and screencaptures).
  cat       Cat format, used by the program Fa-12* (manage programs, backups and
            pictures).
  g1r       Fa-124 format (only usable in input, manage programs and pictures).
  fxi       Fxi format, used by the program Fx-Interface (manage programs and
            pictures).
  fxb       Fx-Interface Backup format (manage backups).
  fxd       Fx-Interface Pro format (only usable in input, manage programs,
            pictures and screen captures).
  fxm       Fx-Interface Pro Backup format (only usable in input, manage 
            backups).
  casemul   Format of the casemul emulator (manage programs and pictures).
  pil       Standard picture formats (manage pictures and screencaptures)
            The format is detected from the extension. All formats managed by
            the Python Image Library are reconized.
  
  Reconized ports:
  
  n             The nth port (from 0)
  /dev/tty*     posix port * (/dev/ttySn for the linux serial port n, 
                /dev/ttyUSBn for the linux USB port n)
  comN          DOS or Miscrosoft Windows serial port N
  
  Avaible transfers tool are (after the keyword 'casio' or a port):
  
  casio     receive or send the data to the calculator using the default
            transfer tool. Optional (the keyword 'casio' is enough).
  serial    use the internal serial transfer tool (recommended).
  cafix     use the transfer tool cafix. Need the command cafix in your path.
            This tool use the port /dev/casio set with the cafix-setup command.
  other     use the user defined tool. You can choose it with the -e option.
            The port of this tool depend of your configuration.
  
  Examples:
  
  * To get data from your calculator:
            casetta 0 serial my_file.newcat
    or, if you have saved your settings with -d:
            casetta casio my_file.newcat
  
  * To send data to your calculator:
            casetta my_file.newcat 0 serial
    or, if you have saved your settings with -d:
            casetta my_file.newcat casio
  
  * To convert a file between two formats:
            casetta my_file.FXI myfile.newcat

Version %s.

License: GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
""" % (__version__)

no_args_message = """
Usage: casetta [OPTION] INPUT [INPUT_FORMAT] OUTPUT [OUTPUT_FORMAT]
To know more, do: "casetta --help".
"""

class Param:
    """Parameters of the cli from sys.argv"""
    input_file = ''
    input_format = None
    output_file = ''
    output_format = None
    split = False
    not_interactive = False
    set_default = False
    port = None
    tool = None
    input_class = None
    output_class = None
    end_of_line = None
    extract = False
    
    def fill(self):
        """Replace default data with autodetected/from prefs data"""
        if self.input_file.startswith('/dev/tty') or \
                self.input_file.startswith('com') or \
                self.input_file.isdigit():
            self.port, self.input_file = self.input_file, 'casio'
        if self.output_file.startswith('/dev/tty') or \
                self.output_file.startswith('com') or \
                self.output_file.isdigit():
            self.port, self.output_file = self.output_file, 'casio'
        if self.end_of_line == None:
            self.end_of_line = prefs.default_end_of_line
        # If no port, use the one from prefs
        if self.port == None:
            self.port = prefs.default_port
        if self.input_file == 'casio' and self.input_format == None:
            self.tool = prefs.default_tool
        elif self.input_file == 'casio':
            self.tool = self.input_format
        if self.output_file == 'casio' and self.output_format == None:
            self.tool = prefs.default_tool
        elif self.output_file == 'casio':
            self.tool = self.output_format
        if self.set_default:
            prefs.define('default_port', self.port)
            prefs.define('default_tool', self.tool)
            prefs.define('default_end_of_line', self.end_of_line)
        # Find format class
        if self.input_file != 'casio':
            if self.input_format != None:
                self.input_class = casetta.formats.\
                                    format_dic[self.input_format]
            else:
                self.input_class = casetta.formats.choose_format(\
                                        self.input_file)
        if self.output_file != 'casio':
            if self.output_format != None:
                self.output_class = casetta.formats.\
                                    format_dic[self.output_format]
            else:
                self.output_class = casetta.formats.choose_format(\
                                        self.output_file)

def arg_parser():
    """Parse sys.argv, and run the right behaviour with the right arguments"""
    # Get arguments
    if len(sys.argv) < 2:
        # No argument => print a message
        sys.stderr.write(no_args_message)
        sys.exit(2)
    elif len(sys.argv) == 2 and (sys.argv[1] == '-h' or \
                                 sys.argv[1] == '--help') :
        # Help message
        print(syntax)
        sys.exit()
    elif len(sys.argv) == 2 and (sys.argv[1] == '-e' or \
                                 sys.argv[1] == '--edit') :
        # Other tool editor
        other_tool_editor()
        sys.exit()
    elif len(sys.argv) < 3:
        # Output file is missing
        sys.stderr.write(no_args_message)
        sys.exit(2)
    else :
        param = Param()
        param.port = prefs.default_port
        arg_list = []
        for arg in sys.argv[1:]:
            # Manage s[plit] n[ot_interactive] and d/set_default options
            if arg[0] == '-' and arg[1] != '-' :
                if 's' in arg :
                    param.split = True
                if 'n' in arg :
                    param.not_interactive = True
                if 'd' in arg :
                    param.set_default = True
                if 'l' in arg :
                    param.end_of_line = arg.split('=')[1]
                if 'x' in arg :
                    param.extract = True
            elif arg == '--split':
                param.split = True
            elif arg == '--not_interactive' or arg == '--not-interactive':
                param.not_interactive = True
            elif arg == '--set_default':
                param.set_default = True
            elif arg.startswith('--end_of_line='):
                param.end_of_line = arg.split('=')[1]
            elif arg == '--extract':
                param.extract = True
            else :
                arg_list.append(arg)
        if len(arg_list) == 2:
            # 0:input, 1:output
            param.input_file = arg_list[0]
            param.output_file = arg_list[1]
        elif len(arg_list) == 3:
            # 0:input, 1:input_type|output, 2:output|output_type
            if arg_list[1].lower() in list(casetta.formats.format_dic.keys()) \
                    or ((arg_list[1].lower() in \
                                list(casetta.devices.tool_dic.keys()) + ['casio']) \
                        and (arg_list[0].lower().startswith('com') \
                        or arg_list[0].startswith('/dev/tty') \
                        or arg_list[0].isdigit())):
                # 0:input, 1:input_type, 2:output
                param.input_file = arg_list[0]
                param.input_format = arg_list[1].lower()
                param.output_file = arg_list[2]
            else:
                # 0:input, 1:output, 2:output_type
                param.input_file = arg_list[0]
                param.output_file = arg_list[1]
                param.output_format = arg_list[2].lower()
        else:
            # 0:input, 1:input_type, 2:output, 3:output_type
            param.input_file = arg_list[0]
            param.output_file = arg_list[2]
            param.input_format = arg_list[1].lower()
            param.output_format = arg_list[3].lower()
        param.fill()
        cli(param)

def other_tool_editor():
    """Interactive editor for user defined transfer tool"""
    file_formats = []
    for format_id in list(casetta.formats.format_dic.keys()):
        format = casetta.formats.format_dic[format_id]
        if format.write and format.read:
            file_formats.append(format_id)
    print("casetta let you use your own command line transfert tool.")
    print("casetta will save/read a temporary file read/save by your")
    print("transfer tool.")
    print("Enter the format of the temporary file:")
    print(("(Avaible values are: " + ', '.join(file_formats) + ' )'))
    format = eval(input())
    print("Enter the command line to send data to the calculator.")
    print("You can use '%file' for the temporary file name, '%port'")
    print("for the serial port.")
    input_cmd = eval(input())
    print("Enter the command line to receive data from the calculator.")
    print("You can use '%file' for the temporary file name, '%port'")
    print("for the serial port.")
    output_cmd = eval(input())
    print("Your settings are:")
    print(("Format: %s" % format))
    print(("Input command line: %s" % input_cmd))
    print(("Output command line: %s" % output_cmd))
    print("Write this settings? [Yn]")
    rep = eval(input())
    if rep.lower().startswith('y') or rep == '':
        prefs.define("other_tool_format", format)
        prefs.define("other_tool_input", input_cmd)
        prefs.define("other_tool_output", output_cmd)
        print("Settings saved.")

def receive(param):
    """Use param to select the right transfer tool."""
    if param.tool == 'serial':
        tool = casetta.devices_serial.Connexion(param.port)
    elif param.tool == 'cafix':
        tool = casetta.devices.CafixTool()
    elif param.tool == 'other':
        tool = casetta.devices.OtherTool(
                    casetta.formats.format_dic[prefs.other_tool_format],
                    prefs.other_tool_input,
                    prefs.other_tool_output)
    return tool.receive()

def send(file_data, param):
    """Use param to select the right transfer tool."""
    if param.tool == 'serial':
        tool = casetta.devices_serial.Connexion(param.port)
    elif param.tool == 'cafix':
        tool = casetta.devices.CafixTool()
    elif param.tool == 'other':
        tool = casetta.devices.OtherTool(
                    casetta.formats.format_dic[prefs.other_tool_format],
                    prefs.other_tool_input,
                    prefs.other_tool_output)
    return tool.send(file_data.data)

def cli(param):
    """Run the CLI inteface with a param object"""
    # Set end of line
    if param.end_of_line.lower() == 'win':
        eol = '\r\n'
    elif param.end_of_line.lower() == 'mac':
        eol = '\r'
    else:
        eol = '\n'
    casetta.formats.format_dic['newcat'].end_of_line = eol
    casetta.formats.format_dic['ctf'].end_of_line = eol
    # INPUT
    if param.input_file.lower() != 'casio':
        file_data = casetta.data.FileData()
        for filename in param.input_file.split(":"):
            try :
                new_data_list = file_data.import_file(filename, \
                                                param.input_class)
            except casetta.errors.FormatNotFound :
                # casetta can't find the input format
                sys.exit("casetta can't find the input format.")
            except casetta.errors.FormatCantOpen:
                sys.exit("The input format you choice can not open files.")
            for new_data in new_data_list:
                # Check names
                if file_data.check_name(new_data) != 1 and \
                        not param.not_interactive:
                    while file_data.check_name(new_data) != 1:
                        if file_data.check_name(new_data) == 0:
                            print(("The name of the %(kind)s '%(name)s' isn't \
correct" % {'kind': new_data.dType, 'name': new_data.get_name()}))
                        elif file_data.check_name(new_data) == -1:
                            print(("The name of the %(kind)s '%(name)s' is \
already used" % {'kind': new_data.dType, 'name': new_data.get_name()}))
                        print("Please enter a new name:")
                        new_data.set_name(eval(input()))
                elif file_data.check_name(new_data) != 1 and \
                        param.not_interactive:
                    sys.exit("Name conflict or bad syntax for '%s'." % \
                             new_data.get_name())
    else:
        # Import data from the calculator
        print("Waiting for data from the calculator...")
        try:
            file_data = receive(param)
        except casetta.errors.ToolNotFound as tool:
            sys.exit('The transfer tool %s does not exist.' % tool)
        except casetta.errors.NoCommand as command:
            sys.exit('The command %s is not in the PATH.' % command)
        except casetta.errors.CalcOutOfMemory:
            sys.exit('The calculator is out of memory.')
        except casetta.errors.ChecksumError:
            sys.exit('The transfer aborted because of a checksum error. \
Please check your link.')
        except casetta.errors.CannotOverwrite as data_name:
            sys.exit('The calculator refuse to overwrite %s.' % data_name)
        except casetta.errors.BadAnswerFromCalc as byte:
            sys.exit('The calculator sent an unknown message (%s).' % \
                     hex(ord(byte)))
        except casetta.errors.TransferAborted:
            sys.exit('The transfer has been aborted.')
        except casetta.errors.HeaderError:
            sys.exit('Unknown data header or data header error.')
        except casetta.errors.ToolNotConfigured:
            if param.tool != 'cafix':
                sys.exit('An error occur using the transfer tool.')
            elif not param.not_interactive:
                sys.stderr.write("""
Warning: the calculator port is not configured yet.
You must run 'cafix-setup' as root.
Press CTRL-C to quit this program and do it yourself.
Press enter to run the command 'sudo cafix-setup'.
""")
                try:
                    eval(input())
                except KeyboardInterrupt:
                    sys.exit(1)
                os.system('sudo cafix-setup')
                if not os.path.exists('/dev/casio'):
                    sys.exit("/dev/casio not found.")
                else:
                    file_data = receive(param)
            else:
                sys.exit("/dev/casio not found.\n" + \
                         "Run 'cafix-setup' as root to fix it.")
    # If --extract, then extract:
    if param.extract:
        # Find a backup
        for data in file_data:
            if data.__class__ == casetta.data.Backup:
                new_file_data = casetta.data.FileData()
                program_list = data.find_program_list()
                for program_metadata in program_list:
                    new_file_data.data.append(data.get_program_by_name(
                                                        program_metadata[0]))
                file_data = new_file_data
                break
    if param.output_file != 'casio' and not param.split:
        # Save the FileData to output file
        try:
            file_data.save(param.output_file, param.output_class)
        except casetta.errors.FormatCantSave:
            sys.exit("The output format you choice can not save files.")
        except casetta.errors.FormatNotFound:
            sys.exit("casetta can't find the output format.")
        except casetta.errors.DataNoManaged as not_managed:
            if not_managed.data_type_list == ['all'] :
                sys.exit("The output format you choice can not " + \
                         "manage all input data.")
            else:
                sys.stderr.write("Warning: the output format can not manage "+\
                                 "the followed data types:\n")
                data_id_list = []
                for kind in not_managed.data_type_list:
                    data_id_list.append(kind().dType)
                sys.stderr.write(", ".join(data_id_list) + '\n')
                file_data.save(param.output_file, param.output_class, True)
    elif param.output_file != 'casio' and param.split:
        if param.output_format == None:
            sys.exit("casetta can't find the output format.")
        fd_dic = file_data.split_by_data_type()
        for data_key in fd_dic :
            if os.path.isdir(param.output_file) :
                filename = param.output_file
            else :
                filename = os.path.dirname(param.output_file)
            filename = os.path.join(filename, data_key().dType + '.' + \
                                 param.output_class().list_ext[0])
            try:
                fd_dic[data_key].save(filename, param.output_class)
            except casetta.errors.DataNoManaged as not_managed:
                if not_managed.data_type_list == ['all'] :
                    sys.stderr.write("Warning: the output format can " + \
                                     "not managed this data. Type: '%s'." % \
                                     data_key().dType)
    else:
        # Export the FileData to the calculator
        print("Wait until the calculator is ready...")
        try :
            send(file_data, param)
        except casetta.errors.ToolNotFound as tool:
            sys.exit('The transfer tool %s does not exist.' % tool)
        except casetta.errors.NoCommand as command:
            sys.exit('The command %s is not in the PATH.' % command)
        except casetta.errors.CalcOutOfMemory:
            sys.exit('The calculator is out of memory.')
        except casetta.errors.ChecksumError:
            sys.exit('The transfer aborted because of a checksum error. \
Please check your link.')
        except casetta.errors.CannotOverwrite as data_name:
            sys.exit('The calculator refuse to overwrite %s.' % data_name)
        except casetta.errors.BadAnswerFromCalc as byte:
            sys.exit('The calculator sent an unknown message (%s).' % \
                     hex(ord(byte)))
        except casetta.errors.TransferAborted:
            sys.exit('The transfer has been aborted.')
        except casetta.errors.HeaderError:
            sys.exit('Unknown data header or data header error.')
        except casetta.errors.ToolNotConfigured:
            if tool != 'cafix':
                sys.exit('An error occur using the transfer tool.')
            elif not param.not_interactive:
                sys.stderr.write("""
Warning: the calculator port is not configured yet.
You must run 'cafix-setup' as root.
Press CTRL-C to quit this program and do it yourself.
Press enter to run the command 'sudo cafix-setup'.
""")
                try:
                    eval(input())
                except KeyboardInterrupt:
                    sys.exit(1)
                os.system('sudo cafix-setup')
                if not os.path.exists('/dev/casio'):
                    sys.exit("/dev/casio not found.")
                else:
                    send(file_data, param)
            else:
                sys.exit("/dev/casio not found.\n" + \
                         "Run 'cafix-setup' as root to fix it.")
        except casetta.errors.DataNoManaged as not_managed:
            if not_managed.data_type_list == ['all'] :
                sys.exit("The transfert tool you choice can not " + \
                         "manage all input data.")
            else:
                sys.stderr.write("Warning: the transfer tool can not manage "+\
                                 "the followed data types:\n")
                sys.stderr.write(", ".join(not_managed.data_type_list) + '\n')
    # End (print nothing)
    sys.exit(0)

if __name__ == '__main__':
    # Load settings
    prefs.load()
    # Parse command line arguments
    arg_parser()
# vim:ai:et:sw=4:ts=4:sts=4:tw=78:fenc=utf-8
