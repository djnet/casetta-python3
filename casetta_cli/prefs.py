# -*- coding: utf-8 -*-
#
############################################################################
#
# (c) 2006-2007 Florian Birée aka Thesa <florian@biree.name> 
# (c) 2006 Achraf Cherti aka Asher256 <achrafcherti@gmail.com>
#
# Website: <URL:http://casetta.tuxfamily.org>
# 
# Version 0.3.0
#
# changelog:
#
# # version 0.3.0:
#
# o Change default values for the new internal transfer tool
# o Add the end of line properties
# o Add the default values for end of line (os dependant)
#
# # version 0.2.0:
#
# o first version (version number aligned on casetta_cli)
#
############################################################################
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#
# http://www.gnu.org/copyleft/gpl.html
#
############################################################################
"""Preference input/output functions for casetta_cli"""

__author__ = "Florian Birée, Achraf Cherti"
__version__ = "0.3.0"
__license__ = "GPL"
__copyright__ = "Copyright (c) 2006-2007, Florian Birée, Achraf Cherti"
__revision__ = "$Revision: $"
# $Source: $
__date__ = "$Date: $"

# Imports
import os
import configparser

# Functions
def load():
    """Load a config file"""
    global default_tool, default_port, other_tool_format, other_tool_input, \
           other_tool_output, default_end_of_line
    try:
        config = configparser.ConfigParser()
        config.read(config_file)
        default_tool = config.get("Default settings", "default tool")
        default_tool = default_tool.replace('casio_', '') # 0.2 compatibility
        default_port = config.get("Default settings", "default port")
        default_end_of_line = config.get("Default settings", \
                                         "default end of line")
        
        other_tool_format = config.get("Other tool", "format")
        other_tool_input = config.get("Other tool", "input command line")
        other_tool_output = config.get("Other tool", "output command line")
    except IOError:
        return
    except configparser.NoOptionError:
        return
    except configparser.NoSectionError:
        return

def define(name, val):
    """Change the setting name to the value val"""
    global default_tool, default_port, other_tool_format, other_tool_input, \
           other_tool_output, default_end_of_line
    globals()[name] = val
    afile = open(config_file, 'w' )
    config = configparser.ConfigParser()
    config.add_section("Default settings")
    config.set("Default settings", "default tool", default_tool)
    config.set("Default settings", "default port", default_port)
    config.set("Default settings", "default end of line", default_end_of_line)
    config.add_section("Other tool")
    config.set("Other tool", "format", other_tool_format)
    config.set("Other tool", "input command line", other_tool_input)
    config.set("Other tool", "output command line", other_tool_output)
    config.write(afile)
    afile.close()

# Default values
config_file = os.path.join(os.path.expanduser('~'), '.casetta')
# [Default settings] section
default_tool = 'serial'
default_port = '0'
if os.name == 'nt':
    default_end_of_line = 'WIN'
elif os.name == 'mac':
    default_end_of_line = 'MAC'
else:
    default_end_of_line = 'UNIX'
# [Other tool] section
other_tool_format = ''
other_tool_input = ''
other_tool_output = ''
